<?php



class Export
{
	private static function createMemberRow($member)
	{
		$row = array($member->first_name, $member->middle_name, $member->last_name,
							$member->email,$member->category_type,$member->usbc_number,
							$member->tnba_number,$member->birthday,$member->sex,
							$member->street_address,$member->zipcode,$member->phone_number,
							$member->senate,$member->senate_region);

		return $row;
	}

	private static function createMembershipRow($membership)
	{
		$row = array($membership->member->first_name, $membership->member->middle_name, $membership->member->last_name,
							$membership->member->tnba_number,$membership->creation_date,$membership->paid_datetime,
							$membership->status,$membership->type);

		return $row;
	}

	private static function createAwardRow($award)
	{
		$row = array($award->member->first_name, $award->member->middle_name, $award->member->last_name,
							$award->member->street_address,$award->member->zipcode,
							$award->member->tnba_number,$award->league->name,$award->league->bowlingAlley->name,
							$award->league->bowlingAlley->zipcode,$award->status,$award->member->sex,$award->date_of_performance,
							$award->certified,$award->current_average,$award->name_of_senate,$award->honor_score,$award->season);
		

		return $row;
	}

	private static function createLeagueRow($league)
	{
		$row = array($league->name,$league->bowlingAlley->name,$league->bowlingAlley->zipcode,($league->certified) ? "YES" : "NO");
		

		return $row;
	}

	private static function createStatshistoryRow($statsHistory)
	{
		$row = array($statsHistory->member->first_name,$statsHistory->member->middle_name,$statsHistory->member->last_name,
					$statsHistory->member->tnba_number,$statsHistory->usbc_number,$statsHistory->league->name,
					$statsHistory->league->bowlingAlley->name,$statsHistory->league->bowlingAlley->zipcode,$statsHistory->average,
					$statsHistory->num_of_games,$statsHistory->season);
		

		return $row;
	}

	public static function init($grid)
	{
		header("Content-type: text/csv");
		switch($grid)
		{
			case MEMBER_GRID:
				header("Content-Disposition: attachment; filename=Members.csv");
				break;
			case MEMBERSHIP_GRID:
				header("Content-Disposition: attachment; filename=Memberships.csv");
				break;
			case AWARD_GRID:
				header("Content-Disposition: attachment; filename=Awards.csv");
				break;
			case LEAGUE_GRID:
				header("Content-Disposition: attachment; filename=Leagues.csv");
				break;
			case STATSHISTORY_GRID:
				header("Content-Disposition: attachment; filename=Statistics.csv");
				break;
		}
		
		header("Pragma: no-cache");
		header("Expires: 0");

		define('FIRST_NAME','FIRST NAME');
		define('MIDDLE_NAME','MIDDLE NAME');
		define('LAST_NAME','LAST NAME');
		define('EMAIL','EMAIL');
		define('CATEGORY_TYPE','CATEGORY TYPE');
		define('USBC_NUMBER','USBC #');
		define('TNBA_NUMBER','TNBA #');
		define('DATE_OF_BIRTH','DATE OF BIRTH');
		define('SEX','SEX');
		define('STREET_ADDRESS','STREET ADDRESS');
		define('ZIPCODE','ZIPCODE');
		define('PHONE_NUMBER','PHONE #');
		define('SENATE','SENATE');
		define('SENATE_REGION','SENATE REGION');

		define('LEAGUE_NAME','LEAGUE NAME');
		define('BOWLING_ALLEY_NAME','BOWLING ALLEY NAME');
		define('BOWLING_ALLEY_ZIPCODE','BOWLING ALLEY ZIPCODE');
		define('STATUS','STATUS');
		define('DATE_OF_PERFORMANCE','DATE OF PERFORMANCE');
		define('CERTIFIED','CERTIFIED');
		define('CURRENT_AVERAGE','CURRENT AVERAGE');
		define('HONOR_SCORE','HONOR SCORE');
		define('SEASON','SEASON');

		define('MEMBERSHIP_STATUS','STATUS');
		define('MEMBERSHIP_CREATION_DATE','MEMBERSHIP CREATION DATE');
		define('PAID_DATE','PAID DATE');
		define('MEMBERSHIP_TYPE','MEMBERSHIP TYPE');

		define('AVERAGE','AVERAGE');
		define('NUM_OF_GAMES','# OF GAMES');
	}

	public static function exportAllMembersToCSV()
	{
		

		//$members = Helper::retrieveAllMembers();
		$dataProvider = Yii::app()->session['filtered_data'];
		$lastPage = false;
		$pagination = $dataProvider->pagination;
		$csv = fopen("php://output", "w");
		$titleRow = array(FIRST_NAME, MIDDLE_NAME, LAST_NAME, EMAIL,CATEGORY_TYPE,USBC_NUMBER,TNBA_NUMBER,
						DATE_OF_BIRTH,SEX,STREET_ADDRESS,ZIPCODE,PHONE_NUMBER,SENATE,SENATE_REGION);
		fputcsv($csv, $titleRow);

		while($members = $dataProvider->getData(true))
		{
			foreach($members as $member)
			{
				$row = Export::createMemberRow($member);

				fputcsv($csv,$row);
				//error_log("here");
				error_log($member->first_name . "/" . $member->tnba_number);
			}
			$pagination->currentPage++;
			error_log("currentPage: " . $pagination->currentPage);
			error_log("pageCount: " . $pagination->pageCount);
			if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;

                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
		}
		
		fclose($csv);
	}

	public static function exportAllMembershipsToCSV()
	{
		$dataProvider = Yii::app()->session['filtered_data'];
		$lastPage = false;
		$pagination = $dataProvider->pagination;

		$csv = fopen("php://output", "w");
		$titleRow = array(FIRST_NAME, MIDDLE_NAME, LAST_NAME,TNBA_NUMBER,MEMBERSHIP_CREATION_DATE,PAID_DATE,MEMBERSHIP_TYPE);
		fputcsv($csv, $titleRow);

		while($memberships = $dataProvider->getData(true))
		{
			foreach($memberships as $membership)
			{
				$row = Export::createMembershipRow($membership);

				fputcsv($csv,$row);
				//error_log("here");
				error_log($membership->member->first_name . "/" . $membership->member->tnba_number);
			}
			$pagination->currentPage++;
			error_log("currentPage: " . $pagination->currentPage);
			error_log("pageCount: " . $pagination->pageCount);
			if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;

                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
		}
		
		fclose($csv);
	}

	public static function exportAllAwardsToCSV()
	{
		$dataProvider = Yii::app()->session['filtered_data'];
		$lastPage = false;
		$pagination = $dataProvider->pagination;

		$csv = fopen("php://output", "w");
		$titleRow = array(FIRST_NAME, MIDDLE_NAME, LAST_NAME, STREET_ADDRESS, ZIPCODE, TNBA_NUMBER, LEAGUE_NAME, BOWLING_ALLEY_NAME,
						  BOWLING_ALLEY_ZIPCODE, STATUS, SEX, DATE_OF_PERFORMANCE, CERTIFIED, CURRENT_AVERAGE, SENATE, HONOR_SCORE, SEASON);
		
		fputcsv($csv, $titleRow);

		while($awards = $dataProvider->getData(true))
		{
			foreach($awards as $award)
			{
				$row = Export::createAwardRow($award);

				fputcsv($csv,$row);
				//error_log("here");
				error_log($award->member->first_name . "/" . $award->member->tnba_number);
			}
			$pagination->currentPage++;
			error_log("currentPage: " . $pagination->currentPage);
			error_log("pageCount: " . $pagination->pageCount);
			if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;

                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
		}
		
		fclose($csv);
	}

	public static function exportAllLeaguesToCSV()
	{
		$dataProvider = Yii::app()->session['filtered_data'];
		$lastPage = false;
		$pagination = $dataProvider->pagination;

		$csv = fopen("php://output", "w");
		$titleRow = array(LEAGUE_NAME, BOWLING_ALLEY_NAME,BOWLING_ALLEY_ZIPCODE,CERTIFIED);
		
		fputcsv($csv, $titleRow);

		while($leagues = $dataProvider->getData(true))
		{
			foreach($leagues as $league)
			{
				$row = Export::createLeagueRow($league);

				fputcsv($csv,$row);
				//error_log("here");
				error_log($league->name . "/" . $league->bowlingAlley->name);
			}
			$pagination->currentPage++;
			error_log("currentPage: " . $pagination->currentPage);
			error_log("pageCount: " . $pagination->pageCount);
			if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;

                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
		}
		
		fclose($csv);
	}

	public static function exportAllStatshistoryToCSV()
	{
		$dataProvider = Yii::app()->session['filtered_data'];
		$lastPage = false;
		$pagination = $dataProvider->pagination;

		$csv = fopen("php://output", "w");
		$titleRow = array(FIRST_NAME,MIDDLE_NAME,LAST_NAME,TNBA_NUMBER,USBC_NUMBER,LEAGUE_NAME,
							BOWLING_ALLEY_NAME,BOWLING_ALLEY_ZIPCODE,AVERAGE,NUM_OF_GAMES,SEASON);
		
		fputcsv($csv, $titleRow);

		while($statsHistorys = $dataProvider->getData(true))
		{
			foreach($statsHistorys as $statsHistory)
			{
				$row = Export::createStatshistoryRow($statsHistory);

				fputcsv($csv,$row);
				//error_log("here");
				error_log($statsHistory->member->first_name . "/" . $statsHistory->member->last_name);
			}
			$pagination->currentPage++;
			error_log("currentPage: " . $pagination->currentPage);
			error_log("pageCount: " . $pagination->pageCount);
			if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;

                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
		}
		
		fclose($csv);
	}

	public static function exportSelectedRowsForMembers($data)
	{

		$csv = fopen("php://output", "w");
		$titleRow = array(FIRST_NAME, MIDDLE_NAME, LAST_NAME, EMAIL,CATEGORY_TYPE,USBC_NUMBER,TNBA_NUMBER,
						DATE_OF_BIRTH,SEX,STREET_ADDRESS,ZIPCODE,PHONE_NUMBER,SENATE,SENATE_REGION);

		fputcsv($csv, $titleRow);

		$pks = $data;
		error_log(print_r($pks,true));

		foreach($pks as $key => $value)
		{
			$member = Member::model()->findByPk($key);
			$row = Export::createMemberRow($member);

			fputcsv($csv,$row);
			//error_log("here");
			error_log($member->first_name . "/" . $member->tnba_number);
		}
		
		fclose($csv);
	}

	public static function exportSelectedRowsForMemberships($data)
	{
		$csv = fopen("php://output", "w");
		$titleRow = array(FIRST_NAME, MIDDLE_NAME, LAST_NAME,TNBA_NUMBER,MEMBERSHIP_CREATION_DATE,PAID_DATE,MEMBERSHIP_TYPE);

		fputcsv($csv, $titleRow);

		$pks = $data;
		error_log(print_r($pks,true));

		foreach($pks as $key => $value)
		{
			$member = Member::model()->findByPk($key);
			$row = Export::createMemberRow($member);

			fputcsv($csv,$row);
			//error_log("here");
			error_log($member->first_name . "/" . $member->tnba_number);
		}
		
		fclose($csv);
	}

	public static function exportSelectedRowsForAwards($data)
	{
		$csv = fopen("php://output", "w");
		$titleRow = array(FIRST_NAME, MIDDLE_NAME, LAST_NAME, STREET_ADDRESS, ZIPCODE, TNBA_NUMBER, LEAGUE_NAME, BOWLING_ALLEY_NAME,
						  BOWLING_ALLEY_ZIPCODE, STATUS, SEX, DATE_OF_PERFORMANCE, CERTIFIED, CURRENT_AVERAGE, SENATE, HONOR_SCORE, SEASON);

		fputcsv($csv, $titleRow);

		$pks = $data;
		error_log(print_r($pks,true));

		foreach($pks as $key => $value)
		{
			$award = AwardsHighAverageBowler::model()->findByPk($key);
			$row = Export::createAwardRow($award);

			fputcsv($csv,$row);
			//error_log("here");
			error_log($award->member->first_name . "/" . $award->member->tnba_number);
		}
		
		fclose($csv);
	}

	public static function exportSelectedRowsForLeagues($data)
	{
		$csv = fopen("php://output", "w");
		$titleRow = array(LEAGUE_NAME, BOWLING_ALLEY_NAME,BOWLING_ALLEY_ZIPCODE,CERTIFIED);

		fputcsv($csv, $titleRow);

		$pks = $data;
		error_log(print_r($pks,true));

		foreach($pks as $key => $value)
		{
			$league = League::model()->findByPk($key);
			$row = Export::createLeagueRow($league);

			fputcsv($csv,$row);
			//error_log("here");
			error_log($league->name . "/" . $league->bowlingAlley->name);
		}
		
		fclose($csv);
	}

	public static function exportSelectedRowsForStatshistory($data)
	{
		$csv = fopen("php://output", "w");
		$titleRow = array(FIRST_NAME,MIDDLE_NAME,LAST_NAME,TNBA_NUMBER,USBC_NUMBER,LEAGUE_NAME,
							BOWLING_ALLEY_NAME,BOWLING_ALLEY_ZIPCODE,AVERAGE,NUM_OF_GAMES,SEASON);

		fputcsv($csv, $titleRow);

		$pks = $data;
		error_log(print_r($pks,true));

		foreach($pks as $key => $value)
		{
			$pks = explode(",",$key);
			$bowlerMemberID = $pks[0];
			$leagueCertNumber = $pks[1];
			$season = $pks[2];

			//error_log($pk);
			$statsHistory = StatsHistory::model()->findByPk(array('bowler_member_id'=>$bowlerMemberID,
																'league_cert_number'=>$leagueCertNumber,
																'season'=>$season));
			error_log(print_r($statsHistory,true));
			$row = Export::createStatshistoryRow($statsHistory);

			fputcsv($csv,$row);
			//error_log("here");
			error_log($statsHistory->member->first_name . "/" . $statsHistory->member->last_name);
		}
		
		fclose($csv);
	}

}

?>