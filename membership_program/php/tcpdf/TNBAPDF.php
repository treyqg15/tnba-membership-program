<?php

include(Yii::app()->basePath.'/../php/Classes/GlobalFunctions.php');
require_once(Yii::app()->basePath.'/../libs/php/tcpdf/tcpdf.php');

// Extend the TCPDF class to create custom Header and Footer
class TNBAPDF extends TCPDF {

    //defining constants
    private $RING;
    private $PIN;
    private $PACKING_LIST;
    private $PACKING_LETTER;

    public $columns;
    public $senate;

    public function __construct($orientation='P', $unit='mm', $format='A4', $unicode=true, $encoding='UTF-8', $diskcache=false, $pdfa=false) {

        parent::__construct($orientation,$unit,$format,$unicode,$encoding,$diskcache,$pdfa);
        
        //init constants
        $this->RING = 'Ring';
        $this->PIN = 'Pin';
        $this->PACKING_LIST = 'PACKING LIST';
        $this->PACKING_LETTER = 'PACKING LETTER';
        $this->PRIOR_AWARD = 'PRIOR AWARD';
        $this->MISSING_INFO_LETTER = 'MISSING INFO LETTER';
        $this->NO_MEMBERSHIP_LETTER = 'NO MEMBERSHIP LETTER';
        $this->CONGRATULATORY_POSTCARD = 'CONGRATULATORY POSTCARD';
        $this->PRIOR_POSTCARD = 'PRIOR POSTCARD';
        $this->ACHIEVEMENT_POSTCARD = 'ACHIEVEMENT POSTCARD';
        $this->MAILING_LABELS = 'GENERATE MAILING LABELS';
        $this->LEAGUE_CERTIFICATION = 'LEAGUE CERTIFICATION';
        $this->TNBA_CARD = 'GENERATE TNBA CARD';
        $this->TNBA_CARDS = 'GENERATE TNBA CARDS';

        $this->columns = array();
        $this->senate = null;

        // set document information
        $this->SetCreator(PDF_CREATOR);
        $this->SetAuthor('The National Bowling Association Inc.');
        $this->SetTitle('Packaging List');
        $this->SetSubject('Packaging List');
        $this->SetKeywords('Packaging List, Awards');

        // set default header data
        $this->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        $this->setFooterData(array(0,64,0), array(0,64,128));

        // set header and footer fonts
        $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $this->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $this->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // ---------------------------------------------------------

        // set default font subsetting mode
        $this->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        $this->SetFont('dejavusans', '', 11, '', true);

        // set text shadow effect
        $this->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
    }

    //Page header
    public function Header() {
        // Logo
        //$image_file = K_PATH_IviMAGES.'logo_example.jpg';
        //$this->Image($image_file, 10, 10, 15, '', 'JPG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        
        // Set font
        $this->SetFont('helvetica', 'B', 20);
        // Title
        $this->Cell(0, 15, 'The National Bowling Association', 0, false, 'C', 0, '', 0, false, 'M', 'M');
    }

    // Page footer
    public function Footer() {
        // Position at 15 mm from bottom
        $this->SetY(-15);
        // Set font
        $this->SetFont('helvetica', 'I', 8);

        // Page number
        $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    public function startGeneration($pdfType,$data) {

        switch($pdfType) {

            case $this->PACKING_LIST:
                $this->generatePackingList($data);
                break;
            case $this->PACKING_LETTER:
                $this->generatePackingLetter($data);
                break;
            case $this->PRIOR_AWARD:
                $this->generatePriorAward($data);
                break;
            case $this->MISSING_INFO_LETTER:
                $this->generateMissingInfoLetter($data);
                break;
            case $this->NO_MEMBERSHIP_LETTER:
                $this->generateNoMembershipLetter($data);
                break;
            case $this->CONGRATULATORY_POSTCARD:
                $this->generateCongratulatoryPostcard($data);
                break;
            case $this->PRIOR_POSTCARD:
                $this->generatePriorPostcard($data);
                break;
            case $this->ACHIEVEMENT_POSTCARD:
                $this->generateAchievementPostcard($data);
                break;
            case $this->MAILING_LABELS:
                $this->generateMailingLabels($data);
                break;
            case $this->LEAGUE_CERTIFICATION:
                $this->generateLeagueCertification($data);
                break;
            case $this->TNBA_CARD:
                $this->generateTNBACard($data);            
            case $this->TNBA_CARDS:
                $this->generateTNBACards($data);
                break;
        }
    }

    private function generatePackingList($data) {

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->AddPage();

        $break = "<br /><br />";
        $rows = $this->getMemberAwardInfo($data);
        $dataProvider = Yii::app()->session['filtered_data'];
        $lastPage = false;
        $pagination = $dataProvider->pagination;
        
        // Set some content to print
        $html = 'Date: ' . date('F d, Y') . $break .
                'Senate: empty' . $break .
                'Dear Senate Secretary:' . $break .
                'Enclosed you will find TNBA High Score Award items to be presented to the following:' . $break .
                '<table border=\"1\">' .
                    '<tr>' .
                        '<th>First Name</th>' .
                        '<th>Last Name</th>' .
                        '<th>Date</th>' .
                        '<th>Score</th>' .
                        '<th>Award Item</th>' .
                    '</tr>' 
        ;

        $numOfPins = 0;
        $numOfRings = 0;
        $length = count($rows);
        
        while($rows = $dataProvider->getData(true))
        {
            //$row = $rows[$i];
            foreach($rows as $row)
            {
                $html .= '<tr>' .
                            '<td>' .
                            $row->member['first_name'] .
                            '</td>' .
                            '<td>' .
                            $row->member['last_name'] .
                            '</td>' .
                            '<td>' .
                            $this->formatDate($row['date_of_performance']) .
                            '</td>' .
                            '<td>' .
                            $row['honor_score'] .
                            '</td>';

                //figure out award item
                $honorScore = HonorScore::model()->findByPk($row['honor_score']);


                $honorScoreAtts = $honorScore->getAttributes(array('jewelry_id'));
                $jewelryType = JewelryType::model()->findByPk($honorScoreAtts['jewelry_id']);

                $jewelryTypeAtts = $jewelryType->getAttributes(array('ring_type','pin_type'));

                $awardItem = (is_null($jewelryTypeAtts['pin_type']) ? $jewelryTypeAtts['ring_type'] . ' ' . $this->RING : $jewelryTypeAtts['pin_type'] . ' ' . $this->PIN);

                //increment counter for rings and pins depending on which one goes along with this award
                if (strpos($awardItem,$this->RING) !== false)
                    ++$numOfRings;
                else
                    ++$numOfPins;

                $html .= '<td>' .
                         $awardItem .
                         '</td>';

                $html .= '</tr>';
                
            }
            
            $pagination->currentPage++;
            error_log("currentPage: " . $pagination->currentPage);
            error_log("pageCount: " . $pagination->pageCount);
            if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;
                
                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
        }

        $html .= '</table>';
        $html .= '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />';

        // Print text using writeHTMLCell()
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        //create total summary field
        $html = "THE NATIONAL BOWLING ASSOCIATION, INC." .
                "<br />" .
                "Awards Department" .
                "<br />" .
                "<br />" .
                "cc: Senate President" .
                "<br />" .
                "---------------------------------------------------------" .
                "<br />" .
                "SUMMARY OF ENCLOSED AWARDS" .
                "<br />" .
                "$numOfRings Ring (s)" .
                "<br />" .
                "$numOfPins Pin (s)" .
                "<br />"; 

        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $this->Output('Packing List.pdf', 'D');

        //============================================================+
        // END OF FILE
        //============================================================+ 
    }

    private function generatePackingLetter($data) {
        
        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->AddPage();

        $break = "<br /><br />";
        $rows = $this->getMemberAwardInfo($data);
        $dataProvider = Yii::app()->session['filtered_data'];
        $lastPage = false;
        $pagination = $dataProvider->pagination;
        
        // Set some content to print
        $html = 'Date: ' . date('F d, Y') . $break .
                'Senate: empty' . $break .
                'Dear Senate Secretary:' . $break .
                'Enclosed you will find TNBA High Score Award items to be presented to the following:' . $break .
                '<table border=\"1\">' .
                    '<tr>' .
                        '<th>First Name</th>' .
                        '<th>Last Name</th>' .
                        '<th>Date</th>' .
                        '<th>Score</th>' .
                        '<th>Award Item</th>' .
                        '<th>Senate</th>' .
                    '</tr>'
        ;

        $numOfPins = 0;
        $numOfRings = 0;
        $length = count($rows);

       
        while($rows = $dataProvider->getData(true))
        {
            //$row = $rows[$i];
            foreach($rows as $row)
            {
                $html .= '<tr>' .
                            '<td>' .
                            $row->member['first_name'] .
                            '</td>' .
                            '<td>' .
                            $row->member['last_name'] .
                            '</td>' .
                            '<td>' .
                            $this->formatDate($row['date_of_performance']) .
                            '</td>' .
                            '<td>' .
                            $row['honor_score'] .
                            '</td>';

                //figure out award item
                $honorScore = HonorScore::model()->findByPk($row['honor_score']);

                $honorScoreAtts = $honorScore->getAttributes(array('jewelry_id'));
                $jewelryType = JewelryType::model()->findByPk($honorScoreAtts['jewelry_id']);

                $jewelryTypeAtts = $jewelryType->getAttributes(array('ring_type','ring_size','pin_type'));

                $awardItem = (is_null($jewelryTypeAtts['pin_type']) ? $jewelryTypeAtts['ring_type'] . ' ' . $this->RING : $jewelryTypeAtts['pin_type'] . ' ' . $this->PIN);

                //increment counter for rings and pins depending on which one goes along with this award
                if (strpos($awardItem,$this->RING) !== false)
                {
                    ++$numOfRings;

                    //find Ring Size
                    $ring = Ring::model()->findByPk(array('type' => $jewelryTypeAtts['ring_type'],'size' => $jewelryTypeAtts['ring_size']));

                    $ringAtts = $ring->getAttributes(array('size'));

                    $awardItem .= ' ' . $ringAtts['size'];
                }
                else
                    ++$numOfPins;

                $html .= '<td>' .
                         $awardItem .
                         '</td>' . 
                         '<td>' .
                         $row->member['senate'] .
                         '</td>';

                $html .= '</tr>';
            }
            
            $pagination->currentPage++;
            error_log("currentPage: " . $pagination->currentPage);
            error_log("pageCount: " . $pagination->pageCount);
            if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;
                
                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
        }

        $html .= '</table>';
        $html .= '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />' .
                  '<br />';

        // Print text using writeHTMLCell()
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        //create total summary field
        $html = "THE NATIONAL BOWLING ASSOCIATION, INC." .
                "<br />" .
                "Awards Department" .
                "<br />" .
                "<br />" .
                "cc: Senate President" .
                "<br />" .
                "---------------------------------------------------------" .
                "<br />" .
                "SUMMARY OF ENCLOSED AWARDS" .
                "<br />" .
                "$numOfRings Ring (s)" .
                "<br />" .
                "$numOfPins Pin (s)" .
                "<br />"; 

        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $this->Output('Packing Letter.pdf', 'D');

        //============================================================+
        // END OF FILE
        //============================================================+
    }

    private function generatePriorAward($data) {

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->AddPage();

        $break = "<br /><br />";
        $rows = $this->getMemberAwardInfo($data);
        $dataProvider = Yii::app()->session['filtered_data'];
        $lastPage = false;
        $pagination = $dataProvider->pagination;
        
        // Set some content to print
        $html = 'Date: ' . date('F d, Y') . $break .
                'To: Local Senate Secretary:' . $break .
                'From: TNBA Awards Department' . $break .
                '*******************************************************************************************' . $break .
                'The member listed below has been awarded a 600, 700, and 800 Series or 265-300 Game.  ' .
                'This award is presented once per season to an individual. This series or game will be placed on record.' . $break;

        $html .= '<table border=\"1\">' .
                    '<tr>' .
                        '<th>First Name</th>' .
                        '<th>Last Name</th>' .
                        '<th>Score</th>' .
                        '<th>Date</th>' .
                    '</tr>';

        $length = count($rows);

        while($rows = $dataProvider->getData(true))
        {
            //$row = $rows[$i];
            foreach($rows as $row)
            {
                $html .= '<tr>' .
                            '<td>' .
                            $row->member['first_name'] .
                            '</td>' .
                            '<td>' .
                            $row->member['last_name'] .
                            '</td>' .
                            '<td>' .
                            $row['honor_score'] .
                            '</td>' .
                            '<td>' .
                            $this->formatDate($row['date_of_performance']) .
                            '</td>' .
                         '</tr>';
            }
            
            $pagination->currentPage++;
            error_log("currentPage: " . $pagination->currentPage);
            error_log("pageCount: " . $pagination->pageCount);
            if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;
                
                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
        }

        $html .= '</table>';
        $html .= '<br />';

        // Print text using writeHTMLCell()
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $this->Output('Prior Award Memorandum.pdf', 'D');

        //============================================================+
        // END OF FILE
        //============================================================+


    }

    private function generateMissingInfoLetter($data) {

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->AddPage();

        $break = "<br /><br />";
        $rows = $this->getMemberAwardInfo($data);
        
        // Set some content to print
        $html = 'Date: ' . date('F d, Y') . $break .
                'To: Local Senate Secretary:' . $break .
                'From: TNBA Awards Department' . $break .
                'Name: __________________________                      Senate: _____________________________' . $break .
                '*******************************************************************************************' . $break .
                '<b>We are returning the attached TNBA Award Application for the above-named applicant due to the reason(s) listed below: </b>' . $break;

        $td = '<td width="4%">( )</td>';
        $html .= '<table width="100%">' .
                  '<tr>' .
                    $td .
                    '<td width="96%">Application(s) received past the forty-five (45) day reporting deadline</td>' .
                  '</tr>' .
                  '<tr>' .
                    $td .
                    '<td>Please indicate scores for <b>game</b>/<b>series</b></td>' .
                  '</tr>' .
                  '<tr>' .
                  $td .
                  '<td>Please indicate choice of award.</td>' .
                  '</tr>' .
                  '<tr>' .
                  $td .
                  '<td>Please indicate size if ring is requested</td>' .
                  '</tr>' . 
                  '<tr>' .
                  $td .
                  '<td>Please indicate date of performance and send recap sheet</td>' .
                  '</tr>' . 
                  '<tr>' .
                  $td .
                  '<td><b>League/Tournament is not certified TNBA.</b></td>' .
                  '</tr>' . 
                  '<tr>' .
                  $td .
                  '<td>600 Series awarded to low average male and female bowlers only</td>' .
                  '</tr>' . 
                  '<tr>' .
                  $td .
                  '<td>Prior Game _____________ awarded on _____________. This game has been placed on record. </td>' .
                  '</tr>' . 
                  '<tr>' .
                  $td .
                  '<td>Indicate current TNBA card number _____________ USBC card number _____________</td>' .
                  '</tr>' . 
                  '<tr>' .
                  $td .
                  '<td>Prior 300 _____ ' .
                  '299 _____ ' . 
                  '298 _____ Game awarded on _____. ' .
                  'This award is presented <b>once per season</b> to an individual. ' . 
                  'This game will be placed on record.</td>' .
                  '</tr>' .
                  '<tr>' .
                  $td .
                  '<td>Prior 600 _____ ' .
                  '700 _____ ' . 
                  '800 _____ Series awarded on _____. ' .
                  'This award is presented <b>once per season</b> to an individual. ' . 
                  'This game will be placed on record.</td>' .
                  '</tr>' .
                  '<tr>' .
                  $td .
                  '<td>Application cannot be honored as received. The date of performance appears to have been altered. ' . 
                  'Please submit a copy of the original league score sheet, signed by both team captains, or if a tournament, ' . 
                  'signed by tournament secretary.</td>' .
                  '</tr>' .
                  '<tr>' .
                  $td .
                  '<td>____________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</td>' . 
                  '</tr>' .
                '</table>';

        // Print text using writeHTMLCell()
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $this->Output('Missing Info Letter.pdf', 'D');

        //============================================================+
        // END OF FILE
        //============================================================+
    }

    private function generateNoMembershipLetter($data) {

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->AddPage();

        $break = "<br /><br />";

        $rows = $this->getMemberAwardInfo($data);
        $dataProvider = Yii::app()->session['filtered_data'];
        $lastPage = false;
        $pagination = $dataProvider->pagination;
        
        $length = count($rows);

        // Set some content to print
        $html = 'Date: ' . date('F d, Y') . $break .
                'To: Local Senate Secretary:' . $break .
                'From: TNBA Awards Department' . $break .
                '*******************************************************************************************' . $break .
                '<b>We are returning the attached TNBA Award Applications for the member(s) listed below. They do not have a current TNBA membership card for the current season: </b>' . $break;   

        $html .= '<table width="100%" border="1">';

        
        while($rows = $dataProvider->getData(true))
        {
            foreach($rows as $row)
            {
                //figure out award item
                $honorScore = HonorScore::model()->findByPk($row['honor_score']);


                $honorScoreAtts = $honorScore->getAttributes(array('jewelry_id'));
                $jewelryType = JewelryType::model()->findByPk($honorScoreAtts['jewelry_id']);

                $jewelryTypeAtts = $jewelryType->getAttributes(array('ring_type','pin_type'));

                $awardItem = (is_null($jewelryTypeAtts['pin_type']) ? $jewelryTypeAtts['ring_type'] . ' ' . $this->RING : $jewelryTypeAtts['pin_type'] . ' ' . $this->PIN);
                $html .= '<tr>' .
                            '<td width="90%"><b>' . $row->member['first_name'] . ' ' . $row->member['middle_name'] . ' ' . $row->member['last_name'] . ' - ' . $row['honor_score'] . ' (' . $awardItem . ') </b></td>' .
                            '<td><b>' . Yii::app()->dateFormatter->format("MM-d-yyyy",strtotime($row['date_of_performance'])) . '</b></td>' .
                         '</tr>';
            }
            
            $pagination->currentPage++;
            error_log("currentPage: " . $pagination->currentPage);
            error_log("pageCount: " . $pagination->pageCount);
            if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;
                
                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
        }

        $html .= '</table>';

        // Print text using writeHTMLCell()
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $this->Output('No Membership Letter.pdf', 'D');

        //============================================================+
        // END OF FILE
        //============================================================+
    }

    private function generateCongratulatoryPostcard($data) {

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->AddPage();

        $break = "<br /><br />";

        $rows = $this->getMemberAwardInfo($data);
        $dataProvider = Yii::app()->session['filtered_data'];
        $lastPage = false;
        $pagination = $dataProvider->pagination;
        
        $this->SetFont('dejavusans', '', 7, '', true);
        $html = '<table border="1">';
        
        while($rows = $dataProvider->getData(true))
        {
            $length = count($rows);
            for($i = 0; $i < $length; $i += 2)
            {
                $row = $rows[$i];
                $html .= '<tr>';
                $html .= '<td>';
                $html .= $this->createCongratulatoryText($row);
                $html .= '</td>';

                //right table
                if(($i + 1) < $length)
                {
                    $row = $rows[$i + 1];
                    $html .= '<td>';
                    $html .= $this->createCongratulatoryText($row);
                    $html .= '</td>';
                }

                $html .= '</tr>';
            }
            
            $pagination->currentPage++;
            error_log("currentPage: " . $pagination->currentPage);
            error_log("pageCount: " . $pagination->pageCount);
            if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;
                
                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
        }

        $html .= '</table>';

        // Print text using writeHTMLCell()
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $this->Output('Congratulatory_Postcard.pdf', 'D');
    }

    private function generatePriorPostcard($data) {

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->AddPage();

        $break = "<br /><br />";

        $rows = $this->getMemberAwardInfo($data);
        $dataProvider = Yii::app()->session['filtered_data'];
        $lastPage = false;
        $pagination = $dataProvider->pagination;

        $this->SetFont('dejavusans', '', 7, '', true);
        $html = '<table border="1">';
        
        while($rows = $dataProvider->getData(true))
        {
            $length = count($rows);
            for($i = 0; $i < $length; $i += 2)
            {
                $row = $rows[$i];

                $html .= '<tr>';
                $html .= '<td>';
                $html .= $this->createPriorPostcard($row);
                $html .= '</td>';

                //right table
                if(($i + 1) < $length)
                {
                    $row = $rows[$i + 1];
                    $html .= '<td>';
                    $html .= $this->createPriorPostcard($row);
                    $html .= '</td>';
                }

                $html .= '</tr>';
            }

            $pagination->currentPage++;
            error_log("currentPage: " . $pagination->currentPage);
            error_log("pageCount: " . $pagination->pageCount);
            if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;

                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
        }

        $html .= '</table>';

        // Print text using writeHTMLCell()
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $this->Output('Prior_Postcard.pdf', 'D');
    }

    private function generateAchievementPostcard($data) {

        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->AddPage();

        $break = "<br /><br />";

        $rows = $this->getMemberAwardInfo($data);
        $length = count($rows);

        $this->SetFont('dejavusans', '', 7, '', true);
        $html = '<table border="1">';
        error_log("# of people: $length");

        for($i = 0; $i < $length; $i += 2)
        {
            $row = $rows[$i];
                
            $html .= '<tr>';
            $html .= '<td>';
            $html .= $this->createAchievementPostcard($row);
            $html .= '</td>';

            //right table
            if(($i + 1) < $length)
            {
                $row = $rows[$i + 1];
                $html .= '<td>';
                $html .= $this->createAchievementPostcard($row);
                $html .= '</td>';
            }

            $html .= '</tr>';

        } 

        $html .= '</table>';

        // Print text using writeHTMLCell()
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        // ---------------------------------------------------------

        // Close and output PDF document
        // This method has several options, check the source code documentation for more information.
        $this->Output('Achievement Postcard.pdf', 'D');
    }

    private function generateMailingLabels($data)
    {
        // Add a page
        // This method has several options, check the source code documentation for more information.
        $this->AddPage();
        
        $break = "<br /><br />";

        //$this->SetMargins(50, 20, 10, true); // set the margins 
        
        $dataProvider = Yii::app()->session['filtered_data'];
        $lastPage = false;
        $pagination = $dataProvider->pagination;

        $html = '<table border="1">';

        while($rows = $dataProvider->getData(true))
        {
            $length = count($rows);
            
            for($i = 0; $i < $length; $i += 2)
            {
                $row = $rows[$i];
                
                $html .= '<tr>';
                $html .= '<td>';
                $html .= $this->createMailingLabel($row);
                $html .= '</td>';

                if(($i + 1) < $length)
                {
                    $row = $rows[$i + 1];
                    $html .= '<td>';
                    $html .= $this->createMailingLabel($row);
                    $html .= '</td>';
                }

                $html .= '</tr>';
            }
            
            $pagination->currentPage++;
            error_log("currentPage: " . $pagination->currentPage);
            error_log("pageCount: " . $pagination->pageCount);
            if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;

                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
        }

        $html .= '</table>';
        error_log("html: $html");
        $this->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        $this->Output('Mailing Labels.pdf', 'D');    
    }

    private function generateLeagueCertification($data)
    {
        $break = "<br /><br />";
        
        $dataProvider = Yii::app()->session['filtered_data'];
        $lastPage = false;
        $pagination = $dataProvider->pagination;

        while($rows = $dataProvider->getData(true))
        {
            $length = count($rows);
            
            for($i = 0; $i < $length; ++$i)
            {
                $row = $rows[$i];
                $league = League::model()->findByPk($row['cert_number']);

                if(!$league->certified)
                    continue;
                
                //reset font on new iteration because of changing font size below
                $this->SetFont('dejavusans', '', 11, '', true);

                $this->AddPage();
                
                /********** SUB-TITLE ************/
                $html = '<h3>OFFICIAL LEAGUE CERTIFICATION</h3>';

                $this->writeHTMLCell(0, 0, '', 10, $html, 0, 1, 0, true, 'C', true);

                /****** CERTIFICATION NUMBER *********/
                $html = 'TNBA CERTIFICATION NUMBER: ' . $league->cert_number;

                $this->writeHTMLCell(0, 0, 140, 30, $html, 0, 1, 0, true, 'C', true);

                /********TEXT #1 *******/

                $html = 'This Certifies that The Bowling League Known As ' . $league->name .
                        ' is Affiliated with The National Bowling Association through the<br />';

                $this->SetFont('dejavusans', '', 8, '', true);
                $this->writeHTMLCell(100, 0, '', '', $html, 0, 1, 0, true, 'C', true);

                $html = 'for The ' . $league->season . ' Season';

                $this->writeHTMLCell(0, 0, 52, '', $html, 0, 1, 0, true, 'C', true);

                /******** Body *********/

                $html = '<p>This certification is issued with the understanding that all TNBA members of the league have qualified ' .
                        'for TNBA membership and are therefore eligible for all Association Services and High Score Awards upon compliance ' .
                        'with the provisions and requirements of the TNBA and the above-named chartered senate</p>';

                $this->writeHTMLCell(0, 0, '', 70, $html, 0, 1, 0, true, 'L', true);

                $html = 'TNBA rules require seven day notification to your local senate Secretary of all individual games and/or series ' .
                        'scores that may be eligible for TNBA Awards.';

                $this->writeHTMLCell(0, 0, '', 90, $html, 0, 1, 0, true, 'L', true);  

                $this->Image(Yii::app()->basePath.'/../images/logo/logo.jpg', 15, 110, 45, 43, 'JPG', 'http://www.tcpdf.org', '', true, 150, '', false, false, 1, false, false, false);
                

                $html = 'THE NATIONAL BOWLING ASSOCIATION, INCORPORATED';

                $this->writeHTMLCell(0, 0, 82, 110, $html, 0, 1, 0, true, 'L', true);

                /********* Conclusion **********/

                $html =  'President';

                $this->writeHTMLCell(0, 0, 62, 130, $html, 0, 1, 0, true, 'C', true);

                $html =  'EXECUTIVE SECRETARY/TREASURER';

                $this->writeHTMLCell(0, 0, 62, 145, $html, 0, 1, 0, true, 'C', true);
                $this->endPage();
            }

            $pagination->currentPage++;
            error_log("currentPage: " . $pagination->currentPage);
            error_log("pageCount: " . $pagination->pageCount);
            if($pagination->currentPage == $pagination->pageCount - 1)
            {
                //if there's just one page, go ahead and quit
                if($pagination->pageCount == 1)
                    break;

                //had to do this since currentPage won't increment up to the length
                if($lastPage)
                {
                    break;
                }

                $lastPage = true;
            }
        }

        $this->Output('League Certification.pdf', 'D'); 
    }

    private function generateTNBACard($memberID)
    {
        $this->SetFont('dejavusans', '', 6, '', true);
        $this->AddPage();

        $html = $this->createTNBACard($memberID);

        $this->writeHTML($html, true, false, true, false, '');

        $this->endPage();

        $this->Output('TNBA Card.pdf', 'D'); 
    }

    private function generateTNBACards($data)
    {
        $this->SetFont('dejavusans', '', 6, '', true);
        $this->AddPage();

        $break = "<br /><br />";

        $dataProvider = Yii::app()->session['filtered_data'];
        $lastPage = false;
        $pagination = $dataProvider->pagination;

        while($rows = $dataProvider->getData(true))
        {
            if($pagination->currentPage > 0)
                $this->AddPage();

            $length = count($rows);
            error_log("length: $length");
            for($i = 1; $i < $length + 1; ++$i)
            {
                $row = $rows[$i - 1];
                
                $html = $this->createTNBACard($row['id']);

                if(is_null($html))
                    continue;

                $this->writeHTML($html, true, false, true, false, '');
            }

            $this->endPage();

            $pagination->currentPage++;
                error_log("currentPage: " . $pagination->currentPage);
                error_log("pageCount: " . $pagination->pageCount);
                error_log("lastPage: $lastPage");
                
                if($pagination->currentPage == $pagination->pageCount - 1)
                {
                    //if there's just one page, go ahead and quit
                    if($pagination->pageCount == 1)
                        break;

                    //had to do this since currentPage won't increment up to the length
                    if($lastPage)
                    {
                        break;
                    }

                    $lastPage = true;
                }
        }

        $this->Output('TNBA Card.pdf', 'D'); 
    }

    private function createTNBACard($memberID)
    {
        $memberModel = Member::model()->findByPk($memberID);

        $zipcodeModel = Zipcode::model()->findByPk($memberModel->zipcode);
        $membershipModel = Membership::model()->findBySql('SELECT season FROM membership WHERE member_id = :member_id',array(':member_id'=>$memberModel->id));

        if(is_null($membershipModel))
            return null;

        $html = '<table>';
        $html .= '<tr>';
        $html .= '<td width="20%"></td>';
        $html .= '<td>THIS IS TO CERTIFY THAT</td>';
        $html .= '</tr>';
        $html .= '</table>';

        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td width="30%"></td>';
        $html .= '<td width="15%"></td>';
        $html .= '<td width="30%"></td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= "<td>$memberModel->first_name $memberModel->middle_name $memberModel->last_name</td>";
        $html .= '<td></td>';
        $html .= "<td><h3>$memberModel->tnba_number</h3></td>";
        $html .= '</tr>';

        $html .= '<tr><td></td></tr>';
        $html .= '<tr><td></td></tr>';

        $html .= '<tr>';
        $html .= "<td>$memberModel->street_address</td>";
        $html .= '<td></td>';
        $html .= "<td></td>";
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= "<td>$zipcodeModel->city_name, $zipcodeModel->state_name, $memberModel->zipcode</td>";
        $html .= '<td></td>';
        $html .= "<td>$memberModel->senate</td>";
        $html .= '</tr>';
        $html .= '</table>';

        $years = explode('-',$membershipModel->season);

        $html .= '<table bgcolor="#00FF00" cellpadding="10">';
        $html .= '<tr width="100%" >';
        $html .= "<td width=\"10%\"></td><td width=\"48%\">THE NATIONAL BOWLING ASSN., INC.<br />SEASON: SEPTEMBER 1, $years[0] TO AUGUST 31, $years[1]</td>";
        $html .= '</tr>';

        $html .= '</table>';

        $html .= '<table>';
        $html .= '<tr>';
        $html .= '<td width="40%"><i>Gregory P Green Signature</i><br />';
        $html .= 'Gregory P. Green<br />';
        $html .= 'PRESIDENT';
        $html .= '</td>';


        $html .= '<td><i>Annette R. Samuel Signature</i><br />';
        $html .= 'Annette R. Samuel<br />';
        $html .= 'EXEC. SEC\' TREAS.';
        $html .= '</td>';

        $html .= '</tr>';
        $html .= '</table>';

        return $html;
    }

    private function createMailingLabel($row)
    {
        $break = "<br /><br />";

        $html = '<table>';

        $html .= '<tr>';
        $html .= '<td>';
        $html .= $row['first_name'] . ' ' . $row['middle_name'] . ' ' . $row['last_name'];
        $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>';
        $html .= $row['street_address'];
        $html .= '</td>';
        $html .= '</tr>';

        //get city / state for zipcode
        $zipcode = Zipcode::model()->findBySql("SELECT city_name,state_name FROM zipcode WHERE zipcode = :zip",array(':zip' => $row['zipcode']));

        $html .= '<tr>';
        $html .= '<td>';
        $html .= $zipcode['city_name'] . ',' . ' ' . $zipcode['state_name'] . ' ' . $row['zipcode'] . $break;
        $html .= '</td>';
        $html .= '</tr>';



        //$html .= $row['street_address'] . $break;
        //$html .= $row['zipcode'];

        $html .= '</table>';
        return $html;
    }

    private function createAchievementPostcard($row) {

        $break = "<br /><br />";

        //left table
        $html = '';

        $html .= '<table>';

        //title
        $html .= '<tr>';
        $html .= '<td>';
        $html .= "The National Bowling Association, Inc." . $break;
        $html .= "Congratulates" . $break;
        $html .= '</td>';
        $html .= '</tr>';

        //date
        $html .= '<tr>';
        $html .= '<td>';
        $html .= $row['first_name'] . ' ' . $row['middle_name'] . ' ' . $row['last_name'] . $break;
        $html .= '</td>';
        $html .= '</tr>';

        //league name
        $html .= '<tr>';
        $html .= '<td>';
        $html .= $row['league_name'];
        $html .= '</td>';
        $html .= '</tr>';

        //senate
        $html .= '<tr>';
        $html .= '<td>';
        $html .= $row['senate'] . $break;
        $html .= '</td>';
        $html .= '</tr>';

        $html .= '<tr>';
        $html .= '<td>';

        //$awards = AwardsHighAverageBowler::model()->findAllBySql('SELECT honor_score ')
        $html .= $row['honor_score'] . '-' . $this->getJewelryType($row);
        $html .= '</td>';
        $html .= '</tr>';

        $html .= '</table>';

        return $html;
    }

    private function createPriorPostcard($row) {

        $break = "<br /><br />";

        //left table
        $html = '';

        $html .= '<table>';
        //date
        $html .= '<tr>';
        $html .= '<td>';
        $html .= 'Date: ' . date('F d, Y') . $break;
        $html .= '</td>';
        $html .= '</tr>';

        //Dear
        $html .= '<tr>';
        $html .= '<td>';
        $html .= 'Dear ' . $row->member['first_name'] . ' ' . $row->member['middle_name'] . ' ' . $row->member['last_name'] . ':' . $break;
        $html .= '</td>';
        $html .= '</tr>';

        //Text
        $html .= '<tr>';
        $html .= '<td>';
        $html .= 'Congratulations on your recent bowling achievement(s). We have received your High Score ' .
                 'application for the following: ' . $break;
        $html .= '</td>';
        $html .= '</tr>';

        //Score
        $html .= '<tr>';
        $html .= '<td>';
        $html .= $row['honor_score'] . ' on ' . GlobalFunctions::formatDate($row['date_of_performance']) . $break;
        $html .= '</td>';
        $html .= '</tr>';

        //determine if person has a previous award
        $awards = AwardsHighAverageBowler::model()->findAllBySql('SELECT id,season,honor_score,date_of_performance FROM awards_high_average_bowler WHERE member_id = (SELECT id FROM member WHERE tnba_number = :tnbaNumber)',array(':tnbaNumber'=>$row->member['tnba_number']));

        /*
        $command = Yii::app()->db->createCommand();
        $awards = $command->select('*')
        ->from('awards_high_average_bowler as a')
        ->join('member as m','m.id = a.member_id')
        ->where('m.tnba_number = :tnbaNumber',array(':tnbaNumber' => $row['tnba_number']))
        ->queryRow();
        */

        $alreadyReceivedAward = false;
        $num = count($awards);
        $award = null;

        for($i = 0; $i < $num; ++$i)
        {
            $award = $awards[$i];
            error_log("award season: {$award['season']}");
            error_log("row season: {$row['season']}");
            if($award->season == $row['season'] && $award->id != $row['id'])
            {
                $alreadyReceivedAward = true;
                break;
            }
        }

        if($alreadyReceivedAward)
        {
            //Text
            $html .= '<tr>';
            $html .= '<td>';
            $html .= 'Games of "298", "299" & "300" and Series of "600", "700", , "800" awards are presented to an individual
                        <u>ONCE PER SEASON.</u> Our records show that you last received an award for the following game or series: ' . $break;
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= $award->honor_score . ' on ' . GlobalFunctions::formatDate($award->date_of_performance) . $break;
            $html .= '</td>';
            $html .= '</tr>';


        }

        $html .= '</table>';

        return $html;
    }

    private function createCongratulatoryText($row) {

            $break = "<br /><br />";

            //left table
            $html = '';

            $html .= '<table>';
            //date
            $html .= '<tr>';
            $html .= '<td>';
            $html .= 'Date: ' . date('F d, Y') . $break;
            $html .= '</td>';
            $html .= '</tr>';

            //Dear
            $html .= '<tr>';
            $html .= '<td>';
            $html .= 'Dear ' . $row->member['first_name'] . ' ' . $row->member['middle_name'] . ' ' . $row->member['last_name'] . ':' . $break;
            $html .= '</td>';
            $html .= '</tr>';
            
            //Text
            $html .= '<tr>';
            $html .= '<td>';
            $html .= 'Congratulations on your recent bowling achievement(s). We have received your High Score ' .
                     'application for the following: ' . $break;
            $html .= '</td>';
            $html .= '</tr>';

            //Score
            $html .= '<tr>';
            $html .= '<td>';
            $html .= $row['honor_score'] . ' on ' . GlobalFunctions::formatDate($row['date_of_performance']) . $break;
            $html .= '</td>';
            $html .= '</tr>';

            //Remaining Text
            $html .= '<tr>';
            $html .= '<td>';
            $html .= "The award item for your game will be ordered as soon as possible and forwarded to you upon receipt. 
                        Best wishes for continued good bowling" . $break;
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= "Sincerely," . $break;
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td>';
            $html .= "TNBA Awards Department" . $break;
            $html .= '</td>';
            $html .= '</tr>';

            $html .= '</table>';

            return $html;
    }

    private function getJewelryType($row) {


        return ((!is_null($row['pin_type'])) ? $row['pin_type'] : $row['ring_type'] . ' Ring size ' . $row['ring_size']);
    }

    private function getMemberAwardInfo($data) {

        $rows = array();
       
        //get user info for awards
        foreach($data as $key => $value)
        {
            $command = Yii::app()->db->createCommand();
            $row = $command->select('m.tnba_number,a.id,l.name as league_name,first_name,middle_name,last_name,date_of_performance,honor_score,season,senate,pin_type,ring_type,ring_size')
            ->from('awards_high_average_bowler as a')
            ->join('member as m','m.id = a.member_id')
            ->join('honor_score as h','a.honor_score = h.name')
            ->join('league as l','l.cert_number = a.league_cert_number')
            ->join('jewelry_type as j','h.jewelry_id = j.id')
            ->where('a.member_id = m.id AND a.id = :a_id',array(':a_id' => $key))
            ->queryRow();
            error_log(print_r($row,true));
            array_push($rows,$row);
        }

        return $rows;
    }

    private function getMemberInfo($pks) {

        $rows = array();

        foreach($pks as $key => $value)
        {
            $member = Member::model()->findByPk($key);
            array_push($rows,$member);
        }

        return $rows;
    }

    private function formatDate($date) {

        $date = explode('-',$date);
        error_log(print_r($date,true));
        $year = $date[0];
        $month = $date[1];
        $day = $date[2];

        return "$month-$day-$year";
    }
}
?>
