<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php /*Yii::app()->bootstrap->register();*/ ?>
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'Register', 'url'=>array('/crud/member/create'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Find A Member', 'url'=>array('/crud/member/findamember')),
                array('label'=>'Manage Members', 'url'=>array('/crud/member/admin'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Manage Memberships', 'url'=>array('/crud/membership/admin'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Manage Awards', 'url'=>array('/crud/awards/admin'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Manage Leagues', 'url'=>array('/crud/league/admin'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Manage Bowlers', 'url'=>array('/stats/statsHistory'), 'visible'=>!Yii::app()->user->isGuest),
                //array('label'=>'Manage Events', 'url'=>array('/manage/events'), 'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container" id="page">

	
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

    <!--
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div>--><!-- footer -->

</div><!-- page -->

</body>
</html>
