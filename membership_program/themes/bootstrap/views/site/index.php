<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

Yii::app()->clientScript->registerCoreScript('jquery');

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/jq/plugins/nivo_slider/nivo-slider.css');

Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jq/plugins/nivo_slider/jquery.nivo.slider.pack.js');
?>

<?php $this->beginWidget('bootstrap.widgets.TbHeroUnit',array(
    'heading'=>'Welcome to '.CHtml::encode(Yii::app()->name),
)); ?>

<script type="text/javascript">
$(window).load(function() {
    $('#slider').nivoSlider();
});
</script>

<div id="slider" class="nivoSlider">
    <img src=<?php echo Yii::app()->baseUrl;?>/images/home_page/slider/battleofthesexes.jpg alt="" />
    <img src=<?php echo Yii::app()->baseUrl;?>/images/home_page/slider/honorhistory.jpg alt="" title="#htmlcaption" />
    <img src=<?php echo Yii::app()->baseUrl;?>/images/home_page/slider/events.jpg alt="" title="This is an example of a caption" />
</div>
<div id="htmlcaption" class="nivo-html-caption">
    <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>.
</div>

<?php $this->endWidget(); ?>
