<?php

function retrieveLocalConfig()
{
	$connectionString = 'mysql:host=localhost;dbname=membership_program';
	$userName = 'root';
	$password = 'root';
	
	return array(
		'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
		'name'=>'TNBA Membership Program',

		// preloading 'log' component
		'preload'=>array(
			'log',
			'bootstrap'
			),

		// autoloading model and component classes
		'import'=>array(
			'application.models.*',
			'application.components.*',
			'bootstrap.widgets.*',
			'editable.*',
			'echmultiselect.*'
		),

		'modules'=>array(
			// uncomment the following to enable the Gii tool
			
			'gii'=>array(
				'class'=>'system.gii.GiiModule',
				'password'=>'gii',
				// If removed, Gii defaults to localhost only. Edit carefully to taste.
				'ipFilters'=>array('127.0.0.1','::1'),
				'generatorPaths'=>array(
	                'bootstrap.gii',
	            )
			),
			
		),

		'theme'=>'bootstrap', // requires you to copy the theme under your themes directory
	  
		// application components
		'components'=>array(
			'user'=>array(
				// enable cookie-based authentication
				'allowAutoLogin'=>true,
			),
			'bootstrap'=>array(
	        	'class'=>'ext.bootstrap.components.Bootstrap',
	        	'coreCss' => true,
	        	'responsiveCss' => true,
	        	'yiiCss' => true,
	        ),
	        'editable' => array(
	            'class'     => 'editable.EditableConfig',
	            'form'      => 'bootstrap',        //form style: 'bootstrap', 'jqueryui', 'plain' 
	            'mode'      => 'popup',            //mode: 'popup' or 'inline'  
	            'defaults'  => array(              //default settings for all editable elements
	               'emptytext' => 'Click to edit'
	            )
        	),
			// uncomment the following to enable URLs in path-format
			
			'urlManager'=>array(
				'urlFormat'=>'path',
				'rules'=>array(
					'<controller:\w+>/<id:\d+>'=>'<controller>/view',
					'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
					'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				),
			),
			
			// uncomment the following to use a MySQL database
			/*
			'db'=>array(
				'connectionString' => 'mysql:host=localhost;dbname=tqtnbain_mempro',
				'emulatePrepare' => true,
				'username' => 'tqtnbain_trey',
				'password' => 'bl1ue2ho3',
				'charset' => 'utf8',
			),
			*/
			'db'=>array(
				'connectionString' => $connectionString,
				'emulatePrepare' => true,
				'username' => $userName,
				'password' => $password,
				'charset' => 'utf8',
			),
			
			'errorHandler'=>array(
				// use 'site/error' action to display errors
				'errorAction'=>'site/error',
			),
			'log'=>array(
				'class'=>'CLogRouter',
				'routes'=>array(
					array(
						'class'=>'CFileLogRoute',
						'levels'=>'trace, info, error, warning',  
					),
					// uncomment the following to show log messages on web pages
					
					array(
						'class' => 'CWebLogRoute',
                        'enabled' =>YII_DEBUG_SHOW_PROFILER,
                        'categories' => 'system.db.*',
					),
					
				),
			),
		),

		// application-level parameters that can be accessed
		// using Yii::app()->params['paramName']
		'params'=>array(
			// this is used in contact page
			'adminEmail'=>'webmaster@example.com',
		),
	);
}
?>