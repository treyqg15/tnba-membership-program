<?php
// Define a path alias for the Bootstrap extension as it's used internally.
// In this example we assume that you unzipped the extension under protected/extensions.
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/bootstrap');
Yii::setPathOfAlias('yiistrap', dirname(__FILE__).'/../extensions/yiistrap');
Yii::setPathOfAlias('editable', dirname(__FILE__).'/../extensions/x-editable');
Yii::setPathOfAlias('echmultiselect', dirname(__FILE__).'/../extensions/echmultiselect');

//include Helper.php
require_once( dirname(__FILE__) . '/../components/Helper.php');

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

//constants for switching DB info
define('DEV_DB',true);

if(DEV_DB)
{
	require('localConfig.php');
	return retrieveLocalConfig();
}
else
{
	require('bluehostConfig.php');
	return retrieveBluehostConfig();
}

