<?php

class Helper
{
	public static function generateTNBANumber() {

		$digits = 5;
		$tnbaNumber = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
		error_log("generated tnba_number: $tnbaNumber");
		if(!Helper::TNBANumberExists($tnbaNumber))
		{
			error_log("number doesn't exist. Assigning it");
			return $tnbaNumber;
		}
		else
		{	
			error_log("number already exists.");
			return Helper::generateTNBANumber();		
		}

	}	

	public static function TNBANumberExists($tnbaNumber) {

		$exists = Member::model()->findBySql("select id from member where tnba_number = :tnbaNumber",array(':tnbaNumber' => $tnbaNumber));

		if(is_null($exists))
		{
			error_log("TNBA # ($tnbaNumber) does not exist");
			return false;
		}

		error_log("TNBA # ($tnbaNumber) does exist");
		return true;
	}

	public static function autoComplete($term) {

		error_log("autocomplete");
		$connection=Yii::app()->db;   // assuming you have configured a "db" connection
		// If not, you may explicitly create a connection:
		// $connection=new CDbConnection($dsn,$username,$password);
		error_log("SELECT zipcode FROM zipcode WHERE zipcode LIKE '".$term."%'");
		$command=$connection->createCommand("SELECT zipcode FROM zipcode WHERE zipcode LIKE '".$term."%'");
		$dataReader=$command->query();
		$list = array();

		foreach($dataReader as $row) 
		{ 
			$data['value'] = $row['zipcode'];
			$data['label'] = $row['zipcode'];

			$list[] = $data;

			unset($data);
		}

		return $list;
	}

	public static function runReport($grid,$reportType,$data)
	{
		require_once(Yii::app()->basePath.'/../php/export/Export.php');
		require_once(Yii::app()->basePath.'/../php/tcpdf/TNBAPDF.php');

		define('MEMBER_GRID','MEMBER');
		define('MEMBERSHIP_GRID','MEMBERSHIP');
		define('AWARD_GRID','AWARD');
		define('LEAGUE_GRID','LEAGUE');
		define('STATSHISTORY_GRID','STATSHISTORY');

		switch($grid)
		{
			case MEMBER_GRID:
				switch($reportType)
				{
					case "EXPORT ALL":
						Export::init($grid);
						Export::exportAllMembersToCSV();
						break;
					case "EXPORT SELECTED ROWS":
						Export::init($grid);
						Export::exportSelectedRowsForMembers($data);
						break;
					case "GENERATE MAILING LABELS":
						$pdf = new TNBAPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
						$pdf->startGeneration($reportType,$data);
						break;
					case "GENERATE TNBA CARDS":
						$pdf = new TNBAPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
						$pdf->startGeneration($reportType,$data);
						break;
				}
				break;
			case MEMBERSHIP_GRID:
				switch($reportType)
				{
					case "EXPORT ALL":
						Export::init($grid);
						Export::exportAllMembershipsToCSV();
						break;
					case "EXPORT SELECTED ROWS":
						Export::init($grid);
						Export::exportSelectedRowsForMemberships($data);
						break;
				}
				break;
			case AWARD_GRID:
				switch($reportType)
				{
					case "EXPORT ALL":
						Export::init($grid);
						Export::exportAllAwardsToCSV();
						break;
					case "EXPORT SELECTED ROWS":
						Export::init($grid);
						Export::exportSelectedRowsForAwards($data);
						break;
					default:
						// create new PDF document
						$pdf = new TNBAPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
						$pdf->startGeneration($reportType,$data);
				}
				break;
			case LEAGUE_GRID:
				switch($reportType)
				{
					case "EXPORT ALL":
						Export::init($grid);
						Export::exportAllLeaguesToCSV();
						break;
					case "EXPORT SELECTED ROWS":
						Export::init($grid);
						Export::exportSelectedRowsForLeagues($data);
						break;
					case "LEAGUE CERTIFICATION":
						$pdf = new TNBAPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
						$pdf->startGeneration($reportType,$data);
				}
				break;
			case STATSHISTORY_GRID:
				switch($reportType)
				{
					case "EXPORT ALL":
						Export::init($grid);
						Export::exportAllStatshistoryToCSV();
						break;
					case "EXPORT SELECTED ROWS":
						Export::init($grid);
						Export::exportSelectedRowsForStatshistory($data);
						break;
				}
				break;
		}
	}

	public static function fetchMemberByTNBANumber($tnbaNumber) {

		$member = Member::model()->findBySql("select * from member where tnba_number = :tnbaNumber",array(':tnbaNumber' => $tnbaNumber));

		if(is_null($member))
		{
			error_log("Member does not exist");
			return null;
		}

		error_log("Member exists");
		return $member;
	}

	public static function fetchBowlingAlleysForZipcode($zipcode) {

		return BowlingAlley::model()->findAllBySql("SELECT id,name FROM bowling_alley WHERE zipcode = $zipcode");
	}

	public static function fetchLeaguesForBowlingAlley($id) {

		return League::model()->findAllBySql("SELECT cert_number,name FROM league WHERE bowling_alley_id = :id", array(':id' => $id));
	}

	public static function retrieveAllMembers() {

		$members = Member::model()->findAll();

		return $members;
	}

	public static function retrieveAllSeasons() {

		$seasons = Season::model()->findAll();

		return $seasons;
	}

	public static function retrieveAllCategories() {

		$categories = Category::model()->findAll();

		return $categories;
	}

	public static function retrieveAllSexes() {

		$sexes = Sex::model()->findAll();

		return $sexes;
	}

	public static function retrieveAllSenates() {

		$senates = Senate::model()->findAll();

		return $senates;
	}

	public static function retrieveAllRegions() {

		$regions = Region::model()->findAll();

		return $regions;
	}

	public static function retrieveAllStatuses() {

		$statuses = MembershipStatus::model()->findAll();

		return $statuses;
	}

	public static function retrieveAllTypes() {

		$types = MembershipType::model()->findAll();

		return $types;
	}

	public static function retrieveAllAwardStatuses() {

		$awardStatuses = AwardsStatus::model()->findAll();

		return $awardStatuses;
	}

	public static function retrieveAllHonorScores() {

		$honorScores = HonorScore::model()->findAll();

		return $honorScores;
	}

	public static function retrieveBowlingAlleysForZipcode($zipcode) {

		$bowlingAlleys = BowlingAlley::model()->findAllBySql("SELECT id,name FROM bowling_alley WHERE zipcode = :zip",array(':zip' => $zipcode));

		return $bowlingAlleys;
	}

	public static function retrieveLeagueForBowlingAlley($bowling_alley_id) {

		$leagues = League::model()->findAllBySql("SELECT cert_number,name FROM league WHERE bowling_alley_id = :id",array(':id' => $bowling_alley_id));

		return $leagues;
	}

	public static function retrieveBowlingAlleysForA() {

		$bowlingAlleys = BowlingAlley::model()->findAll();

		return $bowlingAlleys;
	}

	public static function retrieveRolesForMember($memberID) {

		$roles = RoleMemberId::model()->findAllBySql("SELECT * FROM role_member_id WHERE member_id = :memberID", array(":memberID" => $memberID));

		return $roles;
	}
	
	public static function buildSeasonDropdown($seasons) {

		$num = count($seasons);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$season = $seasons[$i];
			$html .= "<option>" . $season->name . "</option>";
		}

		return $html;
	}

	public static function buildCategoriesDropdown($categories) {

		$num = count($categories);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$category = $categories[$i];
			$html .= "<option>" . $category->type . "</option>";
		}

		return $html;
	}	

	public static function buildSexesDropdown($sexes) {

		$num = count($sexes);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$sex = $sexes[$i];
			$html .= "<option>" . $sex->type . "</option>";
		}

		return $html;
	}

	public static function buildSenatesDropdown($senates) {

		$num = count($senates);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$senate = $senates[$i];
			$html .= "<option>" . $senate->name . "</option>";
		}

		return $html;
	}

	public static function buildRegionsDropdown($regions) {

		$num = count($regions);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$region = $regions[$i];
			$html .= "<option>" . $region->name . "</option>";
		}

		return $html;
	}

	public static function buildStatusesDropdown($statuses) {

		$num = count($statuses);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$status = $statuses[$i];
			$html .= "<option>" . $status->status . "</option>";
		}

		return $html;
	}

	public static function buildTypesDropdown($types) {

		$num = count($types);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$type = $types[$i];
			$html .= "<option>" . $type->type . "</option>";
		}

		return $html;
	}

	public static function buildAwardStatusesDropdown($awardStatuses) {

		$num = count($awardStatuses);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$awardStatus = $awardStatuses[$i];
			$html .= "<option>" . $awardStatus->status . "</option>";
		}

		return $html;
	}

	public static function buildHonorScoresDropdown($honorScores) {

		$num = count($honorScores);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$honorScore = $honorScores[$i];
			$html .= "<option>" . $honorScore->name . "</option>";
		}

		return $html;
	}

	public static function buildBowlingAlleysDropdown($bowlingAlleys) {

		$num = count($bowlingAlleys);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$bowlingAlley = $bowlingAlleys[$i];
			$html .= "<option value=\"".$bowlingAlley->id."\">" . $bowlingAlley->name . "</option>";
		}

		return $html;
	}

	public static function buildLeaguesDropdown($leagues) {

		$num = count($leagues);
		$html = "";

		for($i = 0; $i < $num; ++$i)
		{
			$league = $leagues[$i];
			$html .= "<option value=\"".$league->cert_number."\">" . $league->name . "</option>";
		}

		return $html;
	}
}

?>