<?php

class Error
{
	private $message;
	private $errorCodes;

	function __construct()
	{
		$this->message = array();
		$this->errorCodes = array(
			'1054' => 'Server Error', //Message: Column not found
			'1062' => 'A record with this information already exists', //Message: Duplicate entry '%s' for key %d
			'1064' => 'Server Error', //Message: You have an error in your SQL syntax
			'1451' => 'Server Error', //Message: Integrity constraint violation: 1451 Cannot delete or update a parent row: a foreign key constraint fails
			'1452' => 'Record does not exist', //Message: Cannot add or update a child row: a foreign key constraint fails (%s)
			
		);
	}

	public function addMessage($message)
	{
		$bt =  debug_backtrace();
		$fileName = $bt[0]['file'];
		$lineNum = $bt[0]['line'];

		//$this->message[] = "In file $fileName($lineNum): " . $message;
		$this->message[] = $message;
	}

	public function retrieveMessageForErrorCode($code)
	{
		if(array_key_exists($code, $this->errorCodes))
			return $this->errorCodes[$code];
		else
		{
			$bt =  debug_backtrace();
			$fileName = $bt[0]['file'];
			$lineNum = $bt[0]['line'];
			error_log("new error code: $code from $fileName($lineNum)");
		}
	}

	public function hasMessage()
	{
		if(count($this->message) > 0)
			return true;
		return false;
	}

	//have to convert Error object to array to json_encode it
	public function toEncodableObject()
	{
		$error = new stdClass();

		$error->messages = array();

		$numOfMessages = count($this->message);

		for($i = 0; $i < $numOfMessages; ++$i)
			$error->messages[] = $this->message[$i];
		
		return $error;
	}

	public function buildMessageFromArray($arr)
	{
		foreach($arr as $key => $messages)
		{
			foreach($messages as $key => $message)
			{
				$this->addMessage($message);
			} 
		}
	}
}

?>