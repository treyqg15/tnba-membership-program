<?php

class Response
{
	public $error;
	public $data;
	public $message;

	function __construct()
	{
		$this->error = null;
		$this->data = null;
		$this->message = null;
	}

	public function toEncodableObject()
	{
		$response = new stdClass();
		$response->error = $this->error;
		$response->data = $this->data;
		$response->message = $this->message;
		
		return $response;
	}
}

?>