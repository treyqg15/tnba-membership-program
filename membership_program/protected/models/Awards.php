<?php

/**
 * This is the model class for table "awards".
 *
 * The followings are the available columns in table 'awards':
 * @property integer $id
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $street_address
 * @property string $zipcode
 * @property string $tnba_number
 * @property string $usbc_number
 * @property string $sex
 * @property string $league_cert_number
 * @property string $tournament_cert_number
 * @property string $date_of_performance
 * @property integer $certified
 * @property double $current_average
 * @property string $name_of_lanes
 * @property string $lane_cert
 * @property string $name_of_senate
 * @property string $honor_score
 *
 * The followings are the available model relations:
 * @property Zipcode $zipcode0
 * @property Sex $sex0
 * @property League $leagueCertNumber
 * @property Tournament $tournamentCertNumber
 * @property HonorScore $honorScore
 */
class Awards extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'awards';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, last_name, street_address, zipcode, tnba_number, sex, date_of_performance, current_average, honor_score', 'required'),
			array('id, certified', 'numerical', 'integerOnly'=>true),
			array('current_average', 'numerical'),
			array('first_name, middle_name, last_name', 'length', 'max'=>50),
			array('street_address', 'length', 'max'=>100),
			array('zipcode, league_cert_number, tournament_cert_number, lane_cert', 'length', 'max'=>10),
			array('tnba_number, sex', 'length', 'max'=>6),
			array('usbc_number', 'length', 'max'=>9),
			array('name_of_lanes, honor_score', 'length', 'max'=>30),
			array('name_of_senate', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, first_name, middle_name, last_name, street_address, zipcode, tnba_number, usbc_number, sex, league_cert_number, tournament_cert_number, date_of_performance, certified, current_average, name_of_lanes, lane_cert, name_of_senate, honor_score', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'zipcode0' => array(self::BELONGS_TO, 'Zipcode', 'zipcode'),
			'sex0' => array(self::BELONGS_TO, 'Sex', 'sex'),
			'leagueCertNumber' => array(self::BELONGS_TO, 'League', 'league_cert_number'),
			'tournamentCertNumber' => array(self::BELONGS_TO, 'Tournament', 'tournament_cert_number'),
			'honorScore' => array(self::BELONGS_TO, 'HonorScore', 'honor_score'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'first_name' => 'First Name',
			'middle_name' => 'Middle Name',
			'last_name' => 'Last Name',
			'street_address' => 'Street Address',
			'zipcode' => 'Zipcode',
			'tnba_number' => 'Tnba Number',
			'usbc_number' => 'Usbc Number',
			'sex' => 'Sex',
			'league_cert_number' => 'League Cert Number',
			'tournament_cert_number' => 'Tournament Cert Number',
			'date_of_performance' => 'Date Of Performance',
			'certified' => 'Certified',
			'current_average' => 'Current Average',
			'name_of_lanes' => 'Name Of Lanes',
			'lane_cert' => 'Lane Cert',
			'name_of_senate' => 'Name Of Senate',
			'honor_score' => 'Honor Score',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('street_address',$this->street_address,true);
		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('tnba_number',$this->tnba_number,true);
		$criteria->compare('usbc_number',$this->usbc_number,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('league_cert_number',$this->league_cert_number,true);
		$criteria->compare('tournament_cert_number',$this->tournament_cert_number,true);
		$criteria->compare('date_of_performance',$this->date_of_performance,true);
		$criteria->compare('certified',$this->certified);
		$criteria->compare('current_average',$this->current_average);
		$criteria->compare('name_of_lanes',$this->name_of_lanes,true);
		$criteria->compare('lane_cert',$this->lane_cert,true);
		$criteria->compare('name_of_senate',$this->name_of_senate,true);
		$criteria->compare('honor_score',$this->honor_score,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Awards the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
