<?php

/**
 * This is the model class for table "stats_history".
 *
 * The followings are the available columns in table 'stats_history':
 * @property integer $bowler_member_id
 * @property double $average
 * @property string $league_cert_number
 * @property string $season
 *
 * The followings are the available model relations:
 * @property League $leagueCertNumber
 * @property Member $bowlerMember
 * @property League $season0
 */
class StatsHistory extends CActiveRecord
{
	public $first_name;
	public $middle_name;
	public $last_name;
	public $tnba_number;
	public $usbc_number;
	public $league_name;
	public $bowling_alley_name;
	public $bowling_alley_zipcode;
	public $isUpdate = false; //used to detect if save() call on this model is for new entry or an update

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stats_history';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bowler_member_id, average, league_cert_number, season', 'required'),
			array('bowler_member_id, num_of_games', 'numerical', 'integerOnly'=>true),
			array('average', 'numerical'),
			array('league_cert_number', 'length', 'max'=>10),
			array('season', 'length', 'max'=>9),
			array('season', 'doesRecordExist'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bowler_member_id, average, league_cert_number, season, first_name, middle_name, last_name, tnba_number, usbc_number, league_name, bowling_alley_name, bowling_alley_zipcode, num_of_games', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'league' => array(self::BELONGS_TO, 'League', 'league_cert_number'),
			'member' => array(self::BELONGS_TO, 'Member', 'bowler_member_id'),
			'season' => array(self::BELONGS_TO, 'League', 'season'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bowler_member_id' => 'Bowler Member',
			'average' => 'Average',
			'league_cert_number' => 'League Cert Number',
			'season' => 'Season',
			'num_of_games' => '# of Games'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		//$criteria->together = true;

		$criteria->with = array('member','league','league.bowlingAlley');

		//member criteria
		$criteria->compare('member.first_name',$this->first_name,true);
		$criteria->compare('member.middle_name',$this->middle_name,true);
		$criteria->compare('member.last_name',$this->last_name,true);
		$criteria->compare('member.tnba_number',$this->tnba_number,true);
		$criteria->compare('member.usbc_number',$this->usbc_number,true);

		//league criteria
		$criteria->compare('league.name',$this->league_name,true);

		//bowlingAlley criteria
		$criteria->compare('bowlingAlley.name',$this->bowling_alley_name,true);
		$criteria->compare('bowlingAlley.zipcode',$this->bowling_alley_zipcode,true);

		$criteria->compare('bowler_member_id',$this->bowler_member_id);
		$criteria->compare('average',$this->average);
		$criteria->compare('league.season',$this->season,true);
		$criteria->compare('num_of_games',$this->num_of_games,true);

		Yii::app()->session['filtered_data'] = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

		error_log(print_r(Yii::app()->session['filtered_data']->modelClass,true));

		return Yii::app()->session['filtered_data'];
	}


	public function retrieveStatsHistoryByTNBANumber($tnbaNumber)
	{
		$criteria = new CDbCriteria;
		$criteria->together = true;

		$criteria->with = array('member','league','league.bowlingAlley');
		$criteria->condition='tnba_number=:tnbaNumber'; 
		$criteria->params=array(':tnbaNumber'=>$tnbaNumber);

		//member attributes
		$criteria->compare('member.first_name',$this->first_name,true);
		$criteria->compare('member.middle_name',$this->middle_name,true);
		$criteria->compare('member.last_name',$this->last_name,true);

		//league criteria
		$criteria->compare('league.name',$this->league_name,true);

		//bowlingAlley Criteria
		$criteria->compare('bowlingAlley.name',$this->bowling_alley_name,true);

		$criteria->compare('bowler_member_id',$this->bowler_member_id);
		$criteria->compare('season',$this->season,true);
		$criteria->compare('num_of_games',$this->num_of_games,true);

		error_log(print_r($criteria,true));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StatsHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function doesRecordExist($attribute,$params)
	{
		if($this->isUpdate)
			return;

		error_log("average: " . $this->average);
		error_log("member_id: " . $this->bowler_member_id);
		error_log("cert_number: " . $this->league_cert_number);
		error_log("season: " . $this->season);

		$statsHistory = StatsHistory::model()->findBySql("SELECT * FROM stats_history 
														  WHERE season =:season 
														  AND bowler_member_id =:member_id
														  AND league_cert_number =:cert_number",array(':season'=>$this->season,
														  											  ':member_id'=>$this->bowler_member_id,
														  											  ':cert_number'=>$this->league_cert_number));
		

		$member = Member::model()->findByPk($this->bowler_member_id);
		$league = League::model()->findByPk($this->league_cert_number);

		//error_log(print_r($statsHistory,true));
		if(!is_null($statsHistory))
			$this->addError("season","An entry for the season {$this->season} for member {$member->first_name} {$member->middle_name} {$member->last_name} already exists");
	}
}
