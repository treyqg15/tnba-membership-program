<?php

/**
 * This is the model class for table "awards_games".
 *
 * The followings are the available columns in table 'awards_games':
 * @property integer $id
 * @property integer $games_1
 * @property integer $games_2
 * @property integer $games_3
 * @property integer $total
 * @property integer $isSingleGame
 * @property integer $awards_id
 *
 * The followings are the available model relations:
 * @property AwardsHighAverageBowler $awards
 */
class AwardsGames extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'awards_games';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('games_1, games_2, games_3, total, isSingleGame, singleGame, awards_id', 'numerical', 'integerOnly'=>true),
			array('isSingleGame','validateGames'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, games_1, games_2, games_3, total, isSingleGame, awards_id', 'safe', 'on'=>'search'),
		);
	}

	public function validateGames($attribute)
	{
		if($this->isSingleGame)
		{
			if(is_null($this->singleGame) || empty($this->singleGame))
			{
				$this->addError($attribute,"Please enter a value for the game");
			}
		}
		else
		{
			if(is_null($this->games_1) || empty($this->games_1))
			{
				$this->addError("games_1","Please enter a value for GAME 1");
			}

			if(is_null($this->games_2) || empty($this->games_2))
			{
				$this->addError("games_2","Please enter a value for GAME 2");
			}

			if(is_null($this->games_3) || empty($this->games_3))
			{
				$this->addError("games_3","Please enter a value for GAME 3");
			}

			if(is_null($this->total) || empty($this->total))
			{
				$this->addError("total","Please enter a value for TOTAL");
			}
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'awards' => array(self::BELONGS_TO, 'AwardsHighAverageBowler', 'awards_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'games_1' => 'Games 1',
			'games_2' => 'Games 2',
			'games_3' => 'Games 3',
			'total' => 'Total',
			'isSingleGame' => 'Is Single Game',
			'awards_id' => 'Awards',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('games_1',$this->games_1);
		$criteria->compare('games_2',$this->games_2);
		$criteria->compare('games_3',$this->games_3);
		$criteria->compare('total',$this->total);
		$criteria->compare('isSingleGame',$this->isSingleGame);
		$criteria->compare('singleGame',$this->singleGame);
		$criteria->compare('awards_id',$this->awards_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AwardsGames the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
