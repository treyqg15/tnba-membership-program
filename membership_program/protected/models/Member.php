<?php

/**
 * This is the model class for table "member".
 *
 * The followings are the available columns in table 'member':
 * @property integer $id
 * @property string $creation_time
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $email
 * @property string $category_type
 * @property integer $role_id
 * @property string $usbc_number
 * @property string $tnba_number
 * @property string $e_signature
 * @property string $birthday
 * @property string $sex
 * @property string $street_address
 * @property string $zipcode
 * @property string $phone_number
 * @property string $senate
 * @property string $senate_region
 * @property string $w_9_filepath
 *
 * The followings are the available model relations:
 * @property Role $role
 * @property Zipcode $zipcode0
 * @property Senate $senate0
 * @property Region $senateRegion
 * @property Category $categoryType
 * @property Membership $membership
 */
class Member extends CActiveRecord
{
	public $role_name;
	public $r_member_id;
	public $r_role_id;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('first_name, last_name, category_type, birthday, sex, street_address, zipcode, senate, senate_region, tnba_number, certified', 'required'),
			array('first_name, middle_name, last_name, email', 'length', 'max'=>50),
			array('category_type', 'length', 'max'=>12),
			array('tnba_number, sex', 'length', 'max'=>6),
			array('usbc_number', 'length','max'=>9),
			array('e_signature, street_address', 'length', 'max'=>100),
			array('zipcode, phone_number', 'length', 'max'=>10),
			array('senate', 'length', 'max'=>30),
			array('senate_region', 'length', 'max'=>8),
			array('certified', 'length', 'max'=>1),
			array('w_9_id', 'numerical', 'integerOnly'=>true),
			array('tnba_number, usbc_number','unique','message'=>'{attribute}:{value} already exists!'),
			//array('zipcode','exist','allowEmpty'=>false),
			array('zipcode','validateZipcode'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, creation_time, first_name, middle_name, last_name, email, category_type, role_id, usbc_number, tnba_number, e_signature, birthday, sex, street_address, zipcode, phone_number, senate, senate_region, w_9_id, role_name, r_role_id, certified', 'safe', 'on'=>'search'),
		);
	}

	public function validateZipcode($attribute)
	{
		error_log("validating zipcode: {$this->zipcode}");
		$zipcode = Zipcode::model()->findBySql("SELECT 1 FROM zipcode WHERE zipcode = :zip", array(':zip' => $this->zipcode));

		if(is_null($zipcode))
			$this->addError('zipcode',"{$this->zipcode} is an invalid Zipcode");
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'role' => array(self::BELONGS_TO, 'Role', 'role_id'),
			'zipcode0' => array(self::BELONGS_TO, 'Zipcode', 'zipcode'),
			'senate0' => array(self::BELONGS_TO, 'Senate', 'senate'),
			'senateRegion' => array(self::BELONGS_TO, 'Region', 'senate_region'),
			'categoryType' => array(self::BELONGS_TO, 'Category', 'category_type'),
			'w9' => array(self::BELONGS_TO, 'W9', 'w_9_id'),
			'membership' => array(self::HAS_ONE, 'Membership', 'member_id'),
			//'role_member_id' => array(self::HAS_ONE, 'RoleMemberId', 'member_id'),
			'roles'=>array(self::MANY_MANY, 'Role',
                'role_member_id(member_id, role_id)'),
			'leagues'=>array(self::MANY_MANY, 'League',
                'league_member(league_cert_number, member_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'creation_time' => 'Creation Time',
			'first_name' => 'First Name',
			'middle_name' => 'Middle Name',
			'last_name' => 'Last Name',
			'email' => 'Email',
			'category_type' => 'Category Type',
			'role_id' => 'Role',
			'usbc_number' => 'Usbc Number',
			'tnba_number' => 'Tnba Number',
			'e_signature' => 'E Signature',
			'birthday' => 'Birthday',
			'sex' => 'Sex',
			'street_address' => 'Street Address',
			'zipcode' => 'Zipcode',
			'phone_number' => 'Phone Number',
			'senate' => 'Senate',
			'senate_region' => 'Senate Region',
			'w_9_id' => 'W 9',
		);
	}

	protected function beforeSave()
    {
    	error_log("before save");
    	
        if($file=CUploadedFile::getInstance($this,'w_9_id'))
        {

            error_log("filename: " . $file->name);
            error_log("filetype: " . $file->type);
            error_log("filesize: " . $file->size);
            //error_log("filecontent: " . $file->tempName);
            Yii::app()->db->createCommand()->insert('w_9', array(
			    'file_name'=>$file->name,
			    'file_type'=>$file->type,
			    'file_size'=>$file->size,
			    'file_content'=>file_get_contents($file->tempName)
			));

			$this->w_9_id = Yii::app()->db->getLastInsertID();
        }
		
		
		return parent::beforeSave();
    }

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		/*
		$criteria->with = array('roles' => array(
				'alias' => 's',
				'on' => 'roles.role_id = 1',
				//'together' => true
			)
		);
		*/

		$criteria->with = array('roles');
		$criteria->together = true;
		//$criteria->join = 'INNER JOIN role_member_id on t.id';

		$criteria->compare('id',$this->id);
		$criteria->compare('creation_time',$this->creation_time,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('t.certified',$this->certified,true);
		$criteria->compare('category_type',$this->category_type,true);
		//$criteria->compare('role_id',$this->role_id);
		$criteria->compare('usbc_number',$this->usbc_number,true);
		$criteria->compare('tnba_number',$this->tnba_number,true);
		$criteria->compare('e_signature',$this->e_signature,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('street_address',$this->street_address,true);
		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('senate',$this->senate,true);
		$criteria->compare('senate_region',$this->senate_region,true);
		$criteria->compare('w_9_id',$this->w_9_id,true);

		//roles criteria
		$criteria->compare('roles.member_id',$this->r_member_id);

		//$criteria->addSearchCondition('roles.member_id',$this->id);
		//$criteria->compare('r.role_id',$this->r_role_id);

		//role criteria
		//$criteria->compare('role.name',$this->role_name);
		
		error_log(print_r($criteria,true));

		Yii::app()->session['filtered_data'] = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

		return Yii::app()->session['filtered_data'];
	}

	public function retrieveSearchedForMembers()
	{
		$criteria=new CDbCriteria;

		$arr = Yii::app()->session['member_results'];

		$length = count($arr);
		error_log("length: $length");

		if($length == 0)
			$criteria->condition='zipcode=-1';
		for($i = 0; $i < $length; ++$i)
		{
			$member = $arr[$i];
			$key = "tnbaNumber_$i";

			//error_log("tnba #: {$member->tnba_number}");
			//$criteria->condition='tnba_number=:tnbaNumber'; 
			$criteria->addCondition("tnba_number=:$key","OR");
			$criteria->params[":$key"] = $member->tnba_number;
		}

		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('sex',$this->tnba_number,true);
		$criteria->compare('tnba_number',$this->tnba_number,true);
		$criteria->compare('senate',$this->senate,true);
		$criteria->compare('senate_region',$this->senate_region,true);

		//error_log(print_r($criteria,true));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function retrieveMembersByCertNumber()
	{
		$criteria=new CDbCriteria;
		$certNumber = Yii::app()->session['selected_league'];
		//$criteria->with = array('leagues');
		$criteria->addCondition('lm.league_cert_number=:cert_number');
		$criteria->join ='LEFT OUTER JOIN `league_member` `lm` ON (`t`.`id`=`lm`.`member_id`) ';
		$criteria->join .= 'LEFT OUTER JOIN `league` `l` ON (`l`.`cert_number`=`lm`.`league_cert_number`)';
		$criteria->params[":cert_number"] = $certNumber;
		$criteria->together = true;

		//$criteria->join = 'INNER JOIN role_member_id on t.id';

		$criteria->compare('id',$this->id);
		$criteria->compare('creation_time',$this->creation_time,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		//$criteria->compare('t.certified',$this->certified,true);
		$criteria->compare('category_type',$this->category_type,true);
		//$criteria->compare('role_id',$this->role_id);
		$criteria->compare('usbc_number',$this->usbc_number,true);
		$criteria->compare('tnba_number',$this->tnba_number,true);
		$criteria->compare('e_signature',$this->e_signature,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('street_address',$this->street_address,true);
		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('senate',$this->senate,true);
		$criteria->compare('senate_region',$this->senate_region,true);
		$criteria->compare('w_9_id',$this->w_9_id,true);

		//roles criteria
		//$criteria->compare('roles.member_id',$this->r_member_id);

		//$criteria->addSearchCondition('roles.member_id',$this->id);
		//$criteria->compare('r.role_id',$this->r_role_id);

		//role criteria
		//$criteria->compare('role.name',$this->role_name);
		
		error_log(print_r($criteria,true));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function retrieveMembersWithRoles()
	{
		$criteria=new CDbCriteria;
		$certNumber = Yii::app()->session['selected_league'];
		//$criteria->with = array('leagues');
		$criteria->distinct = true;
		$criteria->join ='INNER JOIN `role_member_id` `rm` ON (`t`.`id`=`rm`.`member_id`) ';
		$criteria->join .= 'INNER JOIN `league_member` `lm` ON (`lm`.`member_id`=`t`.`id`)';
		$criteria->addCondition('lm.league_cert_number=:cert_number');
		$criteria->params[":cert_number"] = $certNumber;
		$criteria->together = true;

		//$criteria->join = 'INNER JOIN role_member_id on t.id';

		$criteria->compare('id',$this->id);
		$criteria->compare('creation_time',$this->creation_time,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('certified',$this->certified,true);
		$criteria->compare('category_type',$this->category_type,true);
		//$criteria->compare('role_id',$this->role_id);
		$criteria->compare('usbc_number',$this->usbc_number,true);
		$criteria->compare('tnba_number',$this->tnba_number,true);
		$criteria->compare('e_signature',$this->e_signature,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('street_address',$this->street_address,true);
		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('senate',$this->senate,true);
		$criteria->compare('senate_region',$this->senate_region,true);
		$criteria->compare('w_9_id',$this->w_9_id,true);

		//roles criteria
		//$criteria->compare('roles.member_id',$this->r_member_id);

		//$criteria->addSearchCondition('roles.member_id',$this->id);
		//$criteria->compare('r.role_id',$this->r_role_id);

		//role criteria
		//$criteria->compare('role.name',$this->role_name);
		
		error_log(print_r($criteria,true));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function getMemberRoles()
	{
		$out = CHtml::listData($this->roles,'id','name');
   		return implode(',', $out);
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Member the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
