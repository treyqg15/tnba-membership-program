<?php

/**
 * This is the model class for table "members".
 *
 * The followings are the available columns in table 'members':
 * @property integer $member_id
 * @property string $member_no
 * @property string $confirmation_number
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $e_signature
 * @property string $birth_datestamp
 * @property string $gender
 * @property string $address
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $zip_code
 * @property string $phone_number
 * @property string $email_address
 * @property string $member_type
 * @property string $membership_type
 * @property string $local_senate
 * @property string $senate_region
 * @property string $dues_paid_league
 * @property string $membership_status
 * @property string $notes
 * @property string $application_datestamp
 * @property string $paid_datestamp
 * @property string $type
 */
class Members extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'members';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('member_no, confirmation_number, first_name, last_name, birth_datestamp, gender, address, city, state, country, zip_code, phone_number, email_address, member_type, membership_type, local_senate, senate_region, dues_paid_league, notes, application_datestamp, paid_datestamp, type', 'required'),
			array('member_no, confirmation_number, member_type', 'length', 'max'=>10),
			array('first_name, middle_name, last_name, e_signature, address, city, state, country, zip_code, phone_number, email_address, membership_type, local_senate, dues_paid_league, type', 'length', 'max'=>255),
			array('gender', 'length', 'max'=>6),
			array('senate_region', 'length', 'max'=>8),
			array('membership_status', 'length', 'max'=>7),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('member_id, member_no, confirmation_number, first_name, middle_name, last_name, e_signature, birth_datestamp, gender, address, city, state, country, zip_code, phone_number, email_address, member_type, membership_type, local_senate, senate_region, dues_paid_league, membership_status, notes, application_datestamp, paid_datestamp, type', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'member_id' => 'Member',
			'member_no' => 'Member No',
			'confirmation_number' => 'Confirmation Number',
			'first_name' => 'First Name',
			'middle_name' => 'Middle Name',
			'last_name' => 'Last Name',
			'e_signature' => 'E Signature',
			'birth_datestamp' => 'Birth Datestamp',
			'gender' => 'Gender',
			'address' => 'Address',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'zip_code' => 'Zip Code',
			'phone_number' => 'Phone Number',
			'email_address' => 'Email Address',
			'member_type' => 'Member Type',
			'membership_type' => 'Membership Type',
			'local_senate' => 'Local Senate',
			'senate_region' => 'Senate Region',
			'dues_paid_league' => 'Dues Paid League',
			'membership_status' => 'Membership Status',
			'notes' => 'Notes',
			'application_datestamp' => 'Application Datestamp',
			'paid_datestamp' => 'Paid Datestamp',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('member_id',$this->member_id);
		$criteria->compare('member_no',$this->member_no,true);
		$criteria->compare('confirmation_number',$this->confirmation_number,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('middle_name',$this->middle_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('e_signature',$this->e_signature,true);
		$criteria->compare('birth_datestamp',$this->birth_datestamp,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('zip_code',$this->zip_code,true);
		$criteria->compare('phone_number',$this->phone_number,true);
		$criteria->compare('email_address',$this->email_address,true);
		$criteria->compare('member_type',$this->member_type,true);
		$criteria->compare('membership_type',$this->membership_type,true);
		$criteria->compare('local_senate',$this->local_senate,true);
		$criteria->compare('senate_region',$this->senate_region,true);
		$criteria->compare('dues_paid_league',$this->dues_paid_league,true);
		$criteria->compare('membership_status',$this->membership_status,true);
		$criteria->compare('notes',$this->notes,true);
		$criteria->compare('application_datestamp',$this->application_datestamp,true);
		$criteria->compare('paid_datestamp',$this->paid_datestamp,true);
		$criteria->compare('type',$this->type,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Members the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
