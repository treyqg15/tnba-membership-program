<?php

/**
 * This is the model class for table "membership".
 *
 * The followings are the available columns in table 'membership':
 * @property integer $membership_id
 * @property integer $member_id
 * @property string $dues_paid_league
 * @property string $status
 * @property string $creation_date
 * @property string $paid_datetime
 * @property string $type
 *
 * The followings are the available model relations:
 * @property MembershipStatus $status0
 * @property MembershipType $type0
 * @property Member $member
 */
class Membership extends CActiveRecord
{
	public $first_name;
	public $middle_name;
	public $last_name;
	public $tnba_number;
	public $usbc_number;
    public $senate;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'membership';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('member_id, season', 'required'),
			array('member_id', 'numerical', 'integerOnly'=>true),
			array('dues_paid_league', 'length', 'max'=>30),
			array('status, type', 'length', 'max'=>7),
			array('paid_datetime, dues_paid_league', 'safe'),
			array('season','seasonExists'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('membership_id, member_id, status, creation_date, paid_datetime, type, first_name, middle_name, last_name, usbc_number, tnba_number, senate, season', 'safe', 'on'=>'search'),
		);
	}

	public function seasonExists($attribute)
	{
		$membership = Membership::model()->findBySql("SELECT 1 FROM membership WHERE member_id = :mid AND season = :season",array(':mid'=>$this->member_id,'season'=>$this->season));
		$member = Member::model()->findByPk($this->member_id);

		if(!is_null($membership))
		{
			$this->addError($attribute,"A membership has been created already for the TNBA #: {$member->tnba_number} for the season {$this->season}");
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'status0' => array(self::BELONGS_TO, 'MembershipStatus', 'status'),
			'type0' => array(self::BELONGS_TO, 'MembershipType', 'type'),
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'membership_id' => 'Membership',
			'member_id' => 'Member',
			'dues_paid_league' => 'Dues Paid League',
			'status' => 'Status',
			'creation_date' => 'Membership Creation Date',
			'paid_datetime' => 'Paid Date',
			'type' => 'Type',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array('member');

		//member attributes
		$criteria->compare('member.first_name',$this->first_name,true);
		$criteria->compare('member.middle_name',$this->middle_name,true);
		$criteria->compare('member.last_name',$this->last_name,true);
		$criteria->compare('member.tnba_number',$this->tnba_number,true);
		$criteria->compare('member.usbc_number',$this->usbc_number,true);
        $criteria->compare('member.senate',$this->senate,true);

		$criteria->compare('membership_id',$this->membership_id);
		$criteria->compare('member_id',$this->member_id);
		$criteria->compare('dues_paid_league',$this->dues_paid_league,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('creation_date',$this->creation_date,true);
		$criteria->compare('paid_datetime',$this->paid_datetime,true);
		$criteria->compare('season',$this->season,true);
		$criteria->compare('type',$this->type,true);

		Yii::app()->session['filtered_data'] = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

		return Yii::app()->session['filtered_data'];
	}

	public function retrieveMembershipsByTNBANumber($tnbaNumber)
	{
		$criteria = new CDbCriteria;
		$criteria->together = true;

		$criteria->with = array('member');
		$criteria->condition='tnba_number=:tnbaNumber'; 
		$criteria->params=array(':tnbaNumber'=>$tnbaNumber);

		//member attributes
		$criteria->compare('member.first_name',$this->first_name,true);
		$criteria->compare('member.middle_name',$this->middle_name,true);
		$criteria->compare('member.last_name',$this->last_name,true);
		$criteria->compare('member.tnba_number',$this->tnba_number,true);
        $criteria->compare('member.senate',$this->senate,true);
		
		$criteria->compare('membership_id',$this->membership_id);
		$criteria->compare('member_id',$this->member_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('season',$this->season,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Membership the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
