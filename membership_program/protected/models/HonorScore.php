<?php

/**
 * This is the model class for table "honor_score".
 *
 * The followings are the available columns in table 'honor_score':
 * @property string $name
 * @property integer $jewelry_id
 * @property string $sex
 * @property string $ts
 * @property integer $isSingleGame
 *
 * The followings are the available model relations:
 * @property AwardsHighAverageBowler[] $awardsHighAverageBowlers
 * @property Sex $sex0
 * @property JewelryType $jewelry
 */
class HonorScore extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'honor_score';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, ts', 'required'),
			array('jewelry_id, isSingleGame', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>60),
			array('sex', 'length', 'max'=>6),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('name, jewelry_id, sex, ts, isSingleGame', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'awardsHighAverageBowlers' => array(self::HAS_MANY, 'AwardsHighAverageBowler', 'honor_score'),
			'sex0' => array(self::BELONGS_TO, 'Sex', 'sex'),
			'jewelry' => array(self::BELONGS_TO, 'JewelryType', 'jewelry_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name' => 'Name',
			'jewelry_id' => 'Jewelry',
			'sex' => 'Sex',
			'ts' => 'Ts',
			'isSingleGame' => 'Is Single Game',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('name',$this->name,true);
		$criteria->compare('jewelry_id',$this->jewelry_id);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('ts',$this->ts,true);
		$criteria->compare('isSingleGame',$this->isSingleGame);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return HonorScore the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
