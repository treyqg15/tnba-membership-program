<?php

/**
 * This is the model class for table "awards_high_average_bowler".
 *
 * The followings are the available columns in table 'awards_high_average_bowler':
 * @property integer $id
 * @property integer $member_id
 * @property string $league_cert_number
 * @property string $tournament_cert_number
 * @property string $date_of_performance
 * @property integer $certified
 * @property double $current_average
 * @property string $status
 * @property string $lane_cert
 * @property string $name_of_senate
 * @property string $honor_score
 *
 * The followings are the available model relations:
 * @property League $leagueCertNumber
 * @property Tournament $tournamentCertNumber
 * @property AwardsStatus $status0
 * @property HonorScore $honorScore
 * @property Senate $nameOfSenate
 * @property Member $member
 */
class AwardsHighAverageBowler extends CActiveRecord
{
	public $first_name;
	public $middle_name;
	public $last_name;
	public $street_address;
	public $zipcode;
	public $tnba_number;
	public $usbc_number;
	public $certified;
	public $sex;
	public $senate;
	public $league_name;
	public $bowling_alley_name;

	public $isCreating = false;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'awards_high_average_bowler';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date_of_performance, league_cert_number, current_average, status, name_of_senate, honor_score, season', 'required'),
			array('member_id, certified', 'numerical', 'integerOnly'=>true),
			array('current_average', 'numerical'),
			array('league_cert_number, tournament_cert_number', 'length', 'max'=>10),
			array('status, name_of_senate', 'length', 'max'=>30),
			array('honor_score', 'length', 'max'=>60),
			array('tnba_number','validateUserEligibility'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, member_id, league_cert_number, tournament_cert_number, date_of_performance, certified, current_average, status, name_of_senate, honor_score, first_name, middle_name, last_name, street_address, zipcode, tnba_number, usbc_number, sex, senate, league_name, season', 'safe', 'on'=>'search'),
		);
	}

	public function validateUserEligibility($attribute)
	{
		//only validate if creating new award record
		if(!$this->isCreating)
			return;

		error_log("validating user eligibility");
		$member = Member::model()->findBySql("SELECT first_name,middle_name,last_name,certified FROM member WHERE id = :id",array(':id'=>$this->member_id));
		$name = "{$member->first_name} {$member->middle_name} {$member->last_name}";

		$league = League::model()->findByPk($this->league_cert_number);

		if(!$member->certified)
			$this->addError('certified',"$name is not certified");

		if(!$league->certified)
			$this->addError('league.certified',"{$league->name} is not certified");

		if($this->awardExists())
			$this->addError($attribute,"$name has already received an award for the season {$this->season}");
	}

	private function awardExists()
	{
		$award = AwardsHighAverageBowler::model()->findBySql("SELECT 1 FROM awards_high_average_bowler WHERE season = :season",array('season'=>$this->season));

		if(!is_null($award))
		{
			return true;
		}

		return false;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'league' => array(self::BELONGS_TO, 'League', 'league_cert_number'),
			'tournamentCertNumber' => array(self::BELONGS_TO, 'Tournament', 'tournament_cert_number'),
			'status0' => array(self::BELONGS_TO, 'AwardsStatus', 'status'),
			'honorScore' => array(self::BELONGS_TO, 'HonorScore', 'honor_score'),
			'nameOfSenate' => array(self::BELONGS_TO, 'Senate', 'name_of_senate'),
			'member' => array(self::BELONGS_TO, 'Member', 'member_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'member_id' => 'Member',
			'league_cert_number' => 'League Cert Number',
			'tournament_cert_number' => 'Tournament Cert Number',
			'date_of_performance' => 'Date Of Performance',
			'certified' => 'Certified',
			'current_average' => 'Current Average',
			'status' => 'Status',
			'name_of_senate' => 'Name Of Senate',
			'honor_score' => 'Honor Score',
			'season' => 'Season'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array('member','league');

		//member criteria
		$criteria->compare('member.first_name',$this->first_name,true);
		$criteria->compare('member.middle_name',$this->middle_name,true);
		$criteria->compare('member.last_name',$this->last_name,true);
		$criteria->compare('member.street_address',$this->street_address,true);
		$criteria->compare('member.zipcode',$this->zipcode,true);
		$criteria->compare('member.tnba_number',$this->tnba_number,true);
		$criteria->compare('member.usbc_number',$this->usbc_number,true);
		$criteria->compare('member.sex',$this->sex);
		$criteria->compare('member.senate',$this->senate);
		$criteria->compare('member.certified',$this->certified);

		//league criteria
		$criteria->compare('league.name',$this->league_name,true);


		$criteria->compare('id',$this->id);
		$criteria->compare('member_id',$this->member_id);
		$criteria->compare('league_cert_number',$this->league_cert_number,true);
		$criteria->compare('tournament_cert_number',$this->tournament_cert_number,true);
		$criteria->compare('date_of_performance',$this->date_of_performance,true);
		$criteria->compare('current_average',$this->current_average);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('name_of_senate',$this->name_of_senate,true);
		$criteria->compare('honor_score',$this->honor_score,true);
		$criteria->compare('season',$this->season,true);

        error_log("here");
        
		Yii::app()->session['filtered_data'] = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

		return Yii::app()->session['filtered_data'];
	}

	public function retrieveAwardsByTNBANumber($tnbaNumber)
	{
		$criteria = new CDbCriteria;
		$criteria->with = array('member','league','league.bowlingAlley');
		$criteria->condition='tnba_number=:tnbaNumber'; 
		$criteria->params=array(':tnbaNumber'=>$tnbaNumber);

		//member criteria
		$criteria->compare('member.first_name',$this->first_name,true);
		$criteria->compare('member.middle_name',$this->middle_name,true);
		$criteria->compare('member.last_name',$this->last_name,true);
		$criteria->compare('member.street_address',$this->street_address,true);
		$criteria->compare('member.zipcode',$this->zipcode,true);
		$criteria->compare('member.tnba_number',$this->tnba_number,true);
		$criteria->compare('member.usbc_number',$this->usbc_number,true);
		$criteria->compare('member.sex',$this->sex);
		$criteria->compare('member.senate',$this->senate);
		$criteria->compare('member.certified',$this->certified);

		//league criteria
		$criteria->compare('league.name',$this->league_name,true);

		//bowlingAlley criteria
		$criteria->compare('bowlingAlley.name',$this->bowling_alley_name,true);

		$criteria->compare('id',$this->id);
		$criteria->compare('member_id',$this->member_id);
		$criteria->compare('league_cert_number',$this->league_cert_number,true);
		$criteria->compare('tournament_cert_number',$this->tournament_cert_number,true);
		$criteria->compare('date_of_performance',$this->date_of_performance,true);
		//$criteria->compare('certified',$this->certified);
		$criteria->compare('current_average',$this->current_average);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('name_of_senate',$this->name_of_senate,true);
		$criteria->compare('honor_score',$this->honor_score,true);
		$criteria->compare('season',$this->season,true);

		error_log(print_r($criteria,true));

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AwardsHighAverageBowler the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
