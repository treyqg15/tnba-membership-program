<?php

/**
 * This is the model class for table "league".
 *
 * The followings are the available columns in table 'league':
 * @property string $cert_number
 * @property string $name
 * @property integer $bowling_alley_id
 * @property string $season
 *
 * The followings are the available model relations:
 * @property AwardsHighAverageBowler[] $awardsHighAverageBowlers
 * @property BowlingAlley $bowlingAlley
 * @property Season $seasonName
 * @property Officer[] $officers
 * @property StatsHistory[] $statsHistories
 * @property StatsHistory[] $statsHistories1
 */
class League extends CActiveRecord
{
	public $bowling_alley_zipcode;
	public $city_name;
	public $state_name;
	public $season;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'league';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, bowling_alley_id, season, senate, certified', 'required'),
			array('bowling_alley_id', 'numerical', 'integerOnly'=>true),
			array('cert_number', 'length', 'max'=>10),
			array('name, senate', 'length', 'max'=>30),
			array('season', 'length', 'max'=>9),
			array('certified', 'length', 'max'=>1),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cert_number, name, bowling_alley_id, season, bowling_alley_zipcode, city_name, state_name, senate, certified', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'awardsHighAverageBowlers' => array(self::HAS_MANY, 'AwardsHighAverageBowler', 'league_cert_number'),
			'bowlingAlley' => array(self::BELONGS_TO, 'BowlingAlley', 'bowling_alley_id'),
			'seasonName' => array(self::BELONGS_TO, 'Season', 'season'),
			'officers' => array(self::MANY_MANY, 'Officer', 'league_officer(cert_number, officer_id)'),
			'statsHistories' => array(self::HAS_MANY, 'StatsHistory', 'season'),
			'statsHistories1' => array(self::HAS_MANY, 'StatsHistory', 'league_cert_number'),
			'senate' => array(self::BELONGS_TO, 'Senate', 'name')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cert_number' => 'Cert Number',
			'name' => 'League Name',
			'bowling_alley_id' => 'Bowling Alley',
			'season' => 'Season',
			'senate' => 'Senate',
			'certified' => 'Certified'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->with = array('bowlingAlley');

		$criteria->compare('cert_number',$this->cert_number,true);
		$criteria->compare('t.name',$this->name,true);
		$criteria->compare('bowling_alley_id',$this->bowling_alley_id);
		$criteria->compare('season',$this->season,true);
		$criteria->compare('senate',$this->senate,true);
		$criteria->compare('certified',$this->certified,true);
		$criteria->compare('bowlingAlley.zipcode',$this->bowling_alley_zipcode,true);
		$criteria->compare('bowlingAlley.zipcode.city_name',$this->city_name,true);
		$criteria->compare('bowlingAlley.zipcode.state_name',$this->state_name,true);

		Yii::app()->session['filtered_data'] = new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));

		return Yii::app()->session['filtered_data'];
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return League the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
