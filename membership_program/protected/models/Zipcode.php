<?php

/**
 * This is the model class for table "zipcode".
 *
 * The followings are the available columns in table 'zipcode':
 * @property string $zipcode
 * @property string $city_name
 * @property string $state_name
 *
 * The followings are the available model relations:
 * @property AwardsHighAverageBowler[] $awardsHighAverageBowlers
 * @property Tournament[] $tournaments
 * @property City $cityName
 * @property City $stateName
 */
class Zipcode extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'zipcode';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('zipcode, city_name, state_name', 'required'),
			array('zipcode', 'length', 'max'=>10),
			array('city_name, state_name', 'length', 'max'=>50),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('zipcode, city_name, state_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'awardsHighAverageBowlers' => array(self::HAS_MANY, 'AwardsHighAverageBowler', 'zipcode'),
			'tournaments' => array(self::HAS_MANY, 'Tournament', 'zipcode'),
			'cityName' => array(self::BELONGS_TO, 'City', 'city_name'),
			'stateName' => array(self::BELONGS_TO, 'City', 'state_name'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'zipcode' => 'Zipcode',
			'city_name' => 'City Name',
			'state_name' => 'State Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('zipcode',$this->zipcode,true);
		$criteria->compare('city_name',$this->city_name,true);
		$criteria->compare('state_name',$this->state_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Zipcode the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
