<?php

/**
 * This is the model class for table "jewelry_type".
 *
 * The followings are the available columns in table 'jewelry_type':
 * @property integer $id
 * @property string $pin_type
 * @property string $ring_type
 * @property double $ring_size
 *
 * The followings are the available model relations:
 * @property HonorScore[] $honorScores
 * @property Ring $ringType
 * @property Ring $ringSize
 * @property Pin $pinType
 */
class JewelryType extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'jewelry_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ring_size', 'numerical'),
			array('pin_type, ring_type', 'length', 'max'=>30),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, pin_type, ring_type, ring_size', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'honorScores' => array(self::HAS_MANY, 'HonorScore', 'jewelry_id'),
			'ringType' => array(self::BELONGS_TO, 'Ring', 'ring_type'),
			'ringSize' => array(self::BELONGS_TO, 'Ring', 'ring_size'),
			'pinType' => array(self::BELONGS_TO, 'Pin', 'pin_type'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'pin_type' => 'Pin Type',
			'ring_type' => 'Ring Type',
			'ring_size' => 'Ring Size',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('pin_type',$this->pin_type,true);
		$criteria->compare('ring_type',$this->ring_type,true);
		$criteria->compare('ring_size',$this->ring_size);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return JewelryType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
