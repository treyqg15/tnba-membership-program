<?php
/* @var $this ManageController */

/*
$this->breadcrumbs=array(
	'Manage'=>array('/manage'),
	'Awards',
);
*/
?>
<!--
<h1><?php echo $this->id . '/' . $this->action->id; ?></h1>

<p>
	You may change the content of this page by modifying
	the file <tt><?php echo __FILE__; ?></tt>.
</p>
-->

<div id="awards_button_menu">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
	    'label'=>'Create Award',
	    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
	    'size'=>'large', // null, 'large', 'small' or 'mini'
	    'url'=>Yii::app()->baseUrl . '/index.php/crud/awards/create'
	)); ?>

	<?php $this->widget('bootstrap.widgets.TbButton', array(
	    'label'=>'View Awards',
	    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
	    'size'=>'large', // null, 'large', 'small' or 'mini'
	    'url'=>Yii::app()->baseUrl . '/index.php/crud/awards/index'
	)); ?>
</div>
