<?php
/* @var $this BowlerController */

//Add in JQuery
Yii::app()->clientScript->registerCoreScript('jquery');

//Javascript
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/common.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/admin.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/statsHistory/viewBowlerHistory.js');

?>

<br /> <br />

<h1>Manage Bowler Stats</h1>

<form id="statsHistory_reports_form" action="<?php echo Yii::app()->createUrl('stats/statsHistory/runReport');?>" method="GET">
		<input type="hidden" name="function_to_call_field" />
		<input type="hidden" id="data_field" name="data_field" />
		<input type="hidden" id="report_type_field" name="report_type_field" />

<?php

$this->widget('bootstrap.widgets.TbButtonGroup', 
	array(
	    'buttons'=>array(
	        array(
			    'label'=>'Add Bowler Stats',
			    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			    'size'=>'small', // null, 'large', 'small' or 'mini'
				'htmlOptions' => array(
		            'data-toggle' => 'modal',
		            'data-target' => '#myModal',
			    )
		    ),
			array(
			    'label'=>'Delete Bowler Stats',
			    'type'=>'danger', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			    'size'=>'small', // null, 'large', 'small' or 'mini'
				'htmlOptions'=>array('onclick' => "js: manageRow('statsHistory-grid','Delete','". Yii::app()->createUrl('stats/statsHistory/deleteBowlerHistory') . "')")
			),
			array('label'=>'Run Reports','items'=>array(
					array(
					    'label'=>'Export Grid',
					    'buttonType'=>'submit', 
					    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
					    'size'=>'small', // null, 'large', 'small' or 'mini'
					    'url'=>'#',
						'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#statsHistory_reports_form','EXPORT ALL')"),
			    	),
			    	array(
					    'label'=>'Export Selected Rows',
					    'buttonType'=>'submit', 
					    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
					    'size'=>'small', // null, 'large', 'small' or 'mini'
					    'url'=>'#',
						'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#statsHistory_reports_form','EXPORT SELECTED ROWS')"),
			    	)
			    )
			)
		)
	)
);

?>

</form>

<?php

$this->widget('zii.widgets.grid.CGridView',
    array(
    	'id'=>'statsHistory-grid',
        //'fixedHeader' => true,
        //'headerOffset' => 40,
        'filter'=>$model,
        'selectableRows'=>2,
        // 40px is the height of the main navigation at bootstrap
        //'type' => 'striped',
        'dataProvider' => $model->search(),
        //'responsiveTable' => true,
        'columns' => array(
        	array(
				'name'=>'first_name',
				'value'=>'$data->member->first_name',
				//'filter'=>CHtml::activeTextField($model,'first_name')
			),
			array(
				'name'=>'middle_name',
				'value'=>'$data->member->middle_name',
				//'filter'=>CHtml::activeTextField($model,'first_name')
			),
			array(
				'name'=>'last_name',
				'value'=>'$data->member->last_name'
			),
			array(
				'name'=>'tnba_number',
				'value'=>'$data->member->tnba_number'
			),
			array(
				'name'=>'usbc_number',
				'value'=>'$data->member->usbc_number'
			),
			array(
				'name'=>'league_name',
				'value'=>'$data->league->name'
			),
			array(
				'name'=>'bowling_alley_name',
				'value'=>'$data->league->bowlingAlley->name'
			),
			array(
				'name'=>'bowling_alley_zipcode',
				'value'=>'$data->league->bowlingAlley->zipcode'
			),
			array(
				'name'=>'average',
				'class'=>'TbEditableColumn',
				'editable' => array(
                    'type' => 'number',
                    'url' => Yii::app()->createUrl('/stats/statsHistory/saveEdits')
                )
			),
			array(
				'name'=>'num_of_games',
				'class'=>'TbEditableColumn',
				'editable' => array(
                    'type' => 'number',
                    'url' => Yii::app()->createUrl('/stats/statsHistory/saveEdits')
                )
			),
			array(
				'name'=>'season',
				'value'=>'$data->league->season',
				'filter'=>CHtml::listData(Season::model()->findAll(), 'name', 'name')
			)
        ),
    )
);

?>

<?php $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'myModal')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Add Bowler Stats</h4>
    </div>
 
    <div class="modal-body">
        <div class="form">

			<?php $form=$this->beginWidget('CActiveForm', array(
			    'id'=>'create_statsHistory_form',
			    'action'=>'createAward',
			    'enableAjaxValidation'=>false,
			    'htmlOptions' => array(
			        'enctype' => 'multipart/form-data',
			    ),
			)); ?>

			    <p class="note">Fields with <span class="required">*</span> are required. </p>


			    <?php echo $form->errorSummary($model); ?>
				<?php 
					$bowlingAlley = new BowlingAlley();
					$league = new League();
					$memberModel = new Member();
				?>
			    <div>
			        <?php echo $form->labelEx($memberModel,'tnba_number'); ?>
			        <?php echo $form->textField($memberModel,'tnba_number'); ?>
			        <?php echo $form->error($memberModel,'tnba_number'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($bowlingAlley,'zipcode'); ?>
					<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',
					    array(
					      'model'=>$bowlingAlley,
					      'name'=>'zipcode',
					      'attribute'=>'zipcode',
					      'sourceUrl'=>Yii::app()->createUrl('stats/statsHistory/autocomplete'),//'autocomplete',
					      'htmlOptions'=>array('placeholder'=>'Zipcode'),
					      'options'=>
					         array(
					         		'select'=>'js: function(event,ui) {
					         			$(this).val(ui.item.value);
					         			$(this).trigger(\'change\');
					         			//var terms = ui.item.value;
					         			//var i = 0;
					         		}',
					   				'minLength'=>'2', // min chars to start search
					               	'showAnim'=>'fold',
					               	'maxLength'=>5
					                )
					    )); 
					?>
					<?php echo $form->error($bowlingAlley,'zipcode'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($bowlingAlley,'name'); ?>
			        <?php echo $form->dropDownList($bowlingAlley,'id', CHtml::listData(BowlingAlley::model()->findAll(array('order' => 'name ASC')), 'id', 'name')); ?>
			        <?php echo $form->error($bowlingAlley,'name'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($league,'name'); ?>
			        <?php echo $form->dropDownList($model,'league_cert_number', CHtml::listData(League::model()->findAll(array('order' => 'name ASC')), 'cert_number', 'name')); ?>
			        <?php echo $form->error($league,'name'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'average'); ?>
			        <?php echo $form->numberField($model,'average'); ?>
			        <?php echo $form->error($model,'average'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'season'); ?>
			        <?php echo $form->dropDownList($model,'season', CHtml::listData(Season::model()->findAll(array('order' => 'name ASC')), 'name', 'name')); ?>
			        <?php echo $form->error($model,'season'); ?>
			    </div>
				
			    <div class="buttons">
			        <?php echo CHtml::submitButton("Add Stats"); ?>
			    </div>

			<?php $this->endWidget(); ?>

		</div><!-- form -->
    </div>
 
    <div class="modal-footer">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 
<?php $this->endWidget(); ?>

<!-- Give JS pages url to controller -->
<script type="text/Javascript">
var statsHistoryControllerUrl = "<?php echo Yii::app()->createUrl('stats/statsHistory'); ?>";
var seasonsDropDownOptions = "<?php echo $dropDownOptions; ?>";
</script>

<?php echo CHtml::beginForm(); ?>

<?php echo CHtml::endForm(); ?>

</div>