<?php
/* @var $this AwardsController */
/* @var $model Awards */

/*
$this->breadcrumbs=array(
	'Awards'=>array('index'),
	'Manage',
);
*/

$this->menu=array(
	array('label'=>'Manage Awards', 'url'=>array('admin')),
);

//Add in JQuery
Yii::app()->clientScript->registerCoreScript('jquery');

//Javascript
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/awards/reports.js');

Yii::app()->clientScript->registerScript('dc', "
$('#awards_reports_grid table tbody tr').live('click',function(e)
{
	var rows = {};
    var keys = $('#awards_reports_grid > div.keys > span');
    var pk=keys.eq(this.rowIndex-2).text();

    var length = $(this).children().length;
    for(var i = 1; i < length; ++i)
    {
    	var value=$(this).children(':nth-child('+i+')').text();
    	rows[columns[i - 1]] = value;
	}

   	selectedRows[pk] = pk;

});
");

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#awards-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

echo "<br /><br />";
?>

<!-- Used to set up url to various report functions in controller -->
<script type="text/javascript">
var generatePackingListUrl = "<?php echo Yii::app()->createUrl('crud/awards/test'); ?>";
</script>

<h1>Generate Award Reports</h1>

<?php /*echo CHtml::link('Advanced Search','#',array('class'=>'search-button'));*/ ?>
<div id="awards_reports_button_menu">
	<form id="awards_reports_form" action="<?php echo Yii::app()->createUrl('crud/awards/runReport');?>" method="GET">
		<input type="hidden" name="function_to_call_field" />
		<input type="hidden" id="data_field" name="data_field" />
		<input type="hidden" id="report_type_field" name="report_type_field" />

		<?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
		    'buttons'=>array(
		        array(
				    'label'=>'Generate Packing List',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'htmlOptions'=>array('onclick' => "js: setPackingFunction('PACKING LIST')"),
				),
		        array(
				    'label'=>'Generate Packing Letters',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'htmlOptions'=>array('onclick' => "js: setPackingFunction('PACKING LETTER')"),
				),
				array(
				    'label'=>'Generate Prior Award Memorandum',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'htmlOptions'=>array('onclick' => "js: setPackingFunction('PRIOR AWARD')"),
				),
		        array(
				    'label'=>'Generate Missing Information Letter',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'htmlOptions'=>array('onclick' => "js: setPackingFunction('MISSING INFO LETTER')"),
		    	),
		    	array(
				    'label'=>'Generate No Membership Letter',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'htmlOptions'=>array('onclick' => "js: setPackingFunction('NO MEMBERSHIP LETTER')"),
		    	),
		    	array(
				    'label'=>'Generate Congratulatory Postcard',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'htmlOptions'=>array('onclick' => "js: setPackingFunction('CONGRATULATORY POSTCARD')"),
		    	)
		))); ?>
	</form>
</div>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'awards_reports_grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows'=>2,
	'selectionChanged'=>'storeSelectedRows',
	'columns'=>array(
		array(
			'name'=>'member.first_name',
			'value'=>'$data->member->first_name'
		),
		array(
			'name'=>'member.middle_name',
			'value'=>'$data->member->middle_name'
		),
		array(
			'name'=>'member.last_name',
			'value'=>'$data->member->last_name'
		),
		array(
			'name'=>'member.street_address',
			'value'=>'$data->member->street_address'
		),
		array(
			'name'=>'member.zipcode',
			'value'=>'$data->member->zipcode'
		),
		array(
			'name'=>'member.tnba_number',
			'value'=>'$data->member->tnba_number'
		),
		array(
			'name'=>'member.usbc_number',
			'value'=>'$data->member->usbc_number'
		),
		'status',
		array(
			'name' => 'member.sex',
			'value'=>'$data->member->sex',
			'filter' => CHtml::listData(Sex::model()->findAll(array('order' => 'type DESC')), 'type', 'type')
		),
		'league_cert_number',
		'tournament_cert_number',
		array(
			'name' => 'date_of_performance',
			'value'=>'Yii::app()->dateFormatter->format("MM-d-yyyy",strtotime($data->date_of_performance))'
		),
		'certified',
		'current_average',
		array(
			'name' => 'member.senate',
			'filter' => CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),
		array(
			'name' => 'honor_score',
			'filter' => CHtml::listData(HonorScore::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
