<?php
/* @var $this AwardsController */
/* @var $model Awards */

/*
$this->breadcrumbs=array(
	'Awards'=>array('index'),
	'Manage',
);
*/

/*
$this->menu=array(
	array('label'=>'Run Reports', 'url'=>array('report'))
);
*/

echo "<br /><br />";

Yii::app()->clientScript->registerCoreScript('jquery');

//Javascript
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/common.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/classes/Response.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/classes/AjaxBuilder.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/classes/MyAjax.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/admin.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/awards/manage.js');
//Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/awards/reports.js');

Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/css/crud/awards/admin.css');

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#awards-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Awards</h1>

<!-- Give JS pages url to controller -->
<script type="text/Javascript">
var awardControllerUrl = "<?php echo Yii::app()->createUrl('crud/award'); ?>";
var awardStatusesDropDownOptions = "<?php echo $awardStatusesDropDownOptions; ?>";
var honorScoresDropDownOptions = "<?php echo $honorScoresDropDownOptions; ?>";
var senatesDropdownOptions = "<?php echo $senatesDropdownOptions; ?>";
</script>

<form id="awards_reports_form" action="<?php echo Yii::app()->createUrl('crud/awards/runReport');?>" method="GET">
		<input type="hidden" name="function_to_call_field" />
		<input type="hidden" id="data_field" name="data_field" />
		<input type="hidden" id="report_type_field" name="report_type_field" />

<?php

$this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>array(
    	array(
		    'label'=>'Add Awards',
		    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
		    'size'=>'small', // null, 'large', 'small' or 'mini'
			'htmlOptions' => array(
	            'data-toggle' => 'modal',
	            'data-target' => '#myModal',
		    ),
		),
		array(
		    'label'=>'Delete Awards',
		    'type'=>'danger', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
		    'size'=>'small', // null, 'large', 'small' or 'mini'
			'htmlOptions'=>array('onclick' => "js: manageRow('awards-grid','Delete','". Yii::app()->createUrl('crud/awards/deleteAward') . "')")
		),
    	array('label'=>'Run Reports','items'=>array(
    			array(
				    'label'=>'Export Grid',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','EXPORT ALL')"),
		    	),
		    	array(
				    'label'=>'Export Selected Rows',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','EXPORT SELECTED ROWS')"),
		    	),
		        array(
				    'label'=>'Generate Packing List',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=> '#',//Yii::app()->createUrl('crud/awards/runReport',array('report'=>'PACKING LIST','data'=>"js: selectedRows")),//"js: setPackingFunction('PACKING LIST')",
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','PACKING LIST')"),
				),
		        array(
				    'label'=>'Generate Packing Letters',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','PACKING LETTER')"),
				),
				array(
				    'label'=>'Generate Prior Award Memorandum',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','PRIOR AWARD')"),
				),
		        array(
				    'label'=>'Generate Missing Information Letter',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','MISSING INFO LETTER')"),
		    	),
		    	array(
				    'label'=>'Generate No Membership Letter',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','NO MEMBERSHIP LETTER')"),
		    	),
		    	array(
				    'label'=>'Generate Congratulatory Postcard',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','CONGRATULATORY POSTCARD')"),
		    	),
		    	array(
				    'label'=>'Prior Postcard',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','PRIOR POSTCARD')"),
		    	),
		    	/*
		    	array(
				    'label'=>'Achievement Postcard',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#awards_reports_form','ACHIEVEMENT POSTCARD')"),
		    	)
		    	*/
		    )
		)
))); 

?>

</form>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'awards-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows'=>2,
	'afterAjaxUpdate'=>'function(id,data) {
		resetVars();
	}',
	'columns'=>array(
		array(
			'name'=>'id',
			'value'=>'$data->id',
			'visible'=>false
		),
		array(
			'name'=>'first_name',
			'value'=>'$data->member->first_name'
		),
		array(
			'name'=>'middle_name',
			'value'=>'$data->member->middle_name'
		),
		array(
			'name'=>'last_name',
			'value'=>'$data->member->last_name'
		),
        array(
            'name'=>'creation_time',
            'header'=>'Requisition Date',
            'value'=> 'convertCreationTimeToDate($data->creation_time)'
        ),
		array(
			'name'=>'street_address',
			'value'=>'$data->member->street_address'
		),
		array(
			'name'=>'zipcode',
			'value'=>'$data->member->zipcode'
		),
		/*
		array(
			'name'=>'zipcode',
            'filter'=> $this->widget('zii.widgets.jui.CJuiAutoComplete', 
            	array(
            		'model'=>$ZipcodeModel,
	        		'attribute'=>'zipcode',
	        		'source'=>'autocomplete',
	        		'options'=>
			        array(
			   				'minLength'=>'2', // min chars to start search
			               	'showAnim'=>'fold',
			               	'maxLength'=>10
			        )
            	),
            	true),
			/*array(
	        	'type'=>'zii.widgets.jui.CJuiAutoComplete',
	        	'model'=>$ZipcodeModel,
	        	'attribute'=>'zipcode',
	        	'source'=>'autocomplete',
	        	'options'=>
			        array(
			   				'minLength'=>'2', // min chars to start search
			               	'showAnim'=>'fold',
			               	'maxLength'=>10
			            )
			),
			*/
		//),
		array(
			'name'=>'tnba_number',
			'value'=>'$data->member->tnba_number'
		),
		array(
			'name'=>'league.name',
			/*
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/awards/saveEdits'),
                'source'=>Editable::source(League::model()->findAll(),'name','name'),
			),
			*/
			'value'=>'$data->league->name'
		),
		array(
			'name'=>'league.bowlingAlley.name',
			/*
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/awards/saveEdits'),
                'source'=>Editable::source(BowlingAlley::model()->findAll(),'name','name'),
                'success' => 'js: function(response, newValue) {
				    alert(newValue);
				}',
			),
			*/
			'value'=>'$data->league->bowlingAlley->name'
		),
		array(
			'name'=>'league.bowlingAlley.zipcode',
			/*
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'typeahead',
    			'source' => Editable::source(Zipcode::model()->findAll(),'zipcode','zipcode'),//Yii::app()->createUrl("/crud/awards/autoComplete"),
                'url' => Yii::app()->createUrl("/crud/awards/saveEdits"),
                'params' => 'js: function(params) {
					
					var i = 0;
					var pk = $(this).parent().parent().children(":nth-child(1)").text();
					params.pk = pk;

					return params;
                }',
                //'source'=>Editable::source(BowlingAlley::model()->findAll(),'name','name'),
                'success' => 'js: function(response, newValue) {
				    alert(newValue);
				}',
			),
			*/
			'value'=>'$data->league->bowlingAlley->zipcode'
		),
		array(
			'name'=>'status',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/awards/saveEdits'),
                'source'=>Editable::source(AwardsStatus::model()->findAll(),'status','status'),
                'value' => $model->status
			),
			'filter' => CHtml::listData(AwardsStatus::model()->findAll(array('order' => 'status DESC')), 'status', 'status')
		),
		array(
			'name' => 'sex',
			'value'=>'$data->member->sex',
			'filter' => CHtml::listData(Sex::model()->findAll(array('order' => 'type DESC')), 'type', 'type')
		),
		//'league_cert_number',
		//'tournament_cert_number',
		array(
			'name' => 'date_of_performance',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'date',
				'format' => 'yyyy-mm-dd',
				'viewformat'  => 'mm/dd/yyyy',
                'url' => Yii::app()->createUrl('/crud/awards/saveEdits'),
			)
			//'value'=>'Yii::app()->dateFormatter->format("MM-d-yyyy",strtotime($data->date_of_performance))'
		),
		array(
			'name'=>'certified',
			'value'=>'($data->member->certified) ? "YES": "NO"'
		),
		'current_average',
		//'name_of_lanes',
		//'lane_cert',
		array(
			'name' => 'senate',
			'value' => '$data->name_of_senate',
			'filter' => CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),
		array(
			'name' => 'honor_score',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/awards/saveEdits'),
                'display' => 'js: function(value, sourceData) {
				    //var escapedValue = $("<div>").text(value).html();
				    var html = value;
				    html += "<br />";
					var showGamesButton = false;
					var awards_games_button_id = null;
					switch(value)
					{
						case "900 Series":
						case "800 Series":
						case "700 Series":
						case "600 Series (FEMALE ONLY)":
						case "ALL SPARE GAME":
						case "TRIPLICATE":
						case "ELEVEN IN A ROW (297 game or less)":
							showGamesButton = true;
							break;
					}
					
					if(showGamesButton)
					{
						var pk = $(this).parent().parent().find("a").attr("data-pk");
						awards_games_button_id = "awards_games_button_" + pk;

						switch(value)
						{
							case "900 Series":
							case "800 Series":
							case "700 Series":
							case "600 Series (FEMALE ONLY)":
								html += "<button class=\"awards_games_button\" id=\""+awards_games_button_id+"\" data-honorscore=\""+value+"\" data-toggle=\"modal\" data-isSingleGame=\"0\" data-target=\"#myModal\">Enter Games</button>";
								break;
							case "ALL SPARE GAME":
							case "TRIPLICATE":
							case "ELEVEN IN A ROW (297 game or less)":
								html += "<button class=\"awards_games_button\" id=\""+awards_games_button_id+"\" data-honorscore=\""+value+"\" data-toggle=\"modal\" data-isSingleGame=\"1\" data-target=\"#myModal\">Enter Games</button>";
								break;
						}
						

					}
					
				    $(this).html(html);

				    if(showGamesButton)
				    	addAwardsGamesClickEvent(awards_games_button_id);
				}',
                'source'=> Editable::source(HonorScore::model()->findAll(),'name','name'),
                'value' => $model->honor_score
			),
			'filter' => CHtml::listData(HonorScore::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),
		array(
			'name' => 'season',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/awards/saveEdits'),
                'source'=> Editable::source(Season::model()->findAll(),'name','name'),
                'value' => $model->season
			),
			'filter' => CHtml::listData(Season::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),
	),
)); 

?>

<?php $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'myModal')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Create Award</h4>
    </div>
 
    <div class="modal-body">
        <div class="form">

			<?php $form=$this->beginWidget('CActiveForm', array(
			    'id'=>'create_award_form',
			    'action'=>'createAward',
			    'enableAjaxValidation'=>false,
			    'htmlOptions' => array(
			        'enctype' => 'multipart/form-data',
			    ),
			)); ?>

			    <p class="note">Fields with <span class="required">*</span> are required. </p>


			    <?php echo $form->errorSummary($model); ?>
				<?php 
					$bowlingAlley = new BowlingAlley();
					$league = new League();
				?>
			    <div>
			        <?php echo $form->labelEx($memberModel,'tnba_number'); ?>
			        <?php echo $form->textField($memberModel,'tnba_number'); ?>
			        <?php echo $form->error($memberModel,'tnba_number'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($bowlingAlley,'zipcode'); ?>
					<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',
					    array(
					      'model'=>$bowlingAlley,
					      'attribute'=>'zipcode',
					      'sourceUrl'=>Yii::app()->createUrl('crud/awards/autocomplete'),//'autocomplete',
					      'htmlOptions'=>array('placeholder'=>'Zipcode'),
					      'options'=>
					         array(
					         		'select'=>'js: function(event,ui) {
					         			$(this).val(ui.item.value);
					         			$(this).trigger(\'change\');
					         			//var terms = ui.item.value;
					         			//var i = 0;
					         		}',
					   				'minLength'=>'2', // min chars to start search
					               	'showAnim'=>'fold',
					               	'maxLength'=>5
					                )
					    )); 
					?>
					<?php echo $form->error($bowlingAlley,'zipcode'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($bowlingAlley,'name'); ?>
			        <?php echo $form->dropDownList($model,'bowling_alley_id', CHtml::listData(BowlingAlley::model()->findAll(array('order' => 'name ASC')), 'id', 'name')); ?>
			        <?php echo $form->error($bowlingAlley,'name'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($league,'name'); ?>
			        <?php echo $form->dropDownList($model,'league_cert_number', CHtml::listData(League::model()->findAll(array('order' => 'name ASC')), 'cert_number', 'name')); ?>
			        <?php echo $form->error($league,'name'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'status'); ?>
			        <?php echo $form->dropDownList($model,'status', CHtml::listData(AwardsStatus::model()->findAll(),'status','status')); ?>
			        <?php echo $form->error($model,'status'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'date_of_performance'); ?>
			        <?php echo $form->dateField($model,'date_of_performance'); ?>
			        <?php echo $form->error($model,'date_of_performance'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'current_average'); ?>
			        <?php echo $form->numberField($model,'current_average'); ?>
			        <?php echo $form->error($model,'current_average'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'name_of_senate'); ?>
			        <?php echo $form->dropDownList($model,'name_of_senate', CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name')); ?>
			        <?php echo $form->error($model,'name_of_senate'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'honor_score'); ?>
			        <?php echo $form->dropDownList($model,'honor_score', CHtml::listData(HonorScore::model()->findAll(array('order' => 'name ASC')), 'name', 'name')); ?>
			        <?php echo $form->error($model,'honor_score'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'season'); ?>
			        <?php echo $form->dropDownList($model,'season', CHtml::listData(Season::model()->findAll(array('order' => 'name ASC')), 'name', 'name')); ?>
			        <?php echo $form->error($model,'season'); ?>
			    </div>
				
			    <div class="buttons">
			        <?php echo CHtml::submitButton("Create Award"); ?>
			    </div>

			<?php $this->endWidget(); ?>

		</div><!-- form -->
    </div>
 
    <div class="modal-footer">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
<?php $this->endWidget(); ?>

<?php
    function convertCreationTimeToDate($creationTime)
    {
        error_log(print_r($creationTime,true));
        $date = date('m-d-Y', strtotime($creationTime));
        return $date;
    }
?>

<!-- -------------------------- MANAGE AWARDS GAMES MODAL --------------------- -->
<?php $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'manage_awards_games')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Enter Games</h4>
    </div>
 	
    <div class="modal-body">
    
    </div>
	
	<div class="modal-footer">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 
<?php $this->endWidget(); ?>