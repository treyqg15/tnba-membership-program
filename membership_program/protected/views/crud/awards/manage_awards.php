<!-- -------------------------- MANAGE AWARDS GAMES MODAL --------------------- --> 	
<?php
	
	$awardsID = Yii::app()->session['awardID'];
	$honorScore = Yii::app()->session['honorScore'];

	error_log("modal awardID: $awardsID");
	error_log("modal honorScore: $honorScore");

	$awardsGamesModel = AwardsGames::model()->findBySql("SELECT * FROM awards_games WHERE awards_id = :id",array(':id'=>$awardsID));
	$honorScoreModel = new HonorScore();

	if(is_null($awardsGamesModel))
		$awardsGamesModel = new AwardsGames();

	
?>
<?php /** @var BootActiveForm $form */
$form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id'=>'awards_games_form',
    'type'=>'inline',
    'method'=>'post',
    'action'=>'saveGames',
    'htmlOptions'=>array('class'=>'well'),
)); ?>

<div id="multi_games_div">
	<?php echo $form->textFieldRow($awardsGamesModel, 'games_1', array('class'=>'input-small', 'placeholder'=>'GAME 1', 'value'=>$awardsGamesModel->games_1)); ?>
	<?php echo $form->textFieldRow($awardsGamesModel, 'games_2', array('class'=>'input-small', 'placeholder'=>'GAME 2', 'value'=>$awardsGamesModel->games_2)); ?>
	<?php echo $form->textFieldRow($awardsGamesModel, 'games_3', array('class'=>'input-small', 'placeholder'=>'GAME 3', 'value'=>$awardsGamesModel->games_3)); ?>
	<?php echo $form->textFieldRow($awardsGamesModel, 'total', array('class'=>'input-small', 'placeholder'=>'TOTAL', 'value'=>$awardsGamesModel->total)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Save')); ?>
</div>

<div id="single_game_div">
	<?php echo $form->textFieldRow($awardsGamesModel, 'singleGame', array('class'=>'input-small', 'placeholder'=>'GAME', 'value'=>$awardsGamesModel->singleGame)); ?>
	<?php $this->widget('bootstrap.widgets.TbButton', array('buttonType'=>'submit', 'label'=>'Save')); ?>
</div>
<?php $this->endWidget(); ?>
 
