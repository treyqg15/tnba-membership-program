<?php
/* @var $this AwardsController */
/* @var $model Awards */

/*
$this->breadcrumbs=array(
	'Awards'=>array('index'),
	'Create',
);
*/

$this->menu=array(
	array('label'=>'List Awards', 'url'=>array('index')),
	array('label'=>'Manage Awards', 'url'=>array('admin')),
);
?>

<h1>Create Awards</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'honorScore'=>$honorScore,'memberModel'=>$memberModel)); ?>