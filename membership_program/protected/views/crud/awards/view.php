<?php
/* @var $this AwardsController */
/* @var $model Awards */

/*
$this->breadcrumbs=array(
	'Awards'=>array('index'),
	$model->id,
);
*/

$this->menu=array(
	array('label'=>'Update Awards', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Awards', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Awards', 'url'=>array('admin')),
);
?>

<h1>View Awards #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		array(
			'name' => 'member.first_name',
			'value' => $model->member->first_name
		),
		array(
			'name' => 'member.middle_name',
			'value' => $model->member->middle_name
		),
		array(
			'name' => 'member.last_name',
			'value' => $model->member->last_name
		),
		array(
			'name' => 'member.street_address',
			'value' => $model->member->street_address
		),
		array(
			'name' => 'member.zipcode',
			'value' => $model->member->zipcode
		),
		array(
			'name' => 'member.tnba_number',
			'value' => $model->member->tnba_number
		),
		array(
			'name' => 'member.usbc_number',
			'value' => $model->member->usbc_number
		),
		array(
			'name' => 'member.sex',
			'value' => $model->member->sex
		),
		'league_cert_number',
		'tournament_cert_number',
		array(
			'name' => 'date_of_performance',
			'value'=>Yii::app()->dateFormatter->format("MM-d-yyyy",strtotime($model->date_of_performance))
		),
		array(
			'name' => 'certified',
			'value' => ($model->certified) ? 'YES' : 'NO'
		),
		'current_average',
		'status',
		'name_of_senate',
		'honor_score',
	),
)); ?>
