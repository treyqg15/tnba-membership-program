<?php
/* @var $this AwardsController */
/* @var $model Awards */
/* @var $form CActiveForm */

Yii::app()->clientScript->registerCoreScript('jquery');

?>

<!--Include Javascript file-->
<?php
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/datePicker.js');
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/awards/create.js');

	//Any_Time Plugin files
	Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jq/plugins/Any_Time/js/anytime_compressed.js');
	Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/jq/plugins/Any_Time/css/anytime.css');
?>
	
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'awardsHighAverageBowler-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<!--
	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>
	-->
	<!--hidden text field that contains member id -->
	<?php echo $form->hiddenField($memberModel,'id'); ?>

	<div class="row">
		<?php echo $form->labelEx($memberModel,'first_name'); ?>
		<?php echo $form->textField($memberModel,'first_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($memberModel,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($memberModel,'middle_name'); ?>
		<?php echo $form->textField($memberModel,'middle_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($memberModel,'middle_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($memberModel,'last_name'); ?>
		<?php echo $form->textField($memberModel,'last_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($memberModel,'last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($memberModel,'street_address'); ?>
		<?php echo $form->textField($memberModel,'street_address',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($memberModel,'street_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($memberModel,'zipcode'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',
		    array(
		      'model'=>$memberModel,
		      'attribute'=>'zipcode',
		      'source'=>Yii::app()->createUrl('crud/awards/autocomplete'),
		      'htmlOptions'=>array('placeholder'=>'Zipcode'),
		      'options'=>
		         array(
		   				'minLength'=>'2', // min chars to start search
		               	'showAnim'=>'fold',
		               	'maxLength'=>10
		                )
		    )); 
		?>
		<?php echo $form->error($memberModel,'zipcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($memberModel,'tnba_number'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',
		    array(
		      'model'=>$memberModel,
		      'attribute'=>'tnba_number',
		      'source'=>Yii::app()->createUrl('crud/awards/autoCompleteTNBANumber'),
		      'htmlOptions'=>array('placeholder'=>'TNBA #'),
		      'options'=>
		         array(
		   				'minLength'=>'2', // min chars to start search
		               	'showAnim'=>'fold',
		               	'maxLength'=>6
		                )
		    ));
		?>
		<?php echo $form->error($memberModel,'tnba_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($memberModel,'usbc_number'); ?>
		<?php echo $form->textField($memberModel,'usbc_number',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($memberModel,'usbc_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($memberModel,'sex'); ?>
		<?php echo $form->dropDownList($memberModel,'sex', CHtml::listData(Sex::model()->findAll(array('order' => 'type DESC')), 'type', 'type'), array('empty'=>'--please select--')); ?>
		<?php echo $form->error($memberModel,'sex'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'league_cert_number'); ?>
		<?php echo $form->textField($model,'league_cert_number',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'league_cert_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tournament_cert_number'); ?>
		<?php echo $form->textField($model,'tournament_cert_number',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'tournament_cert_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_of_performance'); ?>
		<?php 
			if(!is_null($model->date_of_performance) && !empty($model->date_of_performance) && $model->date_of_performance != "")
				$model->date_of_performance = date("m-d-Y",strtotime($model->date_of_performance));
			echo $form->textField($model,'date_of_performance'); ?>
		<?php echo $form->error($model,'date_of_performance'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'certified'); ?>
		<?php echo $form->dropDownList($model,'certified', array('1'=>'YES','0'=>'NO'), array('empty'=>'--please select--')); ?>
		<?php echo $form->error($model,'certified'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'current_average'); ?>
		<?php echo $form->textField($model,'current_average'); ?>
		<?php echo $form->error($model,'current_average'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', CHtml::listData(AwardsStatus::model()->findAll(array('order' => 'ts ASC')), 'status', 'status'), array('empty'=>'--please select--')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name_of_senate'); ?>
		<?php echo $form->dropDownList($model,'name_of_senate', CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name'), array('empty'=>'--please select--')); ?>
		<?php echo $form->error($model,'name_of_senate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'honor_score'); ?>
		<?php echo $form->dropDownList($model,'honor_score', CHtml::listData(HonorScore::model()->findAll(array('order' => 'ts ASC')), 'name', 'name'), array('empty'=>'--please select--')); ?>
		<?php echo $form->error($model,'honor_score'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->