<?php
/* @var $this AwardsController */
/* @var $data Awards */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('first_name')); ?>:</b>
	<?php echo CHtml::encode($data->first_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('middle_name')); ?>:</b>
	<?php echo CHtml::encode($data->middle_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_name')); ?>:</b>
	<?php echo CHtml::encode($data->last_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('street_address')); ?>:</b>
	<?php echo CHtml::encode($data->street_address); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zipcode')); ?>:</b>
	<?php echo CHtml::encode($data->zipcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tnba_number')); ?>:</b>
	<?php echo CHtml::encode($data->tnba_number); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('usbc_number')); ?>:</b>
	<?php echo CHtml::encode($data->usbc_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sex')); ?>:</b>
	<?php echo CHtml::encode($data->sex); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('league_cert_number')); ?>:</b>
	<?php echo CHtml::encode($data->league_cert_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tournament_cert_number')); ?>:</b>
	<?php echo CHtml::encode($data->tournament_cert_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_of_performance')); ?>:</b>
	<?php echo CHtml::encode($data->date_of_performance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('certified')); ?>:</b>
	<?php echo CHtml::encode($data->certified); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('current_average')); ?>:</b>
	<?php echo CHtml::encode($data->current_average); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_of_lanes')); ?>:</b>
	<?php echo CHtml::encode($data->name_of_lanes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lane_cert')); ?>:</b>
	<?php echo CHtml::encode($data->lane_cert); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_of_senate')); ?>:</b>
	<?php echo CHtml::encode($data->name_of_senate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('honor_score')); ?>:</b>
	<?php echo CHtml::encode($data->honor_score); ?>
	<br />

	*/ ?>

</div>