<?php
/* @var $this AwardsController */
/* @var $model Awards */

echo "<br /><br />";

$this->menu=array(
	array('label'=>'View Awards', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Awards', 'url'=>array('admin')),
);
?>

<h1>Update Awards <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,'memberModel'=>$memberModel)); ?>