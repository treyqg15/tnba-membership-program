<?php
/* @var $this LeagueController */
/* @var $model League */

/*
$this->breadcrumbs=array(
	'Leagues'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List League', 'url'=>array('index')),
	array('label'=>'Create League', 'url'=>array('create')),
);
*/

Yii::app()->clientScript->registerCoreScript('jquery');

//Javascript
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/common.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/admin.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/league/manage.js');


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#league-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<br /><br />

<h1>Manage Leagues</h1>

<form id="league_reports_form" action="<?php echo Yii::app()->createUrl('crud/league/runReport');?>" method="GET">
		<input type="hidden" name="function_to_call_field" />
		<input type="hidden" id="data_field" name="data_field" />
		<input type="hidden" id="report_type_field" name="report_type_field" />

<?php

$this->widget('bootstrap.widgets.TbButtonGroup',
	array(
		'buttons'=> 
			array(
				array(
			        'label' => 'Create Leagues',
			        'type' => 'primary',
			        'htmlOptions' => array(
			            'data-toggle' => 'modal',
			            'data-target' => '#myModal',
			        )),
					array(
					    'label'=>'Delete Leagues',
					    'type'=>'danger', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
					    'size'=>'large', // null, 'large', 'small' or 'mini'
						'htmlOptions'=>array('onclick' => "js: manageRow('league-grid','Delete','". Yii::app()->createUrl('crud/league/deleteLeague') . "')")
					),
					array('label'=>'Run Reports','items'=>array(
						array(
						    'label'=>'Export Grid',
						    'buttonType'=>'submit', 
						    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
						    'size'=>'small', // null, 'large', 'small' or 'mini'
						    'url'=>'#',
							'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#league_reports_form','EXPORT ALL')"),
				    	),
				    	array(
						    'label'=>'Export Selected Rows',
						    'buttonType'=>'submit', 
						    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
						    'size'=>'small', // null, 'large', 'small' or 'mini'
						    'url'=>'#',
							'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#league_reports_form','EXPORT SELECTED ROWS')"),
				    	),
				    	array(
						    'label'=>'League Certification',
						    'buttonType'=>'submit', 
						    'type'=>'success', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
						    'size'=>'small', // null, 'large', 'small' or 'mini'
						    'url'=>'#',
							'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#league_reports_form','LEAGUE CERTIFICATION')"),
				    	)
				    )
				)
			)
		)
);

?>

</form>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'league-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'selectableRows'=>2,
	'columns'=>array(
		array(
			'name'=>'name',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/league/saveEdits'),
			),
			
		),
		array(
			'name'=>'bowling_alley_id',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/league/saveEdits'),
                "htmlOptions" => array(
				    "data-options" => 'CHtml::listData(BowlingAlley::model()->findAll(),\'name\',\'name\')',
				    "data-source_url" => Yii::app()->createUrl('crud/league/fetchBowlingAlleysForZipcode'),
				),
                'source'=>'js:function() { 
                	var zipcode = $(this).parent().parent().children(":nth-child(3)").text();
                	var source_url = $(this).data("source_url");
                	source_url += "/zipcode/"+zipcode;
                	return source_url;
                }',/*'js: function() {
                	var zipcode = $(this).parent().parent().children(":nth-child(3)").text();
					fetchBowlingAlleysForZipcode(zipcode);
                }'*///$this->createUrl('crud/league/fetchBowlingAlleysForZipcode'),//Editable::source(BowlingAlley::model()->findAll(),'name','name'),
       			'params' => 'js: function(params) {

					var bowlingAlley = $(this).parent().parent().children(":nth-child(2)").text();
					params.bowlingAlley = bowlingAlley;

					return params;
            	}',
       			'onSave'=>'js: function(response) {
       				//$.fn.yiiGridView.update(\'league-grid\');
       			}',

			),
			'filter' => CHtml::listData(BowlingAlley::model()->findAll(array('order' => 'name ASC')), 'id', 'name'),
			'value'=>'$data->bowlingAlley->name'
		),
		array(
			'name'=>'bowling_alley_zipcode',
			'value'=>'$data->bowlingAlley->zipcode'
		),
		array(
			'name'=>'senate',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'id' => 'senate',
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/league/saveEdits'),
                'source' => CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
			),
			'filter' => CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name'),
			//'value'=>'$data->senate->name'
		),
		array(
			'name'=>'certified',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'id' => 'bowling_alley_id',
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/league/saveEdits'),
                'source' => array("1" => "YES", "0" => "NO")
			),
			'filter' => array("1" => "YES", "0" => "NO"),
			'value'=>'($data->certified) ? "YES" : "NO"'
		),
		array(
			'name' => 'season',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/league/saveEdits'),
                'source'=> Editable::source(Season::model()->findAll(),'name','name'),
                'value' => $model->season
			),
			'filter' => CHtml::listData(Season::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),
		array(
			'class' => 'CButtonColumn',
			'template' => '{modify_members}<br />{view_officers}',
			'buttons' => array(
				'modify_members' => array(
					'label' => 'Modify Members',
					'click' => 'js: function() {
						var leagueName = $(this).parent().parent().children(":nth-child(1)").text();
						var pk = $(this).parent().parent().children(":nth-child(1)").children(0).data("pk");
						setSelectedLeague("view-member-grid",pk,leagueName);
					}',
					'options' => array(
						'data-toggle' => 'modal',
	            		'data-target' => '#view_members',
					),
				),
				'view_officers' => array(
					'label' => 'View Officers',
					'click' => 'js: function() {
						var leagueName = $(this).parent().parent().children(":nth-child(1)").text();
						var pk = $(this).parent().parent().children(":nth-child(1)").children(0).data("pk");
						setSelectedLeague("view-officer-grid",pk,leagueName);
					}',
					'options' => array(
						'data-toggle' => 'modal',
	            		'data-target' => '#view_officers',
					),
				),
			),
		),
		/*
		array(
			'name'=>'city_name',
			'value'=>'!is_null($data->bowlingAlley->zipcode) ? $data->bowlingAlley->zipcode->cityName : "False"'
		),
		array(
			'name'=>'state_name',
			//'value'=>'!is_null($data->bowlingAlley->zipcode) ? "R" : "False"'
		),
		*/
	),
)); ?>

<!-- -------------------------- View OFFICER MODAL --------------------- -->
<?php $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'view_officers')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Officers for League: <span class="modal_league_name"></span></h4>
    </div>
 	
    <div class="modal-body">
    	<?php
    		$memberModel = new Member();
    	?>
		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'view-officer-grid',
			'dataProvider'=>$memberModel->retrieveMembersWithRoles(),
			'filter'=>$memberModel,
			'selectableRows'=>2,
			'columns'=>array(
				'first_name',
				'middle_name',
				'last_name',
				array(
					'name'=>'tnba_number',
					'header'=>'Roles',
					'value' => '$data->memberRoles',
				)
			),
		)); ?>
    </div>
	
	<div class="modal-footer">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 
<?php $this->endWidget(); ?>



<!-- ---------------------------- View MEMBER MODAL ---------------------- -->


<?php $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'view_members')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Members for League: <span class="modal_league_name"></span></h4>
    </div>
 	
    <div class="modal-body">
		
		<div class="form">

			<?php $form=$this->beginWidget('CActiveForm', array(
			    'id'=>'create_member_form',
			    'action'=>'addMemberToLeague',
			    'enableAjaxValidation'=>false,
			    'htmlOptions' => array(
			        'enctype' => 'multipart/form-data',
			    ),
			)); ?>

		    <p class="note">Fields with <span class="required">*</span> are required. </p>

			<?php 
				$memberModel = new Member();
				$leagueMemberModel = new LeagueMember();
				$certNumber = Yii::app()->session['selected_league'];
				error_log("certNumber: $certNumber");
				$memberModel = new Member();//Member::model()->findAllBySql("SELECT * FROM member WHERE id IN (SELECT member_id FROM league_member WHERE league_cert_number = :cert_number)",array(':cert_number'=>$certNumber));
			?>
		    <?php echo $form->errorSummary($model); ?>

			<div>
		        <?php echo $form->labelEx($memberModel,'tnba_number'); ?>
		        <?php echo $form->textField($memberModel,'tnba_number'); ?>
		        <?php echo $form->error($memberModel,'tnba_number'); ?>
		    </div>
			
			<!-- send certNumber to server -->
			<?php echo $form->hiddenField($leagueMemberModel,'league_cert_number',array('value' => Yii::app()->session['selected_league'])); ?>

		    <div class="buttons">
			    <?php echo CHtml::submitButton("Add Member to League"); ?>
			</div>

			<?php $this->endWidget(); ?>
		</div>

		<?php $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'view-member-grid',
			'dataProvider'=>$memberModel->retrieveMembersByCertNumber(),
			'filter'=>$memberModel,
			'selectableRows'=>2,
			'columns'=>array(
				'first_name',
				'middle_name',
				'last_name',

				/*
				array(
					'name'=>'city_name',
					'value'=>'!is_null($data->bowlingAlley->zipcode) ? $data->bowlingAlley->zipcode->cityName : "False"'
				),
				array(
					'name'=>'state_name',
					//'value'=>'!is_null($data->bowlingAlley->zipcode) ? "R" : "False"'
				),
				*/
			),
		)); ?>	

		<?php
			$this->widget('bootstrap.widgets.TbButtonGroup',
				array(
					'buttons'=> 
						array(
								array(
								    'label'=>'Delete Leagues',
								    'type'=>'danger', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
								    'size'=>'large', // null, 'large', 'small' or 'mini'
									'htmlOptions'=>array('onclick' => "js: manageRow('view-member-grid','Delete','". Yii::app()->createUrl('crud/league/deleteMemberFromLeague') . "')")
								)
							)
						)
			);
		?>
    </div>
	
	<div class="modal-footer">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 
<?php $this->endWidget(); ?>




<?php $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'myModal')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Add League</h4>
    </div>
 
    <div class="modal-body">
        <div class="form">

			<?php $form=$this->beginWidget('CActiveForm', array(
			    'id'=>'create_league_form',
			    'action'=>'createLeague',
			    'enableAjaxValidation'=>false,
			)); ?>

			    <p class="note">Fields with <span class="required">*</span> are required. </p>


			    <?php echo $form->errorSummary($model); ?>
				<?php 
					$bowlingAlley = new BowlingAlley();
				?>
			    <div>
			        <?php echo $form->labelEx($model,'name'); ?>
			        <?php echo $form->textField($model,'name'); ?>
			        <?php echo $form->error($model,'name'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($bowlingAlley,'name'); ?>
			        <?php echo $form->dropDownList($bowlingAlley,'id', CHtml::listData(BowlingAlley::model()->findAll(array('order' => 'name ASC')), 'id', 'name')); ?>
			        <?php echo $form->error($bowlingAlley,'name'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($bowlingAlley,'zipcode'); ?>
					<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',
					    array(
					      'model'=>$bowlingAlley,
					      'attribute'=>'zipcode',
					      'sourceUrl'=>Yii::app()->createUrl('crud/league/autocomplete'),//'autocomplete',
					      'htmlOptions'=>array('placeholder'=>'Zipcode'),
					      'options'=>
					         array(
					         		'select'=>'js: function(event,ui) {
					         			$(this).val(ui.item.value);
					         			$(this).trigger(\'change\');
					         			//var terms = ui.item.value;
					         			//var i = 0;
					         		}',
					   				'minLength'=>'2', // min chars to start search
					               	'showAnim'=>'fold',
					               	'maxLength'=>5
					                )
					    )); 
					?>
					<?php echo $form->error($bowlingAlley,'zipcode'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'season'); ?>
			        <?php echo $form->dropDownList($model,'season', CHtml::listData(Season::model()->findAll(array('order' => 'name ASC')), 'name', 'name'), array('empty'=>'--please select--')); ?>
			        <?php echo $form->error($model,'season'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'senate'); ?>
			        <?php echo $form->dropDownList($model,'senate', CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name'), array('empty'=>'--please select--')); ?>
			        <?php echo $form->error($model,'senate'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'certified'); ?>
			        <?php echo $form->dropDownList($model,'certified', array("1" => "YES", "0" => "NO")); ?>
			        <?php echo $form->error($model,'certified'); ?>
			    </div>


			    <div class="buttons">
			        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
			    </div>

			<?php $this->endWidget(); ?>

		</div><!-- form -->
    </div>
 
    <div class="modal-footer">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 
<?php $this->endWidget(); ?>