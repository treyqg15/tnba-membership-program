<?php
/* @var $this LeagueController */
/* @var $model League */

$this->breadcrumbs=array(
	'Leagues'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List League', 'url'=>array('index')),
	array('label'=>'Create League', 'url'=>array('create')),
	array('label'=>'Update League', 'url'=>array('update', 'id'=>$model->cert_number)),
	array('label'=>'Delete League', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->cert_number),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage League', 'url'=>array('admin')),
);
?>

<h1>View League #<?php echo $model->cert_number; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'cert_number',
		'name',
		'bowling_alley_name',
		'season_name',
	),
)); ?>
