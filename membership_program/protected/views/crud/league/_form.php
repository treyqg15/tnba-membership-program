<?php
/* @var $this LeagueController */
/* @var $model League */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'league-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'cert_number'); ?>
		<?php echo $form->textField($model,'cert_number',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'cert_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'bowling_alley_name'); ?>
		<?php echo $form->textField($model,'bowling_alley_name',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'bowling_alley_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'season_name'); ?>
		<?php echo $form->textField($model,'season_name',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'season_name'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->