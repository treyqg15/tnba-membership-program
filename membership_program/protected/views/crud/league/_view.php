<?php
/* @var $this LeagueController */
/* @var $data League */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('cert_number')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->cert_number), array('view', 'id'=>$data->cert_number)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bowling_alley_name')); ?>:</b>
	<?php echo CHtml::encode($data->bowling_alley_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('season_name')); ?>:</b>
	<?php echo CHtml::encode($data->season_name); ?>
	<br />


</div>