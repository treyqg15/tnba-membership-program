<?php
	$this->widget('zii.widgets.jui.CJuiTabs',array(
	    'tabs'=>array(
	        'Memberships '=>array('id'=>'memberships_tab','content'=>$this->renderPartial(
	                                        'memberships',
	                                        array('tnba_number'=>Yii::app()->session['searched_tnba_number']),TRUE
	                                        )),
	        'Leagues '=>array('id'=>'statshistory_tab','content'=>$this->renderPartial(
	                                        'statsHistory',
	                                        array('tnba_number'=>Yii::app()->session['searched_tnba_number']),TRUE
	                                        )),
	        'Awards '=>array('id'=>'awards_tab','content'=>$this->renderPartial(
	                                        'awards',
	                                        array('tnba_number'=>Yii::app()->session['searched_tnba_number']),TRUE
	                                        )),
	        /*
	        'StaticTab With ID'=>array('content'=>'Content for tab 2 With Id' , 'id'=>'tab2'),
	        'Render Partial'=>array('id'=>'test-id','content'=>$this->renderPartial(
	                                        '_renderpage',
	                                        array('Values'=>'This Is My Renderpartial Page'),TRUE
	                                        )),       
	        // panel 3 contains the content rendered by a partial view
	        'AjaxTab'=>array('ajax'=>$this->createUrl('ajax')),
	        */
	    ),
	    // additional javascript options for the tabs plugin
	    'options'=>array(
	        'collapsible'=>true,
	    ),
	    'id'=>'MyTab-Menu',
	));
?>