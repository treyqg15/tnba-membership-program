<?php 

//error_log("usbcNumber: " . Yii::app()->session['searched_usbc_number']);
error_log("tnba_number: $tnba_number");

//LIMIT 1 should only be temporary. There should never be multiple USBC #'s in Member table'
$model = new Membership();

//error_log(print_r($model,true));

if(!is_null($model))
{
	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'member-grid',
		'dataProvider'=>$model->retrieveMembershipsByTNBANumber($tnba_number),
		'columns'=>array(
			//'id',
			array(
				'name' => 'first_name',
				'value' => '$data->member->first_name'
			),
			array(
				'name' => 'middle_name',
				'value' => '$data->member->middle_name'
			),
			array(
				'name' => 'last_name',
				'value' => '$data->member->last_name'
			),
			array(
				'name' => 'creation_date',
				'value'=>'$data->creation_date'//Yii::app()->dateFormatter->format("MM-d-yyyy",'$data->creation_datetime')
			),
			'dues_paid_league',
			'status',
			'type',
		),
	)); 
}
?>