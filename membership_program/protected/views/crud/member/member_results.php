<?php
	
	$memberModel = new Member();

	if(isset($_GET['Member']))
		$memberModel->attributes=$_GET['Member'];

	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'member-results-grid',
		'dataProvider'=>$memberModel->retrieveSearchedForMembers(),
		'columns'=>array(
			//'id',
			array(
				'name' => 'first_name',
				'value' => '$data->first_name'
			),
			array(
				'name' => 'middle_name',
				'value' => '$data->middle_name'
			),
			array(
				'name' => 'last_name',
				'value' => '$data->last_name'
			),
			array(
				'name' => 'sex',
				'value' => '$data->sex'
			),
			array(
				'name' => 'tnba_number',
				'value' => '$data->tnba_number'
			),
			array(
				'name' => 'senate',
				'value' => '$data->senate'
			),
			array(
				'name' => 'senate_region',
				'value' => '$data->senate_region'
			),
			array(
			    'class'=>'CButtonColumn',
			    'template'=>'{view_bowler}',
			    'buttons'=>array(
			    	'view_bowler' => array(
					    'label'=>'View',     //Text label of the button.
					    //'imageUrl'=>'...',  //Image URL of the button.
					    'options'=>array(
					    	'data-tnba_number'=>'$data->tnba_number'
					    ), //HTML options for the button tag.
					    'click'=>'storeTNBANumber',     //A JS function to be invoked when the button is clicked.
					    //'visible'=>'...',   //A PHP expression for determining whether the button is visible.
					)
			    )
			)
		),
	)); 

?>