<?php
	$baseUrl = Yii::app()->baseUrl; 
  	$cs = Yii::app()->getClientScript();
  	$cs->registerScriptFile($baseUrl.'/js/member/find_a_member.js');
  	$cs->registerCssFile($baseUrl.'/css/crud/member/find_a_member.css');

?>

<br /><br />

<div id="my_content">
	<div id="header_div">
		<div id="radio_div">
			<div id="by_name_label_div">
				<input type="radio" id="by_name" name="search_option" checked /> <label for="by_name">Search By Name</label>
			</div>
			<div id="by_tnba_number_label_div">
				<input type="radio" id="by_tnba_number" name="search_option" /> <label for="by_tnba_number">Search by TNBA #</label>
			</div>
		</div>
	</div>

	<div id="by_name_div">
		<?php $form=$this->beginWidget('CActiveForm', array(
		    'id'=>'find_member_form',
		    'action'=>'findMemberByName',
		    'enableAjaxValidation'=>false,
		)); ?>

		 <p class="note">Fields with <span class="required">*</span> are required. </p>

		<?php 
			$member = new Member();
			$zipcode = new Zipcode();
		?>

		<?php echo $form->errorSummary($member); ?>

	    <div>
	        <?php echo $form->labelEx($member,'first_name'); ?>
	        <?php echo $form->textField($member,'first_name'); ?>
	        <?php echo $form->error($member,'first_name'); ?>
	    </div>

	    <div>
	        <?php echo $form->labelEx($member,'last_name'); ?>
	        <?php echo $form->textField($member,'last_name'); ?>
	        <?php echo $form->error($member,'last_name'); ?>
	    </div>

	    <div>
	        <?php echo $form->label($member,'zipcode'); ?>
	        <?php echo $form->numberField($member,'zipcode'); ?>
	        <?php echo $form->error($member,'zipcode'); ?>
	    </div>

	    <div>
	        <?php echo $form->label($zipcode,'state_name'); ?>
	        <?php echo $form->dropDownList($zipcode,'state_name', CHtml::listData(State::model()->findAll(array('order' => 'name ASC')), 'name', 'name')); ?>
	        <?php echo $form->error($zipcode,'state_name'); ?>
	    </div>
		
		<div class="buttons">
	        <?php echo CHtml::submitButton('Submit'); ?>
	    </div>

		<?php $this->endWidget(); ?>

	</div>
	<div id="tnba_number_div">
		<h2>Enter TNBA #</h2>              

		<input id="tnba_number_field" name="tnba_number_field" type="text" />
		<?php
			$this->widget('bootstrap.widgets.TbButton', array(
				'buttonType' => 'ajaxSubmit',
		        'size' => 'small',
		        'label' => 'Search',
		        'url' => Yii::app()->createUrl('crud/member/findMemberByTNBANumber'),
		        'htmlOptions' => array(
		        	'id' => uniqid(),
		        ),
		        'ajaxOptions'=>array( 
		                            'type'=>'post',
		                            'dataType'=>'json',
		                            'data' => "js:{'tnba_number' : $('#tnba_number_field').val()}",
		                            'beforeSend' => 'function( request ) {
	                          			//alert("before send");
	                        		}',
		                            'success'=>'function (response) {
										loadTabs();
		                            }',
		                            'error' => 'function (xhr, ajaxOptions, thrownError) {
									    alert("status: " + xhr.status);
										alert("error: " + thrownError);
									}'
	                                ),
		    ));
	    ?>
	</div>

	<!--will show grid of results from find a member by name search-->
	<div id="member_results_div">
	</div>

	<div id="tabs_div">
	</div>
</div>