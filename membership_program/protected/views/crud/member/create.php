<?php
/* @var $this MemberController */
/* @var $model Member */

?>

<h1>Create Member</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'action'=>'create','id'=>null)); ?>