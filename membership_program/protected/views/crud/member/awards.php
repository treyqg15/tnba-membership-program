<?php
	
	error_log("awards");
	error_log("tnbaNumber: " . Yii::app()->session['searched_tnba_number']);
	error_log("tnba_number: $tnba_number");

//LIMIT 1 should only be temporary. There should never be multiple USBC #'s in Member table'
//$model = AwardsHighAverageBowler::model()->findBySql("SELECT * FROM awards_high_average_bowler where member_id = (SELECT id FROM member WHERE tnba_number = :tnbaNumber)",array(':tnbaNumber' => $tnba_number));

$model = new AwardsHighAverageBowler();

if(isset($_GET['AwardsHighAverageBowler']))
	$model->attributes=$_GET['AwardsHighAverageBowler'];

//error_log(print_r($model,true));

if(!is_null($model))
{
	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'stats_history_grid',
		'dataProvider'=>$model->retrieveAwardsByTNBANumber($tnba_number),
		'columns'=>array(
			//'id',
			array(
				'name'=>'first_name',
				'value'=>'$data->member->first_name'
			),
			array(
				'name'=>'middle_name',
				'value'=>'$data->member->middle_name'
			),
			array(
				'name'=>'last_name',
				'value'=>'$data->member->last_name'
			),
			'sex',
			array(
				'name'=>'league_name',
				'value'=>'$data->league->name'
			),
			array(
				'name'=>'bowling_alley_name',
				'value'=>'$data->league->bowlingAlley->name'
			),
			'status',
			'honor_score',
			'date_of_performance',
			'season',
			'name_of_senate'
		),
	)); 
}
?>