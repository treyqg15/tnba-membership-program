<?php
/* @var $this MemberController */
/* @var $model Member */
/* @var $form CActiveForm */

//Add in JQuery
Yii::app()->clientScript->registerCoreScript('jquery');

//Any_Time Plugin files
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jq/plugins/Any_Time/js/anytime_compressed.js');
Yii::app()->clientScript->registerCssFile(Yii::app()->baseUrl.'/js/jq/plugins/Any_Time/css/anytime.css');

//Javascript
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/datePicker.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/member/create.js');

?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'member-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
	'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'first_name'); ?>
		<?php echo $form->textField($model,'first_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'first_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'middle_name'); ?>
		<?php echo $form->textField($model,'middle_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'middle_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_name'); ?>
		<?php echo $form->textField($model,'last_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'last_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category_type'); ?>
		<?php echo $form->dropDownList($model,'category_type', CHtml::listData(Category::model()->findAll(array('order' => 'type ASC')), 'type', 'type')); ?>
		<?php echo $form->error($model,'category_type'); ?>
	</div>

	<div class="row">
		<input type="hidden" name="role_id" id="role_field" />
		<?php echo $form->labelEx($model,'role_id'); ?>
		<?php
			/*
			$this->widget('CTreeView',
				array('url'=>Yii::app()->createUrl('crud/member/populateRoleTree',array('action'=>$action,'id'=>(is_null($id)) ? null : $id)),
					  'htmlOptions'=>array('onclick' => "js: setRoleValue()"),
				)
			);
			*/
			echo $form->dropDownList($model,'role_id', CHtml::listData(Role::model()->findAll(), 'id', 'name'), array('multiple' => 'multiple'));
		?>
		<?php echo $form->error($model,'role_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'usbc_number'); ?>
		<?php echo $form->textField($model,'usbc_number',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'usbc_number'); ?>
	</div>
	
	<!--
	<div class="row">
		<?php echo $form->labelEx($model,'tnba_number'); ?>
		<?php echo $form->textField($model,'tnba_number',array('size'=>6,'maxlength'=>6)); ?>
		<?php echo $form->error($model,'tnba_number'); ?>
	</div>
	-->
	<div class="row">
		<?php echo $form->labelEx($model,'birthday'); ?>
		<?php echo $form->dateField($model,'birthday'); ?>
		<?php echo $form->error($model,'birthday'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sex'); ?>
		<?php echo $form->dropDownList($model,'sex', CHtml::listData(Sex::model()->findAll(array('order' => 'type DESC')), 'type', 'type'), array('empty'=>'--please select--')); ?>
		<?php echo $form->error($model,'sex'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'street_address'); ?>
		<?php echo $form->textField($model,'street_address',array('size'=>100,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'street_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zipcode'); ?>
		<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',
		    array(
		      'model'=>$model,
		      'attribute'=>'zipcode',
		      'source'=>Yii::app()->createUrl('crud/member/autocomplete'),//'autocomplete',
		      'htmlOptions'=>array('placeholder'=>'Zipcode'),
		      'options'=>
		         array(
		   				'minLength'=>'2', // min chars to start search
		               	'showAnim'=>'fold',
		               	'maxLength'=>5
		                )
		    )); 
		?>
		<?php echo $form->error($model,'zipcode'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone_number'); ?>
		<?php echo $form->textField($model,'phone_number',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'phone_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'senate'); ?>
		<?php echo $form->dropDownList($model,'senate', CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name'), array('empty'=>'--please select--')); ?>
		<?php echo $form->error($model,'senate'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'senate_region'); ?>
		<?php echo $form->dropDownList($model,'senate_region', CHtml::listData(Region::model()->findAll(array('order' => 'name ASC')), 'name', 'name'), array('empty'=>'--please select--')); ?>
		<?php echo $form->error($model,'senate_region'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'e_signature'); ?>
		<?php echo $form->textField($model,'e_signature',array('size'=>100,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'e_signature'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'w_9_id'); ?>
        <?php echo $form->fileField($model, 'w_9_id'); ?>
        <?php echo $form->error($model,'w_9_id'); ?> <!-- by this we can upload w_9_id-->
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->