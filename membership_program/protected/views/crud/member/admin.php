<?php
/* @var $this MemberController */
/* @var $model Member */

Yii::app()->clientScript->registerCoreScript('jquery');

//Javascript
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/common.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/admin.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/member/manage.js');


/*
$this->menu=array(
	array('label'=>'Create Member', 'url'=>array('create'))
);

*/

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#member-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");

?>
<br />
<br />
<h1>Manage Members</h1>

<!-- Give JS pages url to controller -->
<script type="text/Javascript">
var memberControllerUrl = "<?php echo Yii::app()->createUrl('crud/member'); ?>";
var categoriesDropDownOptions = "<?php echo $categoriesDropDownOptions; ?>";
var sexesDropDownOptions = "<?php echo $sexesDropDownOptions; ?>";
var senatesDropDownOptions = "<?php echo $senatesDropDownOptions; ?>";
var regionsDropDownOptions = "<?php echo $regionsDropDownOptions; ?>";
</script>

<form id="member_reports_form" action="<?php echo Yii::app()->createUrl('crud/member/runReport');?>" method="GET">
		<input type="hidden" name="function_to_call_field" />
		<input type="hidden" id="data_field" name="data_field" />
		<input type="hidden" id="report_type_field" name="report_type_field" />

<?php

$this->widget('bootstrap.widgets.TbButtonGroup', 
	array(
	    'buttons'=>array(
	        array(
			    'label'=>'Add Members',
			    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			    'size'=>'small', // null, 'large', 'small' or 'mini'
				'htmlOptions' => array(
		            'data-toggle' => 'modal',
		            'data-target' => '#myModal',
			    ),
			),
			array(
			    'label'=>'Delete Members',
			    'type'=>'danger', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			    'size'=>'small', // null, 'large', 'small' or 'mini'
				'htmlOptions'=>array('onclick' => "js: manageRow('member-grid','Delete','". Yii::app()->createUrl('crud/member/deleteMember') . "')")
			),
			array('label'=>'Run Reports','items'=>array(
				array(
				    'label'=>'Export Grid',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#member_reports_form','EXPORT ALL')"),
		    	),
		    	array(
				    'label'=>'Export Selected Rows',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#member_reports_form','EXPORT SELECTED ROWS')"),
		    	),
		    	array(
				    'label'=>'Generate Mailing Labels',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#member_reports_form','GENERATE MAILING LABELS')"),
		    	),
		    	array(
				    'label'=>'Generate TNBA Cards',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#member_reports_form','GENERATE TNBA CARDS')"),
		    	),
		    )
		)
	)
));

?>

</form>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'member-grid',
	'dataProvider'=>$model->search(),
	'selectableRows'=>2,
	'filter'=>$model,
	'afterAjaxUpdate'=>'function(id,data) {
		attachEvents();
		resetVars();
	}',
	'columns'=>array(
		//'id',
		array(
			'name'=>'first_name',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits')
			)
		),
		array(
			'name'=>'middle_name',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits')
			)
		),
		array(
			'name'=>'last_name',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits')
			)
		),
		array(
			'name'=>'email',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
			)
		),
		array(
			'name'=>'certified',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
                'source'=>array("1"=>"YES","0"=>"NO"),
                'value' => $model->certified
			),
			'filter' => array("1"=>"YES","0"=>"NO"),
			'value'=>'($data->certified) ? "YES": "NO"'
		),
		array(
			'name'=>'category_type',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
                'source'=>Editable::source(Category::model()->findAll(),'type','type'),//CHtml::listData(Category::model()->findAll(), 'type', 'type'),
                'value' => $model->category_type
			)
		),
		array(
			'name'=>'tnba_number',
			'header'=>'Roles',
			//'name'=>'roles.role_id',
			//'name'=>'r_role_id',
			//'name'=>'role_id',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select2',
				'title' => 'Select User Roles',
				//'attribute' => 'r_role_id',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
                'source'=> Editable::source(Role::model()->findAll(), 'id', 'name'),
                'params'=>'js: function(params) {
					return roleParams(params);
                }',
                'select2'   => array(
		           'multiple' => true
		        )
				

				/*
				'onInit' => 'js: function(e, editable) {
					var text = $(this).parent().parent().children(":nth-child(6)").text();
					var values = text.split(",");
					var indexes = [];
					
					var source = editable.options.source;

					for(var i = 0; i < source.length; ++i)
					{
						var source_text = $.trim(source[i].text);
						var source_id = source[i].id;

						for(var j in values)
						{
							if($.trim(values[j]) == source_text)
							{
								indexes.push(source_id);
								//splice out string we have already found
								values.splice(j,1);
							}
						}
					}
					
					var value = "";

					for(var i = 0; i < indexes.length; ++i)
					{
						if(i == 0)
							value += indexes[i];
						else
							value += "," + indexes[i];
					}
					editable.value = value;//"5,2";
				}',
				*/
				/*
				'display' => 'js: function(value, sourceData) {
				    var escapedValue = $("<div>").text(value).html();
				    //$(this).data("value",2);
				    //$(this).html("<b>"+escapedValue+"</b>");
				}'
				*/
    
                /*CHtml::dropdownList('role_id','',CHtml::listData(Role::model()->findAll(), 'id', 'name'),
     						array('empty'=>'','multiple'=>true ,'style'=>'width:400px;','size'=>'10'))
     			*/
                //Editable::source(Role::model()->findAll(),'id','name'),//CHtml::listData(Category::model()->findAll(), 'type', 'type'),
                //'value' => $model->role_id
			),
			'filter' => CHtml::listData(Role::model()->findAll(array('order' => 'name DESC')), 'id', 'name'),
			//'value' => 'createRolesString($data)'
			'value' => '$data->memberRoles',
		),
		array(
			'name'=>'usbc_number',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
			)
		),
		array(
			'name'=>'tnba_number',
			//'class'=>'TbEditableColumn',
			/*
			'editable'=> array(
				'type' => 'number',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
                'success'=>'js:function(response) {
                	readResponse(response);
                }'
			)
			*/
		),
		//'e_signature',
		array(
			'name'=>'birthday',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'date',
				'format' => 'yyyy-mm-dd',
				'viewformat'  => 'mm/dd/yyyy',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
                'options' => array(
                //'placement' => 'left'
                	'showbuttons' => 'bottom'
                )
			)
		),
		array(
			'name'=>'sex',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
                'source'=>Editable::source(Sex::model()->findAll(),'type','type'),//CHtml::listData(Category::model()->findAll(), 'type', 'type'),
                'value' => $model->sex
			),
			'filter' => CHtml::listData(Sex::model()->findAll(array('order' => 'type DESC')), 'type', 'type')
		),
		array(
			'name'=>'street_address',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
			)
		),
		array(
			'name'=>'zipcode',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
                'success'=>'js:function(response) {
                	readResponse(response);
                }'
			)
		),
		array(
			'name'=>'phone_number',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
			)
		),
		
		array(
			'name' => 'senate',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
                'source'=>Editable::source(Senate::model()->findAll(),'name','name'),
                'value' => $model->senate
			),
			'filter' => CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),
		array(
			'name' => 'senate_region',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/member/saveEdits'),
                'source'=>Editable::source(Region::model()->findAll(),'name','name'),
                'value' => $model->senate_region
			),
			'filter' => CHtml::listData(Region::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),
		array(
			'name'=> 'w_9_id',
			'type' => 'raw',
			'value' => 'createW9Column($data)',
			'filter' => false
			 //'($data->w_9_id) ? CHtml::link("Download W9",array("/crud/member/downloadW9","id"=>$data->id)) : CHtml::fileField("w9_$data->id", "", array("size" => 55, "maxlength" => 55));'
		),
		/*
		array(
			//'label' => 'w_9_id',
			//'class' => 'CLinkColumn',
			//'urlExpression' => (!is_null($model->w_9_id)) ? 'Yii::app()->createUrl("/crud/member/downloadW9",array("id"=>$data->id))' : '"No W9"'
			'name' => 'w_9_id',
			'type' => 'raw',
			'value' => (!is_null($model->w_9_id)) ? 'CHtml::button("Download W9",array("onclick"=>"document.location.href="' . Yii::app()->createUrl("crud/member/downloadW9",array("id"=>'$data->id')) . '"))' : '"No W9 Uploaded"'//(!is_null($model->w_9_filepath)) ? 'YES' : 'NO'
		),
		*/
	),
)); 

?>

<?php $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'myModal')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Add Member</h4>
    </div>
 
    <div class="modal-body">
        <div class="form">

			<?php $form=$this->beginWidget('CActiveForm', array(
			    'id'=>'create_member_form',
			    'action'=>'createMember',
			    'enableAjaxValidation'=>false,
			    'htmlOptions' => array(
			        'enctype' => 'multipart/form-data',
			    ),
			)); ?>

			    <p class="note">Fields with <span class="required">*</span> are required. </p>


			    <?php echo $form->errorSummary($model); ?>
				<?php 
					$bowlingAlley = new BowlingAlley();
					$roleMemberId = new RoleMemberId();
					$w9 = new W9();
				?>
			    <div>
			        <?php echo $form->labelEx($model,'first_name'); ?>
			        <?php echo $form->textField($model,'first_name'); ?>
			        <?php echo $form->error($model,'first_name'); ?>
			    </div>

			   <div>
			        <?php echo $form->labelEx($model,'middle_name'); ?>
			        <?php echo $form->textField($model,'middle_name'); ?>
			        <?php echo $form->error($model,'middle_name'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'last_name'); ?>
			        <?php echo $form->textField($model,'last_name'); ?>
			        <?php echo $form->error($model,'last_name'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'email'); ?>
			        <?php echo $form->textField($model,'email'); ?>
			        <?php echo $form->error($model,'email'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'certified'); ?>
			        <?php echo $form->dropDownList($model,'certified',array("1"=>"YES","0"=>"NO")); ?>
			        <?php echo $form->error($model,'certified'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'category_type'); ?>
			        <?php echo $form->dropDownList($model,'category_type', CHtml::listData(Category::model()->findAll(), 'type', 'type')); ?>
			        <?php echo $form->error($model,'category_type'); ?>
			    </div>
				
				<div>
					<?php echo $form->labelEx($roleMemberId,'role_id'); ?>
					<?php echo $form->dropDownList($roleMemberId,'role_id', CHtml::listData(Role::model()->findAll(), 'id', 'name'), array('multiple' => 'multiple')); ?>
					<?php echo $form->error($roleMemberId,'role_id'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'usbc_number'); ?>
			        <?php echo $form->textField($model,'usbc_number'); ?>
			        <?php echo $form->error($model,'usbc_number'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'birthday'); ?>
			        <?php echo $form->dateField($model,'birthday'); ?>
			        <?php echo $form->error($model,'birthday'); ?>
			    </div>
				
				<div>
			        <?php echo $form->labelEx($model,'sex'); ?>
			        <?php echo $form->dropDownList($model,'sex', CHtml::listData(Sex::model()->findAll(array('order' => 'type DESC')), 'type', 'type')); ?>
			        <?php echo $form->error($model,'sex'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'street_address'); ?>
			        <?php echo $form->textField($model,'street_address'); ?>
			        <?php echo $form->error($model,'street_address'); ?>
			    </div>

			    <div>
					<?php echo $form->labelEx($model,'zipcode'); ?>
					<?php $this->widget('zii.widgets.jui.CJuiAutoComplete',
					    array(
					      'model'=>$model,
					      'name'=>'zipcode',
					      'attribute'=>'zipcode',
					      'sourceUrl'=>Yii::app()->createUrl('crud/member/autocomplete'),//'autocomplete',
					      'htmlOptions'=>array('placeholder'=>'Zipcode'),
					      'options'=>
					         array(
					   				'minLength'=>'2', // min chars to start search
					               	'showAnim'=>'fold',
					               	'maxLength'=>5
					                )
					    )); 
					?>
					<?php echo $form->error($model,'zipcode'); ?>
				</div>

			    <div>
			        <?php echo $form->labelEx($model,'phone_number'); ?>
			        <?php echo $form->textField($model,'phone_number'); ?>
			        <?php echo $form->error($model,'phone_number'); ?>
			    </div>

			    <div>
			        <?php echo $form->labelEx($model,'senate'); ?>
			        <?php echo $form->dropDownList($model,'senate', CHtml::listData(Senate::model()->findAll(), 'name', 'name')); ?>
			        <?php echo $form->error($model,'senate'); ?>
			    </div>
	
				<div>
			        <?php echo $form->labelEx($model,'senate_region'); ?>
			        <?php echo $form->dropDownList($model,'senate_region', CHtml::listData(Region::model()->findAll(), 'name', 'name')); ?>
			        <?php echo $form->error($model,'senate_region'); ?>
			    </div>
				
				<div>
			        <?php echo $form->labelEx($model,'w_9_id'); ?>
			        <?php echo $form->fileField($model,'w_9_id'); ?>
			        <?php echo $form->error($model,'w_9_id'); ?>
			    </div>
				
			    <div class="buttons">
			        <?php echo CHtml::submitButton("Create Member"); ?>
			    </div>

			<?php $this->endWidget(); ?>

		</div><!-- form -->
    </div>
 
    <div class="modal-footer">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 
<?php $this->endWidget(); ?>

<?php

	function createW9Column($data)
	{
		$html = null;

		if($data->w_9_id)
		{
			$html = '<select class="up_down_select" id="up_down_select_'.$data->id.'"><option>Upload W9</option><option selected>Download W9</option></select>';
		
			$html .= ' <div id="up_down_div_'.$data->id.'">'.CHtml::link("Download W9",array("/crud/member/downloadW9","id"=>$data->id)).'</div>';
		}
		else
		{
			$html = '<select class="up_down_select" id="up_down_select_'.$data->id.'"><option selected>Upload W9</option><option>Download W9</option></select>';
		
			$html .= ' <div id="up_down_div_'.$data->id.'">'.CHtml::fileField("up_down_select_$data->id").'</div>';
		}

		return $html;
	}
	
	function createRolesString($data)
	{
		//error_log(print_r($data->roles[0]->name,true));
		error_log(print_r($data->r_role_id,true));

		$roles = Role::model()->findAllBySql("SELECT name FROM role WHERE id IN (SELECT role_id FROM role_member_id WHERE member_id = $data->id)");

		$val = "";

		$length = count($roles);

		for($i = 0; $i < $length; ++$i)
		{
			if($i == 0)
				$val .= $roles[$i]->name;
			else
				$val .= ", {$roles[$i]->name}";
		}

		//error_log("val: $val");
		return $val;
	}

?>
