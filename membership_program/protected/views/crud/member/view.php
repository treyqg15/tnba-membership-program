<?php
/* @var $this MemberController */
/* @var $model Member */

echo "<br /><br /><br />";
$this->menu=array(
	array('label'=>'Create Member', 'url'=>array('create')),
	array('label'=>'Update Member', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Member', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Member', 'url'=>array('admin')),
);
?>

<h3><?php echo $model->first_name . ' ' . $model->middle_name . ' ' . $model->last_name ?></h3>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'first_name',
		'middle_name',
		'last_name',
		'email',
		'category_type',
		'role_id' => array(
			'name'=> 'Role',
			'value'=> ($model->role_id != NULL) ? Role::model()->FindByPk($model->role_id)->name : ''
		),
		'usbc_number',
		'tnba_number',
		'e_signature',
		array(
			'name' => 'birthday',
			'value'=>Yii::app()->dateFormatter->format("MM-d-yyyy",strtotime($model->birthday))
		),
		'sex',
		'street_address',
		'zipcode',
		'phone_number',
		'senate',
		'senate_region',
		array(
			'name' => 'W-9',
			'type' => 'raw',
			'value' => (!is_null($model->w_9_id)) ? CHtml::link('Download W9',array("crud/member/downloadW9","id"=>$model->id)) : 'No W9 Uploaded'//(!is_null($model->w_9_filepath)) ? 'YES' : 'NO'
		),
	),
)); ?>
