<?php
	
	error_log("statsHistory");
	error_log("tnbaNumber: " . Yii::app()->session['searched_tnba_number']);
	error_log("tnba_number: $tnba_number");

//LIMIT 1 should only be temporary. There should never be multiple USBC #'s in Member table'
$model = StatsHistory::model()->findBySql("SELECT * FROM stats_history where bowler_member_id = (SELECT id FROM member WHERE tnba_number = :tnbaNumber LIMIT 1)",array(':tnbaNumber' => $tnba_number));

$model = new StatsHistory();

if(isset($_GET['StatsHistory']))
	$model->attributes=$_GET['StatsHistory'];

error_log(print_r($model,true));

if(!is_null($model))
{
	$this->widget('zii.widgets.grid.CGridView', array(
		'id'=>'stats_history_grid',
		'dataProvider'=>$model->retrieveStatsHistoryByTNBANumber($tnba_number),
		'columns'=>array(
			//'id',
			array(
				'name'=>'first_name',
				'value'=>'$data->member->first_name'
			),
			array(
				'name'=>'middle_name',
				'value'=>'$data->member->middle_name'
			),
			array(
				'name'=>'last_name',
				'value'=>'$data->member->last_name'
			),
			array(
				'name'=>'league_name',
				'value'=>'$data->league->name'
			),
			array(
				'name'=>'bowling_alley_name',
				'value'=>'$data->league->bowlingAlley->name'
			),
			'average',
			'season'
		),
	)); 
}
?>