<?php
/* @var $this MemberController */
/* @var $model Member */

echo "<br /><br />";

$this->menu=array(
	array('label'=>'Create Member', 'url'=>array('create')),
	array('label'=>'View Member', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Member', 'url'=>array('admin')),
);
?>

<h3> <?php echo $model->first_name . ' ' . $model->middle_name . ' ' . $model->last_name ?></h3>

<?php $this->renderPartial('_form', array('model'=>$model,'action'=>'update','id'=>$model->id)); ?>