<?php
	
	require_once(Yii::app()->basePath.'/../php/tcpdf/TNBAPDF.php');
?>

<br />
<br />
<p>Your new TNBA Number is: <b><?php echo $tnbaNumber ?></b>. You can download and print your card by clicking the Download button below</p>

<form action="<?php echo Yii::app()->createUrl('crud/member/generateTNBACard');?>" method="GET">
	<input type="hidden" name="memberID" value="<?php echo $memberID; ?>" />
	<input type="submit" value="Download" />
</form>