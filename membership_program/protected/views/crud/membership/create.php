<?php
/* @var $this MembershipController */
/* @var $model Membership */

echo "<br /><br />";

$this->menu=array(
	array('label'=>'Manage Membership', 'url'=>array('admin')),
);
?>

<h1>Create Membership</h1>

<?php $this->renderPartial('_form', array('model'=>$model,
										  'memberModel'=>$memberModel)); ?>