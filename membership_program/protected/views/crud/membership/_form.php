<?php
/* @var $this MembershipController */
/* @var $model Membership */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'membership-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($memberModel,'tnba_number'); ?>
		<?php echo $form->textField($memberModel,'tnba_number'); ?>
		<?php echo $form->error($memberModel,'tnba_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'dues_paid_league'); ?>
		<?php echo $form->textField($model,'dues_paid_league',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'dues_paid_league'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->dropDownList($model,'status', CHtml::listData(MembershipStatus::model()->findAll(array('order' => 'status ASC')), 'status', 'status')); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'creation_date'); ?>
		<?php echo $form->textField($model,'creation_date'); ?>
		<?php echo $form->error($model,'creation_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'paid_datetime'); ?>
		<?php echo $form->textField($model,'paid_datetime'); ?>
		<?php echo $form->error($model,'paid_datetime'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type', CHtml::listData(MembershipType::model()->findAll(array('order' => 'type ASC')), 'type', 'type')); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->