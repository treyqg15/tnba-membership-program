<?php
/* @var $this MembershipController */
/* @var $model Membership */

echo "<br /><br />";

Yii::app()->clientScript->registerCoreScript('jquery');

//Javascript
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/common.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/global/admin.js');
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/membership/manage.js');


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#membership-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Memberships</h1>

<!-- Give JS pages url to controller -->
<script type="text/Javascript">
var membershipControllerUrl = "<?php echo Yii::app()->createUrl('crud/membership'); ?>";
var statusesDropDownOptions = "<?php echo $statusesDropDownOptions; ?>";
var typesDropDownOptions = "<?php echo $typesDropDownOptions; ?>";
</script>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<form id="memberships_reports_form" action="<?php echo Yii::app()->createUrl('crud/membership/runReport');?>" method="GET">
		<input type="hidden" name="function_to_call_field" />
		<input type="hidden" id="data_field" name="data_field" />
		<input type="hidden" id="report_type_field" name="report_type_field" />

<?php
$this->widget('bootstrap.widgets.TbButtonGroup', 
	array(
	    'buttons'=>array(
	        array(
			    'label'=>'Create Memberships',
			    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			    'size'=>'small', // null, 'large', 'small' or 'mini'
				'htmlOptions' => array(
		            'data-toggle' => 'modal',
		            'data-target' => '#myModal',
			    ),
			),
			array(
			    'label'=>'Delete Memberships',
			    'type'=>'danger', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			    'size'=>'small', // null, 'large', 'small' or 'mini'
				'htmlOptions'=>array('onclick' => "js: manageRow('membership-grid','Delete','". Yii::app()->createUrl('crud/membership/deleteMembership') . "')")
			),
			array('label'=>'Run Reports','items'=>array(
				array(
				    'label'=>'Export Grid',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#memberships_reports_form','EXPORT ALL')"),
		    	),
		    	array(
				    'label'=>'Export Selected Rows',
				    'buttonType'=>'submit', 
				    'type'=>'primary', // null, 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
				    'size'=>'small', // null, 'large', 'small' or 'mini'
				    'url'=>'#',
					'linkOptions'=>array('onclick' => "js: triggerReportTypeForm('#memberships_reports_form','EXPORT SELECTED ROWS')"),
		    	),
		    )
		)
	)
));

?>

</form>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'membership-grid',
	'dataProvider'=>$model->search(),
	'selectableRows'=>2,
	'afterAjaxUpdate'=>'function(id,data) {
		resetVars();
	}',
	'filter'=>$model,
	'columns'=>array(
		array(
			'name' => 'first_name',
			'value' => '$data->member->first_name',
		),
		array(
			'name' => 'middle_name',
			'value' => '$data->member->middle_name',
		),
		array(
			'name' => 'last_name',
			'value' => '$data->member->last_name',
		),
		array(
			'name' => 'tnba_number',
			'value' => '$data->member->tnba_number',
		),
		array(
			'name' => 'usbc_number',
			'value' => '$data->member->usbc_number',
		),
		array(
			'name'=>'status',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/membership/saveEdits'),
                'source'=>Editable::source(MembershipStatus::model()->findAll(),'status','status'),
                'value' => $model->status
			)
		),
		'creation_date',
		array(
			'name'=>'dues_paid_league',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'text',
                'url' => Yii::app()->createUrl('/crud/membership/saveEdits'),
                'value' => $model->dues_paid_league,
                'success' => 'js: function(response, newValue) {
				    $.fn.yiiGridView.update("membership-grid"); 
				}',
			)
		),
		array(
            'name'=>'paid_datetime',
            'header'=>'Dues Paid Date',
            'value'=> 'convertCreationTimeToDate($data->paid_datetime)'
        ),
		array(
			'name'=>'type',
			'class'=>'TbEditableColumn',
			'editable'=> array(
				'type' => 'select',
                'url' => Yii::app()->createUrl('/crud/membership/saveEdits'),
                'source'=>Editable::source(MembershipType::model()->findAll(),'type','type'),
                'value' => $model->type
			),
        ),
        array(
            'name'=>'senate',
            'value'=> '$data->member->senate',
            'filter' => CHtml::listData(Senate::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		),
		array(
            'name'=>'season',
            'filter' => CHtml::listData(Season::model()->findAll(array('order' => 'name ASC')), 'name', 'name')
		)
	),
));

?>

<?php $this->beginWidget(
    'bootstrap.widgets.TbModal',
    array('id' => 'myModal')
); ?>
 
    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4>Add Member</h4>
    </div>
 
    <div class="modal-body">
        <div class="form">

			<?php $form=$this->beginWidget('CActiveForm', array(
			    'id'=>'create_membership_form',
			    'action'=>'createMembership',
			    'enableAjaxValidation'=>false,
			    'htmlOptions' => array(
			        'enctype' => 'multipart/form-data',
			    ),
			)); ?>

			    <p class="note">Fields with <span class="required">*</span> are required. </p>


			    <?php echo $form->errorSummary($model); ?>
				<?php 
					$member = new Member();
				?>
			    <div>
			        <?php echo $form->labelEx($member,'tnba_number'); ?>
			        <?php echo $form->textField($member,'tnba_number'); ?>
			        <?php echo $form->error($member,'tnba_number'); ?>
			    </div>

				<!--
			   	<div>
					<?php echo $form->labelEx($model,'dues_paid_league'); ?>
			        <?php echo $form->textField($model,'dues_paid_league'); ?>
			        <?php echo $form->error($model,'dues_paid_league'); ?>
			   	</div>
				-->

			   	<div>
					<?php echo $form->labelEx($model,'status'); ?>
			        <?php echo $form->dropDownList($model,'status', CHtml::listData(MembershipStatus::model()->findAll(array('order' => 'status ASC')), 'status', 'status')); ?>
			        <?php echo $form->error($model,'status'); ?>
			   	</div>

			   	<div>
					<?php echo $form->labelEx($model,'dues_paid_league'); ?>
			        <?php echo $form->textField($model,'dues_paid_league'); ?>
			        <?php echo $form->error($model,'dues_paid_league'); ?>
			   	</div>

			   	<div>
					<?php echo $form->labelEx($model,'type'); ?>
			        <?php echo $form->dropDownList($model,'type', CHtml::listData(MembershipType::model()->findAll(array('order' => 'type ASC')), 'type', 'type')); ?>
			        <?php echo $form->error($model,'type'); ?>
			   	</div>

			   	<div>
					<?php echo $form->labelEx($model,'season'); ?>
			        <?php echo $form->dropDownList($model,'season', CHtml::listData(Season::model()->findAll(array('order' => 'name ASC')), 'name', 'name')); ?>
			        <?php echo $form->error($model,'season'); ?>
			   	</div>
				
			    <div class="buttons">
			        <?php echo CHtml::submitButton("Create Membership"); ?>
			    </div>

			<?php $this->endWidget(); ?>

		</div><!-- form -->
    </div>
 
    <div class="modal-footer">
        <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'label' => 'Close',
                'url' => '#',
                'htmlOptions' => array('data-dismiss' => 'modal'),
            )
        ); ?>
    </div>
 <?php
    
    function convertCreationTimeToDate($creationTime)
    {
        if(is_null($creationTime) || empty($creationTime))
            return 'NOT PAID';
        error_log(print_r($creationTime,true));
        $date = date('m-d-Y', strtotime($creationTime));
        return $date;
    }

?>
<?php $this->endWidget(); ?>