<?php
/* @var $this MembershipController */
/* @var $model Membership */

$this->menu=array(
	array('label'=>'Create Membership', 'url'=>array('create')),
	array('label'=>'View Membership', 'url'=>array('view', 'id'=>$model->membership_id)),
	array('label'=>'Manage Membership', 'url'=>array('admin')),
);
?>

<h1>Update Membership <?php echo $model->membership_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model,
										  'memberModel'=>$memberModel)); ?>