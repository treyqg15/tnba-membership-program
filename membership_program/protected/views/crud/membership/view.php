<?php
/* @var $this MembershipController */
/* @var $model Membership */

echo "<br /><br />";

$this->menu=array(
	array('label'=>'Create Membership', 'url'=>array('create')),
	array('label'=>'Update Membership', 'url'=>array('update', 'id'=>$model->membership_id)),
	array('label'=>'Delete Membership', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->membership_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Membership', 'url'=>array('admin')),
);
?>

<h1>View Membership #<?php echo $model->membership_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'membership_id',
		'member_id',
		'dues_paid_league',
		'status',
		'creation_datetime',
		'paid_datetime',
		'type',
	),
)); ?>
