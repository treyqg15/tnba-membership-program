<?php

class ManageController extends Controller
{
	public function actionAwards()
	{
		$this->render('awards');
	}

	public function actionBowlers()
	{
		$this->render('bowlers');
	}

	public function actionEvents()
	{
		$this->render('events');
	}

	public function actionLeagues()
	{
		$this->render('leagues');
	}

	public function actionMembers()
	{
		$this->render('members');
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}