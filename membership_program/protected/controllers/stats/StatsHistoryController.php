<?php

class StatsHistoryController extends Controller
{
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('viewBowlerHistory','index'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array(''),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('saveEdits','createBowlerHistory','deleteBowlerHistory','retrieveBowlingAlleysAndLeaguesForZipcode','retrieveLeagueForBowlingAlley','autoComplete','fetchLeagueForBowlingAlley','fetchBowlingAlleysForZipcode','runReport'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionViewBowlerHistory()
	{
		$model = new StatsHistory('search');
		$model->unsetAttributes();  // clear any default values
		$seasons = Helper::retrieveAllSeasons();
		$dropDownOptions = Helper::buildSeasonDropdown($seasons);

		if(isset($_GET['StatsHistory']))
			$model->attributes=$_GET['StatsHistory'];

		$this->render('viewBowlerHistory',array(
			'model'=>$model,
			'dropDownOptions'=>$dropDownOptions
		));
	}

	public function actionAddBowlerStatsHistory()
	{
		$model = new StatsHistory('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['StatsHistory']))
			$model->attributes=$_GET['StatsHistory'];

		$this->render('addBowlerStatsHistory',array(
			'model'=>$model
		));
	}

	public function actionIndex()
	{
		$this->actionViewBowlerHistory();
	}

	public function actionSaveEdits()
	{
		header('Content-Type: application/json');

		$error = new Error();
		$response = new Response();

		error_log(print_r($_POST,true));
		$name = $_POST['name'];
		$value = $_POST['value'];
		$member_id = $_POST['pk']['bowler_member_id'];
		$league_cert_number = $_POST['pk']['league_cert_number'];
		$season = $_POST['pk']['season'];
		
		$statsHistoryModel = StatsHistory::model()->findBySql("SELECT * FROM stats_history WHERE bowler_member_id = :id 
																							AND league_cert_number = :cert_number 
																							AND season = :season",
																							array(':id' => $member_id,
																								  ':cert_number' => $league_cert_number,
																								  ':season' => $season));
		$statsHistoryModel->isUpdate = true;

		$statsHistoryModel->$name = $value;

		if(!$statsHistoryModel->save())
		{
			//check if any errors were thrown by model validation rules
			if(!is_null($statsHistoryModel->getErrors()))
			{
				$error->buildMessageFromArray($statsHistoryModel->getErrors());
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());

				exit;
			}
		}

		$response->message = "Update Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionCreateBowlerHistory()
	{
		header('Content-Type: application/json');

		error_log(print_r($_POST,true));

		$memberData = $_POST['Member'];
		$bowlingAlleyData = $_POST['BowlingAlley'];
		$statsHistoryData = $_POST['StatsHistory'];

		$error = new Error();
		$response = new Response();
		

		try{

			if(empty($memberData['tnba_number']) && !is_numeric($memberData['tnba_number']))
			{
				error_log("Must enter a TNBA #");
				$error->addMessage("Must enter a TNBA #");
			}
			if(empty($statsHistoryData['league_cert_number']) && !is_numeric($statsHistoryData['league_cert_number']))
			{
				error_log("Must enter a League #");
				$error->addMessage("Must enter a League #");
			}
			if(empty($statsHistoryData['average']))
			{
				error_log("Must enter an Average");
				$error->addMessage("Must enter an Average");
			}
			if(empty($statsHistoryData['season']))
			{
				error_log("Must enter a Season");
				$error->addMessage("Must enter a Season");
			}

			if($error->hasMessage())
			{
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());
				exit;
			}

			if(Helper::TNBANumberExists($memberData['tnba_number']))
			{
				try 
				{
					/* NEED TRANSACTION */
					$statsHistory = new StatsHistory();
					$member = Member::model()->findBySql('SELECT id FROM member WHERE tnba_number = :tnbaNumber',array(':tnbaNumber'=>$memberData['tnba_number']));
					$statsHistoryData['bowler_member_id'] = $member->id;
					$statsHistory->attributes = $statsHistoryData;

					if(!$statsHistory->save())
					{
						//check if any errors were thrown by model validation rules
						if(!is_null($statsHistory->getErrors()))
						{
							$error->buildMessageFromArray($statsHistory->getErrors());
							$response->error = $error->toEncodableObject();
						}
					}		
				}
				catch(CDBException $e)
				{
					$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
					error_log($e->getMessage());
					error_log($e->errorInfo[1]);
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());
					exit;
				}
			}
			else
				throw new Exception("TNBA # {$memberData['tnba_number']} does not exist");
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		if(!is_null($response->error))
		{
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Save Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionDeleteBowlerHistory()
	{
		header('Content-Type: application/json');

		$data = json_decode($_POST['data'],true);

		error_log(print_r($data,true));

		try{
			if(empty($data))
				throw new Exception("No row was selected for deletion");

			$response = new Response();
			$error = new Error();

			foreach($data as $key => $value)
			{
				$connection=Yii::app()->db;
				if(is_null($connection->getCurrentTransaction()))
				{
					$transaction=$connection->beginTransaction();
				}

				$command = Yii::app()->db->createCommand();

				/*the grid sent the data as comma delimited so need to explode the data
				[0] => bowler_member_id
				[2] => league_cert_number
				[3] => season
				*/
				try{
					$row = explode(',',$value);
					$bowler_member_id = $row[0];
					$league_cert_number = $row[1];
					$season = $row[2];

					$command->delete('stats_history', 'bowler_member_id=:id AND league_cert_number=:lcn AND season=:seas', array(':id'=>$bowler_member_id,
																					':lcn'=>$league_cert_number,
																					':seas'=>$season));
					error_log("delete: $bowler_member_id,$league_cert_number,$season");
					$transaction->commit();
				}
				catch(Exception $e)
				{
					$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
					error_log($e->getMessage());
					error_log($e->errorInfo[1]);
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());
					$transaction->rollback();
					error_log("rollback");
					exit;
				}
			}
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Delete Successful";
		echo json_encode($response->toEncodableObject());
	}

	private function validateNewRows($data)
	{
		$error = new Error();
		$response = new Response();

		$num = 1;

		foreach($data as $key => $value)
		{
			$row = $data[$key];
			if(empty($row['tnba_number']) && !is_numeric($row['tnba_number']))
			{
				error_log("Must enter a TNBA # for row #$num");
				$error->addMessage("Must enter a TNBA # for row #$num");
			}
			if(empty($row['league_cert_number']) && !is_numeric($row['league_cert_number']))
			{
				error_log("Must enter a League # for row #$num");
				$error->addMessage("Must enter a League # for row #$num");
			}
			if(empty($row['average']))
			{
				error_log("Must enter an Average for row #$num");
				$error->addMessage("Must enter an Average for row #$num");
			}
			if(empty($row['season']))
			{
				error_log("Must enter a Season for row #$num");
				$error->addMessage("Must enter a Season for row #$num");
			}

			if($error->hasMessage())
			{
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());
				exit;
			}
		}
	}

	public function actionRetrieveBowlingAlleysAndLeaguesForZipcode()
	{
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		$zipcode = $_GET['zipcode'];
		error_log($zipcode);
		try{
			$bowlingAlleys = Helper::retrieveBowlingAlleysForZipcode($zipcode);
			$count = count($bowlingAlleys);

			if(count($bowlingAlleys) == 0)
				throw new Exception("No Bowling alleys for zipcode $zipcode");
			$bowlingAlleysDropdown = Helper::buildBowlingAlleysDropdown($bowlingAlleys);			

			$bowling_alley_id = $bowlingAlleys[0]->id;

			$leagues = Helper::retrieveLeagueForBowlingAlley($bowling_alley_id);
			$leaguesDropdown = Helper::buildLeaguesDropdown($leagues);	

			if(count($leaguesDropdown) == 0)
				throw new Exception("No leagues for bowling alley {$bowlingAlleys[0]->name}");
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());

			exit;
		}	

		$data = array(
			'bowlingAlleysDropdown' => $bowlingAlleysDropdown,
			'leaguesDropdown' => $leaguesDropdown
		);

		error_log(print_r($data,true));
		$response->data = $data;

		echo json_encode($response->toEncodableObject());
	}

	public function actionRetrieveLeagueForBowlingAlley()
	{
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		$bowling_alley_id = $_GET['bowling_alley_id'];
		error_log($bowling_alley_id);

		$leagues = Helper::retrieveLeagueForBowlingAlley($bowling_alley_id);
		$leaguesDropdown = Helper::buildLeaguesDropdown($leagues);		

		error_log(print_r($leaguesDropdown,true));
		$response->data = $leaguesDropdown;

		echo json_encode($response->toEncodableObject());
	}

	public function actionAutoComplete($term) {

		header('Content-type: application/json');

		$list = Helper::autoComplete($term);

    	echo json_encode($list);
	}

	public function actionFetchBowlingAlleysForZipcode($zipcode)
	{
		error_log("zipcode: $zipcode");
		$bowlingAlleys = Helper::fetchBowlingAlleysForZipcode($zipcode);
		echo CJSON::encode(Editable::source($bowlingAlleys,'id','name'));
	}

	public function actionFetchLeagueForBowlingAlley($id)
	{
		error_log("id: $id");
		$leagues = Helper::fetchLeaguesForBowlingAlley($id);
		echo CJSON::encode(Editable::source($leagues,'cert_number','name'));
	}

	public function actionRunReport()
	{
		define('GRID','STATSHISTORY');
		$data = json_decode($_GET['data_field']);
		$reportType = $_GET['report_type_field'];

		error_log(print_r($_GET,true));
		//error_log(print_r($rows,true));

		Helper::runReport(GRID,$reportType,$data);
		
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}