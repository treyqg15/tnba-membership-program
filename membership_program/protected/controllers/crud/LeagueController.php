<?php

class LeagueController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','saveEdits','createLeague','fetchBowlingAlleysForZipcode','autoComplete','deleteLeague','storeSelectedLeague','addMemberToLeague','fetchLeagueForBowlingAlley','deleteMemberFromLeague','runReport'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->actionAdmin();
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new League('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['League']))
			$model->attributes=$_GET['League'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actionFetchBowlingAlleysForZipcode($zipcode)
	{
		error_log("zipcode: $zipcode");
		$bowlingAlleys = Helper::fetchBowlingAlleysForZipcode($zipcode);
		echo CJSON::encode(Editable::source($bowlingAlleys,'id','name'));
	}

	public function actionCreateLeague()
	{
		error_log(print_r($_POST,true));

		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();
		$leaguePost = $_POST['League'];
		$bowlingAlleyPost = $_POST['BowlingAlley'];

		try 
		{
			$league = new League();
			$leaguePost['bowling_alley_id'] = $bowlingAlleyPost['id'];
			$leaguePost['cert_number'] = Helper::generateTNBANumber();
			$league->attributes = $leaguePost;
			error_log("new league");
			//error_log(print_r($league,true));

			if(!$league->save())
			{
				//check if any errors were thrown by model validation rules
				if(!is_null($league->getErrors()))
				{
					$error->buildMessageFromArray($league->getErrors());
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());

					exit;
				}
			}
		}
		catch(CDBException $e)
		{
			$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
			error_log($e->getMessage());
			error_log($e->errorInfo[1]);
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Save Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionSaveEdits()
	{
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		error_log(print_r($_POST,true));
		$name = $_POST['name'];
		$value = $_POST['value'];
		$id = $_POST['pk'];

		$league = League::model()->findByPk($id);
		
		try{

			$league->$name = $value;
			$league->save();

			//check if any errors were thrown by model validation rules
			if(!is_null($league->getErrors()))
			{
				$error->buildMessageFromArray($league->getErrors());
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());

				exit;
			}
		}
		catch(CDBException $e)
		{
			$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
			error_log($e->getMessage());
			error_log($e->errorInfo[1]);
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Edit Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionDeleteLeague()
	{
		header('Content-Type: application/json');

		$data = json_decode($_POST['data'],true);

		error_log(print_r($data,true));

		try{
			if(empty($data))
				throw new Exception("No row was selected for deletion");

			$response = new Response();
			$error = new Error();

			foreach($data as $key => $value)
			{
				$connection=Yii::app()->db;
				if(is_null($connection->getCurrentTransaction()))
				{
					$transaction=$connection->beginTransaction();
				}

				$command = Yii::app()->db->createCommand();

				try{
					$league_id = $value;

					$command->delete('league', 'cert_number=:league_id', array(':league_id'=>$league_id));
					error_log("delete: $league_id");
					$transaction->commit();
				}
				catch(Exception $e)
				{
					$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
					error_log($e->getMessage());
					error_log($e->errorInfo[1]);
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());
					$transaction->rollback();
					error_log("rollback");
					exit;
				}
			}
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Delete Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionDeleteMemberFromLeague()
	{
		header('Content-Type: application/json');

		$data = json_decode($_POST['data'],true);

		error_log(print_r($data,true));

		try{
			if(empty($data))
				throw new Exception("No row was selected for deletion");

			$response = new Response();
			$error = new Error();

			foreach($data as $key => $value)
			{
				$connection=Yii::app()->db;
				if(is_null($connection->getCurrentTransaction()))
				{
					$transaction=$connection->beginTransaction();
				}

				$command = Yii::app()->db->createCommand();

				try
				{
					$member_id = $value;

					$command->delete('league_member', 'member_id=:member_id', array(':member_id'=>$member_id));
					error_log("delete: $member_id");
					$transaction->commit();
				}
				catch(Exception $e)
				{
					$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
					error_log($e->getMessage());
					error_log($e->errorInfo[1]);
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());
					$transaction->rollback();
					error_log("rollback");
					exit;
				}
			}
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Delete Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionAddMemberToLeague()
	{
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		error_log(print_r($_POST,true));

		try
		{
			$memberData = $_POST['Member'];
			$leagueMemberData = $_POST['LeagueMember'];

			try
			{
				//find member_id by tnba number
				$memberModel = Helper::fetchMemberByTNBANumber($memberData['tnba_number']);
				if(is_null($memberModel))
					throw new Exception("No member with TNBANumber {$memberData['tnba_number']} exists");

				$memberID = $memberModel->id;

				//set league_cert_number
				$leagueMemberData['league_cert_number'] = Yii::app()->session['selected_league'];

				//set member_id
				$leagueMemberData['member_id'] = $memberID;

				$roles = Helper::retrieveRolesForMember($memberID);

				if(count($roles) > 0)
					$leagueMemberData['is_officer'] = 1;
				else
					$leagueMemberData['is_officer'] = 0;

				$leagueMemberModel = new LeagueMember();
				$leagueMemberModel->attributes = $leagueMemberData;

				if(!$leagueMemberModel->save())
				{
					//check if any errors were thrown by model validation rules
					if(!is_null($leagueMemberModel->getErrors()))
					{
						$error->buildMessageFromArray($leagueMemberModel->getErrors());
						$response->error = $error->toEncodableObject();
						echo json_encode($response->toEncodableObject());

						exit;
					}
				}
			}
			catch(CDBException $e)
			{
				$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
				error_log($e->getMessage());
				error_log($e->errorInfo[1]);
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());
				exit;
			}
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Successfully added Member to League";
		echo json_encode($response->toEncodableObject());
	}

	public function actionStoreSelectedLeague()
	{
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		error_log(print_r($_POST,true));
		Yii::app()->session['selected_league'] = $_POST['id'];

		$response->message = "Stored Successfully";

		echo json_encode($response->toEncodableObject());
	}

	public function actionFetchLeagueForBowlingAlley($id)
	{
		error_log("id: $id");
		$leagues = Helper::fetchLeaguesForBowlingAlley($id);
		echo CJSON::encode(Editable::source($leagues,'cert_number','name'));
	}

	public function actionRunReport()
	{
		define('GRID','LEAGUE');
		$data = json_decode($_GET['data_field']);
		$reportType = $_GET['report_type_field'];

		error_log(print_r($_GET,true));
		//error_log(print_r($rows,true));

		Helper::runReport(GRID,$reportType,$data);
		
	}

	public function actionAutoComplete($term)
	{
		$list = Helper::autoComplete($term);
		echo json_encode($list);
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return League the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=League::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param League $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='league-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
