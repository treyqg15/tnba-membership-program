<?php

class MembershipController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','createMembership','saveEdits','deleteMembership','runReport'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$this->actionAdmin();
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Membership('search');
		$model->unsetAttributes();  // clear any default values

		//get status dropdown info
		$statuses = Helper::retrieveAllStatuses();
		$statusesDropDownOptions = Helper::buildStatusesDropdown($statuses);

		$types = Helper::retrieveAllTypes();
		$typesDropDownOptions = Helper::buildTypesDropdown($types);

		if(isset($_GET['Membership']))
			$model->attributes=$_GET['Membership'];

		$this->render('admin',array(
			'model'=>$model,
			'statusesDropDownOptions'=>$statusesDropDownOptions,
			'typesDropDownOptions'=>$typesDropDownOptions
		));
	}

	public function actionCreateMembership()
	{
		error_log(print_r($_POST,true));

		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();
		$membershipPost = $_POST['Membership'];
		$memberPost = $_POST['Member'];

		try{
			
			$membership = new Membership();
			$member = Helper::fetchMemberByTNBANumber($memberPost['tnba_number']);
			
			//if no member is found, display error
			if(is_null($member))
				throw new Exception("Member with TNBA # {$memberPost['tnba_number']} does not exist");

			error_log("member id: {$member->id}");
			$membershipPost['member_id'] = $member->id;
			$membership->attributes = $membershipPost;
			$membership->creation_date = new CDbExpression('NOW()');
			if(!is_null($membershipPost['dues_paid_league']) && !empty($membershipPost['dues_paid_league']))
				$membership->paid_datetime = new CDbExpression('NOW()');
			
			error_log("new membership");

			if(!$membership->save())
			{
				//check if any errors were thrown by model validation rules
				if(!is_null($membership->getErrors()))
				{
					$error->buildMessageFromArray($membership->getErrors());
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());

					exit;
				}
			}
		}
		catch(CDBException $e)
		{
			$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
			error_log($e->getMessage());
			error_log($e->errorInfo[1]);
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			error_log("rollback");
			exit;
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());

			exit;
		}

		$response->message = "Save Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionDeleteMembership()
	{
		header('Content-Type: application/json');

		$data = json_decode($_POST['data'],true);

		error_log(print_r($data,true));

		try{
			if(empty($data))
				throw new Exception("No row was selected for deletion");

			$response = new Response();
			$error = new Error();

			foreach($data as $key => $value)
			{
				$connection=Yii::app()->db;
				if(is_null($connection->getCurrentTransaction()))
				{
					$transaction=$connection->beginTransaction();
				}

				$command = Yii::app()->db->createCommand();

				try{
					$id = $value;

					$command->delete('membership', 'membership_id=:id', array(':id'=>$id));
					error_log("delete: $id");
					$transaction->commit();
				}
				catch(Exception $e)
				{
					$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
					error_log($e->getMessage());
					error_log($e->errorInfo[1]);
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());
					$transaction->rollback();
					error_log("rollback");
					exit;
				}
			}
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Delete Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionSaveEdits()
	{
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		error_log(print_r($_POST,true));
		$name = $_POST['name'];
		$value = $_POST['value'];
		$id = $_POST['pk'];

		$membership = Membership::model()->findByPk($id);

		try{

			$membership->$name = $value;
			if($name == "dues_paid_league")
				$membership->paid_datetime = new CDbExpression('NOW()');
			error_log(print_r($membership->attributes,true));

			if(!$membership->save(false))
			{
				//check if any errors were thrown by model validation rules
				if(!is_null($membership->getErrors()))
				{
					error_log(print_r($membership->getErrors(),true));
					$error->buildMessageFromArray($membership->getErrors());
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());

					exit;
				}
			}
		}
		catch(CDBException $e)
		{
			$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
			error_log($e->getMessage());
			error_log($e->errorInfo[1]);
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Edit Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionRunReport()
	{
		error_log(print_r($_GET,true));
		define('GRID','MEMBERSHIP');

		$data = json_decode($_GET['data_field']);
		$reportType = $_GET['report_type_field'];

		Helper::runReport(GRID,$reportType,$data);
	}

	private function validateNewRows($data)
	{
		$error = new Error();
		$response = new Response();

		$num = 1;

		foreach($data as $key => $value)
		{
			$row = $data[$key];
			if(empty($row['tnba_number']))
			{
				error_log("Must enter a TNBA # for row #$num");
				$error->addMessage("Must enter a TNBA # for row #$num");
			}
			if(empty($row['status']))
			{
				error_log("Must enter a Membership Status for row #$num");
				$error->addMessage("Must enter a Membership Status for row #$num");
			}
			if(empty($row['type']))
			{
				error_log("Must enter a Membership Type for row #$num");
				$error->addMessage("Must enter a Membership Type for row #$num");
			}

			++$num;
		}

		if($error->hasMessage())
		{
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}
	}

	private function retrieveMemberByTnbaNumber($tnbaNumber)
	{
		return Member::model()->findBySql('SELECT id FROM member WHERE tnba_number = :tnbaNumber',array(':tnbaNumber'=>$tnbaNumber));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Membership the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Membership::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Membership $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='membership-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
