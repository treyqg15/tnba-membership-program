<?php

class AwardsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';



	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','autocomplete','autoCompleteTNBANumber'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','report','test','runReport'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','createAward','deleteAward','saveEdits','retrieveBowlingAlleysAndLeaguesForZipcode','retrieveLeagueForBowlingAlley','fetchLeagueForBowlingAlley','retrieveBowlingAlleys','deleteAward','fetchBowlingAlleysForZipcode','saveGames','storeGamesSessionInfo','manageAwards'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new AwardsHighAverageBowler('search');
		$ZipcodeModel = new Zipcode();
		$memberModel = new Member();

		//get award status dropdown info
		$awardStatuses = Helper::retrieveAllAwardStatuses();
		$awardStatusesDropDownOptions = Helper::buildAwardStatusesDropdown($awardStatuses);

		//get honor scores dropdown info
		$honorScores = Helper::retrieveAllHonorScores();
		$honorScoresDropDownOptions = Helper::buildHonorScoresDropdown($honorScores);

		//get senates dropdown info
		$senates = Helper::retrieveAllSenates();
		$senatesDropdownOptions = Helper::buildSenatesDropdown($senates);

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AwardsHighAverageBowler']))
			$model->attributes=$_GET['AwardsHighAverageBowler'];

		$this->render('admin',array(
			'model'=>$model,
			'ZipcodeModel'=>$ZipcodeModel,
			'memberModel'=>$memberModel,
			'awardStatusesDropDownOptions'=>$awardStatusesDropDownOptions,
			'honorScoresDropDownOptions'=>$honorScoresDropDownOptions,
			'senatesDropdownOptions'=>$senatesDropdownOptions
		));
	}

	public function actionCreateAward()
	{
		header('Content-Type: application/json');

		error_log(print_r($_POST,true));

		$memberData = $_POST['Member'];
		$awardsData = $_POST['AwardsHighAverageBowler'];
		$bowlingAlleyData = $_POST['BowlingAlley'];
		//$leagueData = $_POST['League'];

		$response = new Response();
		$error = new Error();


		try{
			if(empty($memberData['tnba_number']) && !is_numeric($memberData['tnba_number']))
			{
				error_log("Must enter a TNBA #");
				$error->addMessage("Must enter a TNBA #");
			}
			if(empty($awardsData['status']))
			{
				error_log("Must enter a Membership Status");
				$error->addMessage("Must enter a Membership Status");
			}
			if(empty($awardsData['date_of_performance']))
			{
				error_log("Must enter a Date Of Performance");
				$error->addMessage("Must enter a Date Of Performance");
			}
			if(empty($awardsData['honor_score']))
			{
				error_log("Must enter a Honor Score");
				$error->addMessage("Must enter a Honor Score");
			}
			if(empty($awardsData['current_average']))
			{
				error_log("Must enter a Current Average");
				$error->addMessage("Must enter a Current Average");
			}
			if(empty($awardsData['name_of_senate']))
			{
				error_log("Must enter a Senate");
				$error->addMessage("Must enter a Senate");
			}
			
			if($error->hasMessage())
			{
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());
				exit;
			}

			try 
			{
				//start transaction
				$connection=Yii::app()->db;
				if(is_null($connection->getCurrentTransaction()))
					$transaction = $connection->beginTransaction();

				$awards = new AwardsHighAverageBowler();
				$awards->isCreating = true;

				//if no member is found, display error
				$member = Helper::fetchMemberByTNBANumber($memberData['tnba_number']);

				//if no member is found, display error
				if(is_null($member))
					throw new Exception("Member with TNBA # {$memberData['tnba_number']} does not exist");

				$awardsData['member_id'] = $member->id;

				$awards->attributes = $awardsData;
				$awards->date_of_performance = $awardsData['date_of_performance'];
				error_log("new awards");
				//error_log(print_r($awards,true));
				
				if(!$awards->save())
				{
					//check if any errors were thrown by model validation rules
					if(!is_null($awards->getErrors()))
					{
						$error->buildMessageFromArray($awards->getErrors());
						$response->error = $error->toEncodableObject();
						echo json_encode($response->toEncodableObject());

						exit;
					}
				}
			}
			catch(CDBException $e)
			{
				$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
				error_log($e->getMessage());
				error_log($e->errorInfo[1]);
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());
				$transaction->rollback();
				error_log("rollback");
				exit;
			}
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());

			exit;
		}

		$transaction->commit();
		$response->message = "Save Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionDeleteAward()
	{
		header('Content-Type: application/json');

		$data = json_decode($_POST['data'],true);

		error_log(print_r($data,true));

		try{
			if(empty($data))
				throw new Exception("No row was selected for deletion");

			$response = new Response();
			$error = new Error();

			foreach($data as $key => $value)
			{
				$connection=Yii::app()->db;
				if(is_null($connection->getCurrentTransaction()))
				{
					$transaction=$connection->beginTransaction();
				}

				$command = Yii::app()->db->createCommand();

				try{
					$awards_id = $value;

					$command->delete('awards_high_average_bowler', 'id=:awards_id', array(':awards_id'=>$awards_id));
					error_log("delete: $awards_id");
					$transaction->commit();
				}
				catch(Exception $e)
				{
					$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
					error_log($e->getMessage());
					error_log($e->errorInfo[1]);
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());
					$transaction->rollback();
					error_log("rollback");
					exit;
				}
			}
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Delete Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionSaveEdits()
	{
		/*
		Yii::import('bootstrap.widgets.TbEditableSaver');
	    $es = new TbEditableSaver($_GET['model']);  // 'modelName' is classname of model to be updated
	    $es->update();
	    */
		
		//error_log("model: {$_GET['model']}");
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		//error_log("awards id: $awards_id");

		error_log(print_r($_POST,true));
		$name = $_POST['name'];
		$value = $_POST['value'];
		$id = $_POST['pk'];

		$awards = AwardsHighAverageBowler::model()->findByPk($id);
		//error_log(print_r($awards,true));
		if($name == "name" || $name == "zipcode")
		{
			$league = League::model()->findByPk($awards->league_cert_number);
			$bowlingAlley = BowlingAlley::model()->findByPk($league->bowling_alley_id);

			error_log(print_r($bowlingAlley,true));

			$dropdowns = $this->retrieveBowlingAlleysAndLeaguesForZipcode($value);
			$bowlingAlleysDropdown = $dropdowns[0];
			$leaguesDropdown = $dropdowns[1];

			$cert_number = $leaguesDropdown[0]->cert_number;

			$awards->league_cert_number = $cert_number;

			$data = array(
				'bowlingAlleysDropdown' => $bowlingAlleysDropdown,
				'leaguesDropdown' => $leaguesDropdown
			);

			if(!$awards->save())
			{
				//check if any errors were thrown by model validation rules
				if(!is_null($awards->getErrors()))
				{
					$error->buildMessageFromArray($awards->getErrors());
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());

					exit;
				}
			}

			error_log(print_r($data,true));
			$response->data = $data;

			echo json_encode($response->toEncodableObject());
		}
		else
		{
			$awards->$name = $value;
			try
			{
				if(!$awards->save())
				{
					//check if any errors were thrown by model validation rules
					if(!is_null($awards->getErrors()))
					{
						$error->buildMessageFromArray($awards->getErrors());
						$response->error = $error->toEncodableObject();
						echo json_encode($response->toEncodableObject());

						exit;
					}
				}
			}
			catch(CDBException $e)
			{
				$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
				error_log($e->getMessage());
				error_log($e->errorInfo[1]);
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());
				exit;
			}

			$response->message = "Edit Successful";
			echo json_encode($response->toEncodableObject());
		}
		
	}

	public function actionSaveGames()
	{
		$response = new Response();
		$error = new Error();

		error_log("save games");
		error_log(print_r($_POST,true));
		$awardsGamesPost = $_POST['AwardsGames'];
		$awardsID = Yii::app()->session['awardID'];
		$shouldUpdate = false;

		$awardsGamesModel = AwardsGames::model()->findBySql("SELECT * FROM awards_games WHERE awards_id = :id",array(':id'=>$awardsID));

		if(!is_null($awardsGamesModel))
		{
			error_log("update existing games for awardsID: $awardsID");
			$shouldUpdate = true;
		}
		else
			$awardsGamesModel = new AwardsGames();

		$honorScoreName = Yii::app()->session['honorScore'];

		$isSingleGame = HonorScore::model()->findByPk($honorScoreName)->isSingleGame;

		$awardsGamesPost['isSingleGame'] = $isSingleGame;
		$awardsGamesPost['awards_id'] = Yii::app()->session['awardID'];

		$awardsGamesModel->attributes = $awardsGamesPost;

		if($shouldUpdate)
		{
			if(!$awardsGamesModel->update())
			{
				//check if any errors were thrown by model validation rules
				if(!is_null($awardsGamesModel->getErrors()))
				{
					$error->buildMessageFromArray($awardsGamesModel->getErrors());
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());

					exit;
				}
			}
		}
		else
		{
			if(!$awardsGamesModel->save())
			{
				//check if any errors were thrown by model validation rules
				if(!is_null($awardsGamesModel->getErrors()))
				{
					$error->buildMessageFromArray($awardsGamesModel->getErrors());
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());

					exit;
				}
			}
		}

		$response->message = "Save Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionReport()
	{
		$model=new AwardsHighAverageBowler('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['AwardsHighAverageBowler']))
			$model->attributes=$_GET['AwardsHighAverageBowler'];

		$this->render('report',array(
			'model'=>$model,
		));
	}

	public function actionManageAwards()
	{
		$model=new AwardsGames;

		$this->render('manage_awards',array(
			'model'=>$model,
		));
	}

	public function actionStoreGamesSessionInfo()
	{
		$response = new Response();
		$error = new Error();

		try{
			$awardID = $_POST['awardID'];
			$honorScore = $_POST['honorScore'];

			error_log(print_r($_POST,true));
			Yii::app()->session['awardID'] = $awardID;
			Yii::app()->session['honorScore'] = $honorScore;
		}
		catch(Exception $e)
		{
			$error->buildMessageFromArray($awardsGames->getErrors());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());

			exit;
		}
		$response->message = "message";
		echo json_encode($response->toEncodableObject());
	}

	private function retrieveBowlingAlleysAndLeaguesForZipcode($zipcode)
	{
		$dropdowns = array();
		$bowlingAlleys = Helper::retrieveBowlingAlleysForZipcode($zipcode);
		$count = count($bowlingAlleys);

		if(count($bowlingAlleys) == 0)
			throw new Exception("No Bowling alleys for zipcode $zipcode");

		$bowlingAlleysDropdown = Helper::buildBowlingAlleysDropdown($bowlingAlleys);			

		$bowling_alley_id = $bowlingAlleys[0]->id;

		$leagues = Helper::retrieveLeagueForBowlingAlley($bowling_alley_id);
		$leaguesDropdown = Helper::buildLeaguesDropdown($leagues);	

		if(count($leaguesDropdown) == 0)
			throw new Exception("No leagues for bowling alley {$bowlingAlleys[0]->name}");

		array_push($dropdowns,$bowlingAlleysDropdown);
		array_push($dropdowns,$leaguesDropdown);

		return $dropdowns;
	}

	public function actionRetrieveBowlingAlleysAndLeaguesForZipcode()
	{
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		$bowlingAlleysDropdown = null;
		$leaguesDropdown = null;

		$zipcode = $_GET['zipcode'];
		error_log($zipcode);
		try{

			$dropdowns = $this->retrieveBowlingAlleysAndLeaguesForZipcode($zipcode);
			$bowlingAlleysDropdown = $dropdowns[0];
			$leaguesDropdown = $dropdowns[1];
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());

			exit;
		}	

		$data = array(
			'bowlingAlleysDropdown' => $bowlingAlleysDropdown,
			'leaguesDropdown' => $leaguesDropdown
		);

		error_log(print_r($data,true));
		$response->data = $data;

		echo json_encode($response->toEncodableObject());
	}

	public function actionRetrieveLeagueForBowlingAlley()
	{
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		$bowling_alley_id = $_GET['bowling_alley_id'];
		error_log($bowling_alley_id);

		$leagues = Helper::retrieveLeagueForBowlingAlley($bowling_alley_id);
		$leaguesDropdown = Helper::buildLeaguesDropdown($leagues);		

		error_log(print_r($leaguesDropdown,true));
		$response->data = $leaguesDropdown;

		echo json_encode($response->toEncodableObject());
	}

	public function actionTest()
	{
		echo CJSON::encode(CHtml::listData(BowlingAlley::model()->findAll(), 'id', 'name'));
	}

	public function actionRunReport()
	{
		require_once(Yii::app()->basePath.'/../php/tcpdf/TNBAPDF.php');

		define('GRID','AWARD');
		$data = json_decode($_GET['data_field']);
		$reportType = $_GET['report_type_field'];

		error_log(print_r($_GET,true));
		//error_log(print_r($rows,true));

		Helper::runReport(GRID,$reportType,$data);
		
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return AwardsHighAverageBowler the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=AwardsHighAverageBowler::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param AwardsHighAverageBowler $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='awardsHighAverageBowler-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionAutoComplete($term) {

		header('Content-type: application/json');

		$list = Helper::autoComplete($term);

    	echo json_encode($list);
	}

	public function actionAutoCompleteTNBANumber($term) {

		header('Content-type: application/json');

		$connection=Yii::app()->db;   // assuming you have configured a "db" connection
		// If not, you may explicitly create a connection:
		// $connection=new CDbConnection($dsn,$username,$password);
		error_log("SELECT tnba_number FROM member WHERE tnba_number LIKE '".$term."%'");
		$command=$connection->createCommand("SELECT tnba_number FROM member WHERE tnba_number LIKE '".$term."%'");
		$dataReader=$command->query();
		$list = array();

		foreach($dataReader as $row) 
		{ 
			$data['value'] = $row['tnba_number'];
			$data['label'] = $row['tnba_number'];

			$list[] = $data;

			unset($data);
		}

		echo json_encode($list);
	}

	public function actionFetchBowlingAlleysForZipcode($zipcode)
	{
		error_log("zipcode: $zipcode");
		$bowlingAlleys = Helper::fetchBowlingAlleysForZipcode($zipcode);
		echo CJSON::encode(Editable::source($bowlingAlleys,'id','name'));
	}

	public function actionFetchLeagueForBowlingAlley($id)
	{
		error_log("id: $id");
		$leagues = Helper::fetchLeaguesForBowlingAlley($id);
		echo CJSON::encode(Editable::source($leagues,'cert_number','name'));
	}

	private function validateNewRows($data)
	{
		$error = new Error();
		$response = new Response();

		$num = 1;

		foreach($data as $key => $value)
		{
			$row = $data[$key];
			
			if(empty($row['tnba_number']) && !is_numeric($row['tnba_number']))
			{
				error_log("Must enter a TNBA # for row #$num");
				$error->addMessage("Must enter a TNBA # for row #$num");
			}
			if(empty($row['status']))
			{
				error_log("Must enter a Membership Status for row #$num");
				$error->addMessage("Must enter a Membership Status for row #$num");
			}
			if(empty($row['date_of_performance']))
			{
				error_log("Must enter a Date Of Performance for row #$num");
				$error->addMessage("Must enter a Date Of Performance for row #$num");
			}
			if(empty($row['honor_score']))
			{
				error_log("Must enter a Honor Score for row #$num");
				$error->addMessage("Must enter a Honor Score for row #$num");
			}
			if(empty($row['current_average']))
			{
				error_log("Must enter a Current Average for row #$num");
				$error->addMessage("Must enter a Current Average for row #$num");
			}
			if(empty($row['name_of_senate']))
			{
				error_log("Must enter a Senate for row #$num");
				$error->addMessage("Must enter a Senate for row #$num");
			}

			++$num;
		}

		if($error->hasMessage())
		{
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}
	}

	//sets fields to NULL for the non-required fields that are left blank by the user
	private function foreignKeyNull() {

		$model = new AwardsHighAverageBowler;
		$post = $_POST['AwardsHighAverageBowler'];
		
		foreach($post as $key => $value)
		{
			error_log("val: " . $post[$key]);
			error_log("type: " . gettype($post[$key]));

			//check if attribute is required and is null
			if(!property_exists($model->getValidators($key)[0],"requiredValue"))
			{
				if(!isset($post[$key]) || $post[$key] == "")
				{
					error_log("$key is null");
					$post[$key] = null;
				}
			}
			else
			{
				if($key == "date_of_performance" && !is_null($post[$key]) && !empty($post[$key]))
				{
					$date = explode('-',$value);
					error_log(print_r($date,true));
					$year = $date[2];
					$month = $date[0];
					$day = $date[1];

					$post[$key] = "$year-$month-$day";
				}
			}
		}

		return $post;
	}
}