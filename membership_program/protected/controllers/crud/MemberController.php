<?php

class MemberController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{ 
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','populateRoleTree','autocomplete','findamember','findMemberByTNBANumber','tabs','viewTNBANumber','uploadW9','findMemberByName','memberResults','generateTNBACard'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','downloadW9','displaySavedImage','saveEdits','createMember','deleteMember','runReport'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Member;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Member']))
		{
			error_log(print_r($_POST,true));

			$post = $_POST['Member'];
			$tnbaNumber = Helper::generateTNBANumber();
			$post['tnba_number'] = $tnbaNumber;
			$post = $this->foreignKeyNull($post);
   			$model->attributes = $post;
			
			//start transaction
			$connection=Yii::app()->db;
			if(is_null($connection->getCurrentTransaction()))
				$transaction = $connection->beginTransaction();

			if($model->save())
			{	
				$id = $model->getPrimaryKey();
				error_log("new id: $id");

				//create role entry for member
				if(array_key_exists("role_id", $post))
				{
					//assign member roles if any
					$roleMemberIDData = $post['role_id'];

					error_log("key exists");
					$length = count($roleMemberIDData);
					error_log("length: $length");

					for($i = 0; $i < $length; ++$i)
					{
						$roleMemberID = new RoleMemberId();
						$role_id = $roleMemberIDData[$i];
						error_log("role id: $role_id");
						$roleMemberID->role_id = $role_id;
						$roleMemberID->member_id = $id;

						if(!$roleMemberID->save())
						{
							//check if any errors were thrown by model validation rules
							if(!is_null($roleMemberID->getErrors()))
							{
								$error->buildMessageFromArray($roleMemberID->getErrors());
								$response->error = $error->toEncodableObject();
								echo json_encode($response->toEncodableObject());

								exit;
							}
						}
					}
				}
				else
					$post['role_id'] = null;

				$transaction->commit();

				if($this->createMembership($id))
					$this->redirect(array('viewTNBANumber','tnbaNumber'=>$tnbaNumber,'memberID'=>$id));	
				
			}

			$transaction->rollback();
		}

		$this->render('create',array(
			'model'=>$model,
			'create'=>'create'
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->actionAdmin();
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->actionAdmin();
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Member('search');
		
		$ZipcodeModel = new Zipcode();
		
		//get categories dropdown info
		$categories = Helper::retrieveAllCategories();
		$categoriesDropDownOptions = Helper::buildCategoriesDropdown($categories);
		
		//get sex dropdown info
		$sexes = Helper::retrieveAllSexes();
		$sexesDropDownOptions = Helper::buildSexesDropdown($sexes);

		//get senates dropdown info
		$senates = Helper::retrieveAllSenates();
		$senatesDropDownOptions = Helper::buildSenatesDropdown($senates);

		//get regions dropdown info
		$regions = Helper::retrieveAllRegions();
		$regionsDropDownOptions = Helper::buildRegionsDropdown($regions);

		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Member']))
			$model->attributes=$_GET['Member'];

		$roles = new RoleMemberId();

		$this->render('admin',array(
			'model'=>$model,
			'ZipcodeModel'=>$ZipcodeModel,
			'categoriesDropDownOptions'=>$categoriesDropDownOptions,
			'sexesDropDownOptions'=>$sexesDropDownOptions,
			'senatesDropDownOptions'=>$senatesDropDownOptions,
			'regionsDropDownOptions'=>$regionsDropDownOptions

		));
	}

	public function actionViewTNBANumber($tnbaNumber,$memberID)
	{
		$this->render('viewTNBANumber',array(
			'tnbaNumber'=>$tnbaNumber,
			'memberID'=>$memberID
		));
	}

	//Find A Member
	public function actionFindAMember()
	{
		$model=new Member('search');

		$this->render('find_a_member',array(
			'model'=>$model,
		));
	}

	public function actionTabs()
	{
		$model=new Member;

		$this->render('tabs',array(
			'model'=>$model,
		));
	}

	public function actionMemberResults()
	{
		$memberModel=new Member;

		$this->render('member_results',array(
			'memberModel'=>$memberModel,
		));
	}

	public function actionError()
	{
		error_log("action error");
		if($error=Yii::app()->errorHandler->error)
        	$this->render('error', $error);
	}

	public function actionFindMemberByTNBANumber()
	{
		header('Content-type: application/json');

		//echo "find member by usbc number";
		error_log(print_r($_POST,true));

		$tnbaNumber = $_POST['tnba_number'];

		//save usbc # in session
		Yii::app()->session['searched_tnba_number'] = $tnbaNumber;

		error_log('session searched_tnba_number: ' . Yii::app()->session['searched_tnba_number']);
		error_log("tnbaNumber: $tnbaNumber");

		$member = Member::model()->findBySql("select * from member where tnba_number = :tnbaNumber",array(':tnbaNumber' => $tnbaNumber));
		error_log(print_r($member,true));

		$object = new StdClass;
		$object->first_name = $member->first_name;
		$object->middle_name = $member->middle_name;
		$object->last_name = $member->last_name;
		$object->sex = $member->sex;
		$object->senate = $member->senate;
		$object->senate_region = $member->senate_region;

		echo json_encode($object);
	    //Yii::app()->end();
	}

	public function actionFindMemberByName()
	{
		header('Content-type: application/json');

		$response = new Response();
		$error = new Error();

		error_log(print_r($_POST,true));

		$first_name = $_POST['Member']['first_name'];
		$last_name = $_POST['Member']['last_name'];
		$zipcode = $_POST['Member']['zipcode'];
		$state = $_POST['Zipcode']['state_name'];
		$zipcodeCondition = null;
		$stateCondition = null;
		$sql = "SELECT * FROM member INNER JOIN zipcode ON member.zipcode = zipcode.zipcode WHERE 
				first_name LIKE :firstName AND last_name LIKE :lastName ";
		$params = array();
		$params[':firstName'] = "%$first_name%";
		$params[':lastName'] = "%$last_name%";

		if(empty($first_name))
			$error->addMessage("Must Enter First Name");

		if(empty($last_name))
			$error->addMessage("Must Enter Last Name");

		if(!empty($zipcode))
		{
			$zipcodeCondition = "AND";
			$sql .= "AND member.zipcode = :zip";
			$params[':zip'] = $zipcode;
		}

		if(!empty($state))
		{
			$stateCondition = "AND";
			$sql .= "AND state_name LIKE :st";
			$params[':st'] = "%$state%";
		}

		if($error->hasMessage())
		{
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		error_log("sql: $sql");
		error_log(print_r($params,true));

		try{
			$members = Member::model()->findAllBySql("$sql",
				$params
				);

			$length = count($members);

			$arr = array();

			for($i = 0; $i < $length; ++$i)
			{
				$member = $members[$i];

				$object = new StdClass;
				$object->first_name = $member->first_name;
				$object->middle_name = $member->middle_name;
				$object->last_name = $member->last_name;
				$object->sex = $member->sex;
				$object->senate = $member->senate;
				$object->senate_region = $member->senate_region;
				$object->tnba_number = $member->tnba_number;

				array_push($arr,$object);
			}
		}
		catch(CDBException $e)
		{
			$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
			error_log($e->getMessage());
			error_log($e->errorInfo[1]);
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			
			exit;
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());

			exit;
		}

		Yii::app()->session['member_results'] = $arr;

		$response->data = $arr;
		$response->message = "Find Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionSaveEdits()
	{
		header('Content-Type: application/json');

		$response = new Response();
		$error = new Error();

		error_log(print_r($_POST,true));
		$name = $_POST['name'];
		$id = $_POST['pk'];
		$value = $_POST['value'];
		
		if(is_array($value))
			$this->saveMultipleRoles($name,$value,$id);
		else
			$this->saveOtherAttributes($name,$value,$id);
	}

	private function saveMultipleRoles($name,$values,$id)
	{
		error_log("save multiple roles");
		$response = new Response();
		$error = new Error();

		try 
		{
			//start transaction
			$connection=Yii::app()->db;
			if(is_null($connection->getCurrentTransaction()))
				$transaction = $connection->beginTransaction();

			//delete old member roles
			try
			{
				//$id = $id['member_id'];
				error_log("id: $id");
				$command = Yii::app()->db->createCommand();
				$command->delete('role_member_id', 'member_id=:id', array(':id'=>$id));
				error_log("delete all roles for member_id: $id");
			}
			catch(CDBException $e)
			{
				$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
				error_log($e->getMessage());
				error_log($e->errorInfo[1]);
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());
				$transaction->rollback();
				error_log("rollback");
				exit;
			}

			//insert new member roles
			foreach($values as $key => $value) 
			{
				$roleMemberId = new RoleMemberId();

				$role_id = $value;
				$roleMemberId->role_id = $role_id;
				$roleMemberId->member_id = $id;

				if(!$roleMemberId->save())
				{
					//check if any errors were thrown by model validation rules
					if(!is_null($roleMemberId->getErrors()))
					{
						$error->buildMessageFromArray($roleMemberId->getErrors());
						$response->error = $error->toEncodableObject();
						echo json_encode($response->toEncodableObject());

						exit;
					}
				}
			}
		}
		catch(CDBException $e)
		{
			$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
			error_log($e->getMessage());
			error_log($e->errorInfo[1]);
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			$transaction->rollback();
			error_log("rollback");
			exit;
		}

		$transaction->commit();
		$response->message = "Edit Successful";
		echo json_encode($response->toEncodableObject());
	}

	private function saveOtherAttributes($name,$value,$id)
	{
		$response = new Response();
		$error = new Error();

		$member = Member::model()->findByPk($id);
		
		try{

			$member->$name = $value;
			$member->save();

			//check if any errors were thrown by model validation rules
			if(!is_null($member->getErrors()))
			{
				$error->buildMessageFromArray($member->getErrors());
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());

				exit;
			}
		}
		catch(CDBException $e)
		{
			$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
			error_log($e->getMessage());
			error_log($e->errorInfo[1]);
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Edit Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionCreateMember()
	{
		header('Content-Type: application/json');

		error_log(print_r($_POST,true));

		$data = $_POST['Member'];
		$response = new Response();
		$error = new Error();
		$memberPK = null;

		try{
			//$this->validateNewRows($data);
			
			if(empty($data['first_name']))
			{
				error_log("Must enter a First Name");
				$error->addMessage("Must enter a First Name");
			}
			if(empty($data['last_name']))
			{
				error_log("Must enter a Last Name");
				$error->addMessage("Must enter a Last Name");
			}
			if(empty($data['category_type']))
			{
				error_log("Must enter a Category Type");
				$error->addMessage("Must enter a Category Type");
			}
			if(empty($data['birthday']))
			{
				error_log("Must enter a Birthday");
				$error->addMessage("Must enter a Birthday");
			}
			if(empty($data['sex']))
			{
				error_log("Must enter a Sex");
				$error->addMessage("Must enter a Sex");
			}
			if(empty($data['street_address']))
			{
				error_log("Must enter a Street Address");
				$error->addMessage("Must enter a Street Address");
			}
			if(empty($data['zipcode']))
			{
				error_log("Must enter a Zipcode");
				$error->addMessage("Must enter a Zipcode");
			}
			if(empty($data['senate']))
			{
				error_log("Must enter a Senate");
				$error->addMessage("Must enter a Senate");
			}
			if(empty($data['senate_region']))
			{
				error_log("Must enter a Senate region");
				$error->addMessage("Must enter a Senate region");
			}

			if($error->hasMessage())
			{
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());
				exit;
			}


			try 
			{
				//start transaction
				$connection=Yii::app()->db;
				if(is_null($connection->getCurrentTransaction()))
					$transaction = $connection->beginTransaction();
				
				//so null will get sent to the DB if usbc # is empty
				if(empty($data['usbc_number']))
					$data['usbc_number'] = null;
				
				//Save W9
				$w9 = new W9();
				//$w9->attributes = $_POST['W9'];
				if($file=CUploadedFile::getInstance($w9,'file_content'))
				{

				}

				$newTNBANumber = Helper::generateTNBANumber();
				$data['tnba_number'] = $newTNBANumber;
				$member = new Member();

				$member->attributes = $data;
				error_log("new member");
				//error_log(print_r($member,true));

				if(!$member->save())
				{
					//check if any errors were thrown by model validation rules
					if(!is_null($member->getErrors()))
					{
						$error->buildMessageFromArray($member->getErrors());
						$response->error = $error->toEncodableObject();
						echo json_encode($response->toEncodableObject());

						exit;
					}
				}

				$memberPK = $member->getPrimaryKey();

				$roleMemberIDData = $_POST['RoleMemberId'];

				//create role entry for member
				if(array_key_exists("RoleMemberId", $_POST))
				{
					error_log("key exists");
					$length = count($roleMemberIDData['role_id']);
					error_log("length: $length");

					for($i = 0; $i < $length; ++$i)
					{
						$roleMemberID = new RoleMemberId();
						$role_id = $roleMemberIDData['role_id'][$i];
						error_log("role id: $role_id");
						$roleMemberID->role_id = $role_id;
						$roleMemberID->member_id = $memberPK;
						error_log("new primary key: " . $memberPK);

						if(!$roleMemberID->save())
						{
							//check if any errors were thrown by model validation rules
							if(!is_null($roleMemberID->getErrors()))
							{
								$error->buildMessageFromArray($roleMemberID->getErrors());
								$response->error = $error->toEncodableObject();
								echo json_encode($response->toEncodableObject());

								exit;
							}
						}
					}
				}

				$this->createMembership($memberPK);
			}
			catch(CDBException $e)
			{
				$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
				error_log($e->getMessage());
				error_log($e->errorInfo[1]);
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());
				$transaction->rollback();
				error_log("rollback");
				exit;
			}
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());

			exit;
		}

		$transaction->commit();
		$response->data = array('member_id'=>$memberPK);
		$response->message = "Creation of member {$member->first_name} {$member->middle_name} {$member->last_name} was successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionDeleteMember()
	{
		header('Content-Type: application/json');

		$data = json_decode($_POST['data'],true);

		error_log(print_r($data,true));

		try{
			if(empty($data))
				throw new Exception("No row was selected for deletion");

			$response = new Response();
			$error = new Error();

			foreach($data as $key => $value)
			{
				$connection=Yii::app()->db;
				if(is_null($connection->getCurrentTransaction()))
				{
					$transaction=$connection->beginTransaction();
				}

				$command = Yii::app()->db->createCommand();

				/*the grid sent the data as comma delimited so need to explode the data
				[0] => member_id
				[2] => league_cert_number
				[3] => season
				*/
				try{
					$member_id = $value;

					$command->delete('member', 'id=:member_id', array(':member_id'=>$member_id));
					error_log("delete: $member_id");
					$transaction->commit();
				}
				catch(Exception $e)
				{
					$error->addMessage($error->retrieveMessageForErrorCode($e->errorInfo[1]));
					error_log($e->getMessage());
					error_log($e->errorInfo[1]);
					$response->error = $error->toEncodableObject();
					echo json_encode($response->toEncodableObject());
					$transaction->rollback();
					error_log("rollback");
					exit;
				}
			}
		}
		catch(Exception $e)
		{
			$error->addMessage($e->getMessage());
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}

		$response->message = "Delete Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionDisplaySavedImage()
	{
	    $model=$this->loadModel($_GET['id']);
	 	error_log("display saved image");
	 	error_log("id: " . $model->id);

	 	$w_9 = W9::model()->findBySql("SELECT * FROM w_9 WHERE id = (SELECT w_9_id FROM member WHERE id = :m_id)",array('m_id'=>$model->id));

	    header('Pragma: public');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Content-Transfer-Encoding: binary');
	    header('Content-length: '. $w_9->file_size);
	    header('Content-Type: '. $w_9->file_type);
	    header('Content-Disposition: attachment; filename=' . $w_9->file_name);

	 	
	 	
	    echo $w_9->file_content;
	}

	public function actionPopulateRoleTree($action,$id)
	{
		error_log(print_r($_POST,true));
		error_log("action: $action");
		error_log("id: $id");

		//run function only if it's called via ajax
		if (!Yii::app()->request->isAjaxRequest) {
			exit();
		}

		$parentId = "NULL";
		if (isset($_GET['root']) && $_GET['root'] !== 'source') {
			$parentId = (int) $_GET['root'];
		}

		$sql = "SELECT node.id,node.name
		FROM role AS node,
		        role AS parent
		WHERE node.lft BETWEEN parent.lft AND parent.rgt
		        AND parent.name = 'Roles'
		ORDER BY node.lft";

		$req = Yii::app()->db->createCommand($sql);

		$children = Yii::app()->db->createCommand(
            "SELECT m1.id, m1.name AS text, m2.id IS NOT NULL AS hasChildren "
            . "FROM role AS m1 LEFT JOIN role AS m2 ON m1.id=m2.parent_id "
            . "WHERE m1.parent_id <=> $parentId "
            . "GROUP BY m1.id ORDER BY m1.name ASC"
        )->queryAll();

		//get member role_id
		$role_id = null;
		switch($action)
		{
			case 'update':
				$member = Member::model()->findBySql("select role_id from member where id = :id",array(':id' => $id));
				if(!is_null($member))
					$role_id = $member->role_id;
				break;
			case 'create':
				$role = Role::model()->findBySql("select id from role where id = :id",array(':id' => (empty($id) ? 1 : $id)));
				if(!is_null($role))
					$role_id = $role->id;
				break;

		}    	
		error_log("role_id: $role_id");
        for($i = 0; $i < count($children); ++$i)
        {
        	if($children[$i]['hasChildren'] == 0)
        	{
        		$id = $children[$i]['id'];
        		error_log("current role_id");
        		$text = $children[$i]['text'];
        		$children[$i]['text'] = CHtml::radioButton("roles",($id == $role_id) ? true : false,array('value' => $id)) . " $text";
        	}

        	error_log($children[$i]['text']);
        }

        echo str_replace(
	        '"hasChildren":"0"',
	        '"hasChildren":false',
	        CTreeView::saveDataAsJson($children)
        );

		exit();
	}

	public function actionAutoComplete($term) {

		error_log("autoComplete");
		$list = Helper::autoComplete($term);
		echo json_encode($list);
	}

	public function actionDownloadW9($id) {

		error_log("id: $id");
		$w_9 = W9::model()->findBySql("SELECT * FROM w_9 WHERE id = (SELECT w_9_id FROM member WHERE id = :m_id)",array(':m_id'=>$id));

	    header('Pragma: public');
	    header('Expires: 0');
	    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
	    header('Content-Transfer-Encoding: binary');
	    header('Content-length: '. $w_9->file_size);
	    header('Content-Type: '. $w_9->file_type);
	    header('Content-Disposition: attachment; filename=' . $w_9->file_name);
	    header('Accept-Ranges: bytes');

	 	
	 	
	    echo $w_9->file_content;
	}

	public function actionUploadW9($id) {

		error_log("upload");
		error_log("id: $id");

		error_log(print_r($_FILES,true));
		error_log(print_r($_POST,true));
		$response = new Response();
		$error = new Error();

		$name = $_FILES[0]['name'];
		$content = file_get_contents($_FILES[0]['tmp_name']);
		$type = $_FILES[0]['type'];
		$size = $_FILES[0]['size'];

		//$type = substr($type, strpos($type, "/") + 1);
		error_log("type: $type");

		$w_9 = new W9();
		$w_9->file_name = $name;
		$w_9->file_type = $type;
		$w_9->file_size = $size;
		$w_9->file_content = $content;

		if(!$w_9->save())
		{
			//check if any errors were thrown by model validation rules
			if(!is_null($w_9->getErrors()))
			{
				$error->buildMessageFromArray($w_9->getErrors());
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());

				exit;
			}
		}

		$w_9_id = $w_9->getPrimaryKey();

		error_log("w_9_id: $w_9_id");

		$member = Member::model()->findByPk($id);
		$member->w_9_id = $w_9_id;

		if(!$member->save())
		{
			//check if any errors were thrown by model validation rules
			if(!is_null($member->getErrors()))
			{
				$error->buildMessageFromArray($member->getErrors());
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());

				exit;
			}
		}

		$response->message = "Upload Successful";
		echo json_encode($response->toEncodableObject());
	}

	public function actionRunReport()
	{
		error_log(print_r($_GET,true));
		define('GRID','MEMBER');

		$data = json_decode($_GET['data_field']);
		$reportType = $_GET['report_type_field'];

		Helper::runReport(GRID,$reportType,$data);
	}

	public function actionGenerateTNBACard()
	{
		require_once(Yii::app()->basePath.'/../php/tcpdf/TNBAPDF.php');

		define('TNBA_CARD','GENERATE TNBA CARD');
		$memberID = $_GET['memberID'];

		error_log("memberID: $memberID");

		//create tnba card
		$pdf = new TNBAPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
		return $pdf->startGeneration(TNBA_CARD,$memberID);
	}

	private function validateNewRows($data)
	{
		$error = new Error();
		$response = new Response();

		$num = 1;

		foreach($data as $key => $value)
		{
			$row = $data[$key];
			if(empty($row['first_name']))
			{
				error_log("Must enter a First Name for row #$num");
				$error->addMessage("Must enter a First Name for row #$num");
			}
			if(empty($row['last_name']))
			{
				error_log("Must enter a Last Name for row #$num");
				$error->addMessage("Must enter a Last Name for row #$num");
			}
			if(empty($row['category_type']))
			{
				error_log("Must enter a Category Type for row #$num");
				$error->addMessage("Must enter a Category Type for row #$num");
			}
			if(empty($row['birthday']))
			{
				error_log("Must enter a Birthday for row #$num");
				$error->addMessage("Must enter a Birthday for row #$num");
			}
			if(empty($row['sex']))
			{
				error_log("Must enter a Sex for row #$num");
				$error->addMessage("Must enter a Sex for row #$num");
			}
			if(empty($row['street_address']))
			{
				error_log("Must enter a Street Address for row #$num");
				$error->addMessage("Must enter a Street Address for row #$num");
			}
			if(empty($row['zipcode']))
			{
				error_log("Must enter a Zipcode for row #$num");
				$error->addMessage("Must enter a Zipcode for row #$num");
			}
			if(empty($row['phone_number']))
			{
				error_log("Must enter a phone_number for row #$num");
				$error->addMessage("Must enter a phone_number for row #$num");
			}
			if(empty($row['senate']))
			{
				error_log("Must enter a Senate for row #$num");
				$error->addMessage("Must enter a Senate for row #$num");
			}
			if(empty($row['senate_region']))
			{
				error_log("Must enter a Senate region for row #$num");
				$error->addMessage("Must enter a Senate region for row #$num");
			}

			++$num;
		}

		if($error->hasMessage())
		{
			$response->error = $error->toEncodableObject();
			echo json_encode($response->toEncodableObject());
			exit;
		}
	}

	//create membership for member
	private function createMembership($memberId) {

		$membershipModel = new Membership;
		$membershipModel->member_id = $memberId;
		$membershipModel->status = 'Active';
		$membershipModel->type = 'New';
		$season = Season::model()->findBySql("SELECT name FROM season ORDER BY name DESC");
		$membershipModel->season = $season->name;
		
		if(!$membershipModel->save())
		{
			//check if any errors were thrown by model validation rules
			if(!is_null($membershipModel->getErrors()))
			{
				$error = new Error();
				$response = new Response();

				$error->buildMessageFromArray($membershipModel->getErrors());
				$response->error = $error->toEncodableObject();
				echo json_encode($response->toEncodableObject());

				exit;
			}
		}

		return true;
	}

	//sets fields to NULL for the non-required fields that are left blank by the user
	private function foreignKeyNull($post) {

		$model = new Member;
		foreach($post as $key => $value)
		{
			//error_log("val: " . $post[$key]);
			error_log("type: " . gettype($post[$key]));

			if(gettype($post[$key]) != "array")
			{
				//if attribute is not required
				if(!property_exists($model->getValidators($key)[0],"requiredValue"))
				{
					if(!isset($post[$key]) || $post[$key] == "")
					{
						error_log("$key is null");
						$post[$key] = null;
					}
				}
			}
		}

		return $post;
	}

	private function getForeignKeyId() {

		$post = $_POST['Member'];


	}

	private function generateTNBANumber() {

		$digits = 5;
		$tnbaNumber = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);
		error_log("generated tnba_number: $tnbaNumber");
		if(!$this->TNBANumberExists($tnbaNumber))
		{
			error_log("number doesn't exist. Assigning it");
			return $tnbaNumber;
		}
		else
		{	
			error_log("number already exists.");
			return $this->generateTNBANumber();		
		}

	}

	private function TNBANumberExists($tnbaNumber) {

		$exists = Member::model()->findBySql("select id from member where tnba_number = :tnbaNumber",array(':tnbaNumber' => $tnbaNumber));

		if(is_null($exists))
			return false;

		return true;
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Member the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Member::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Member $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='member-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
