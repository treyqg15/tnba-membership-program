<?php

class BowlerController extends Controller
{
	public function actionUpdateBowler()
	{
		$model = new Bowler();
		$awardsStatusModel = new AwardsStatus();
		
		$this->render('updateBowler',array(
			'model'=>$model,
			'awardsStatusModel'=>$awardsStatusModel
		));
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}