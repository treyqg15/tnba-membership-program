1. need to add unique field to confirmation_number in members table after insert to get rid of 0 values in the table

2. need to create member_type(current category table) table

3. need to create membership_type table

4. need to create region(senate) table

5. need to create membership table

6. need to create membership_status table

insert into member (`first_name`,`middle_name`,`last_name`,`email`,`category_id`,`role_id`,`usbc_number`,`tnba_number`,`e_signature`,`birthday`,`sex`,`street_address`,`zipcode`,`phone_number`,`senate`,`senate_region`) (select first_name,middle_name,last_name,email_address,(select id from category where category.type = members.member_type),null,null,member_no,e_signature,birth_datestamp,gender,address,zip_code,phone_number,local_senate,senate_region from members)

select member_id,`street_address`,city,state,zip_code from members where CHAR_LENGTH (zip_code) != 5 AND  zip_code != '' AND CHAR_LENGTH (zip_code) != 10

select member_id,`address`,city,state,zip_code,SUBSTRING(zip_code,1,5) from members where CHAR_LENGTH(zip_code) > 5

update members set zip_code = SUBSTRING(zip_code,1,5) where CHAR_LENGTH(zip_code) > 5

select `member_id`,`city`,`state`zip_code from members where zip_code NOT IN (select zipcode from zipcode)

SELECT m1.id, m1.name AS text, m2.id IS NOT NULL AS hasChildren, m.first_name,m.last_name,m.tnba_number,m.role_id
FROM role AS m1 LEFT JOIN role AS m2 ON m1.id=m2.parent_id
INNER JOIN member as m ON m.role_id = m2.id and m.id = 58660
WHERE m1.parent_id <=> 1
GROUP BY m1.id ORDER BY m1.name ASC