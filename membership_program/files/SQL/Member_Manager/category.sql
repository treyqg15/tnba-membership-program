create table category (
	id tinyint PRIMARY KEY NOT NULL AUTO_INCREMENT,
        creationTime timestamp default current_timestamp,
        type varchar(12) NOT NULL UNIQUE
)

