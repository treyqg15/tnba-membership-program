create table role ( 
	id tinyint PRIMARY KEY NOT NULL AUTO_INCREMENT,
	creationTime timestamp default current_timestamp NOT NULL,
	name varchar(50) NOT NULL,
	lft tinyint NOT NULL UNIQUE,
	rgt tinyint NOT NULL UNIQUE
)

