create table membership (
	member_id int not null primary key,
	dues_paid_league varchar(30) default null,
	status varchar(7) default 'Pending',
	creation_datetime timestamp default current_timestamp,
	paid_datetime timestamp default 0,
	type varchar(7) default 'New',
	foreign key (member_id) references member(id),
	foreign key (status) references membership_status(status),
	foreign key (type) references membership_type(type)
)

