create table zipcode (
	zipcode varchar(10) primary key not null,
	city_name varchar(50) not null,
	state_name varchar(50) not null,
	foreign key (city_name,state_name)  references city(name, state_name) on delete cascade
)

