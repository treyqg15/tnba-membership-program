create table league_officer (
	cert_number varchar(10) not null,
	officer_id int not null,
	primary key (cert_number,officer_id),
	foreign key (cert_number) references league(cert_number) on delete cascade,
	foreign key (officer_id) references officer(id) on delete cascade
)

