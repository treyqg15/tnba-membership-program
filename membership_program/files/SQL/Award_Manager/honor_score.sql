create table honor_score (
	name varchar(30) primary key not null,
	ts timestamp not null default current_timestamp,
	jewelry_id tinyint default null,	
	sex varchar(6),
	foreign key (jewelry_id) references jewelry_type(id) on delete set null,
	foreign key (sex) references sex(type) on delete set null
)

