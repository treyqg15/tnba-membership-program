create table league (
	cert_number varchar(10) primary key not null,
	name varchar(30) not null,
	bowling_alley_name varchar(30) not null,
	season_name varchar(9) default null,
	foreign key (season_name) references season(name) on delete set null
)

