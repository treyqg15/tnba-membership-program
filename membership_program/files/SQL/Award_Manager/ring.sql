create table ring (
	type varchar(30) not null,
	size float not null,
	primary key (type,size),
	foreign key (size) references ring_size(size) on delete cascade
)

