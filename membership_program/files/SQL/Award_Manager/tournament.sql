create table tournament (
	cert_number varchar(10) primary key not null,
	name varchar(50) not null,
	zipcode varchar(10) not null,
	foreign key (zipcode) references zipcode(zipcode) on delete cascade
)

