create table officer (
	id int primary key not null,
	name varchar(50) not null,
	position varchar(40) not null unique,
	foreign key (position) references officer_position (position) on delete cascade
)

