create table jewelry_type (
	id tinyint not null primary key AUTO_INCREMENT,
	pin_type varchar(30) default null,
	ring_type varchar(30) default null,
	ring_size float default null,
	foreign key (pin_type) references pin(type) on delete cascade,
	foreign key (ring_type,ring_size) references ring(type,size) on delete cascade
)

