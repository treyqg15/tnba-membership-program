$(document).ready(function() {
	
	var form = "create_statsHistory_form";
	var url = "statsHistory/createBowlerHistory";
	var gridID = "statsHistory-grid";

	initGridRowSelection(gridID);

	initFormSubmitForPage(form,url,gridID);

	form = "statsHistory_reports_form";
	initReportsFormSubmitForPage(form);	
	
	$("#zipcode").change(function() {

		var zipcode = $(this).val();
		var url = 'statsHistory/fetchBowlingAlleysForZipcode/zipcode/'+zipcode;
		$.ajax({
			'url': url,
			'type': 'get',
			'success': function(response) { 

				var arr = $.parseJSON(response);
				var html = '<select>';

				for (var i = 0; i < arr.length; ++i)
				{
					var bowlingAlley = arr[i];
					html += '<option value="'+bowlingAlley.value+'">'+bowlingAlley.text+'</option>';
				}

				html += '</select>';

				$('#BowlingAlley_id').html(html);

				//Get first returned bowling alley
				fetchLeaguesForBowlingAlley(arr[0].value);
			}
		});
	});

	$("#BowlingAlley_id").change(function() {

		var id = $(this).val();
		fetchLeaguesForBowlingAlley(id);
	});
});

function fetchLeaguesForBowlingAlley(id)
{
	var url = 'statsHistory/fetchLeagueForBowlingAlley/id/'+id;

	$.ajax({
		'url': url,
		'type': 'get',
		'data': {
			'bowling_alley_id' : id
		},
		'success': function(response) { 

			var arr = $.parseJSON(response);
			var html = '<select>';

			for (var i = 0; i < arr.length; ++i)
			{
				var bowlingAlley = arr[i];
				html += '<option value="'+bowlingAlley.value+'">'+bowlingAlley.text+'</option>';
			}

			html += '</select>';

			$('#StatsHistory_league_cert_number').html(html);
		}
	});
}

