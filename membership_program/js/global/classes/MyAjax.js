var MyAjax = function MyAjax(builder)
{
	var url = builder.getUrl();
	var type = builder.getType();
	var success = builder.getSuccessFunc();
	var async = builder.getAsync();
	var gridID = builder.getGridID();
	var error = builder.getErrorFunc();
	var data = builder.getData();
	var dataType = builder.getDataType();

	return {

		makeCall : function()
		{
			$.ajax({
	           	type: "POST",
				url: url,
				data: data, 
				dataType: dataType,
				async : async,
				success: function(response)
				{
					success(response);
				}
			});
		}
	}
};