var AjaxBuilder = function(url,type,successFunc) {
	
	//required params
	var url = url;
	var type = type;
	var success = successFunc;

	//optional params
	var async = true;
	var gridID = null;
	var error = null;
	var data = null;
	var dataType = 'json';

	return {

		setAsync : function (asy)
		{
			async = asy;

			return this;
		},
		getAsync : function ()
		{
			return async;
		},
		setGridID : function (gID)
		{
			gridID = gID;

			return this;
		},
		getGridID : function ()
		{
			return gridID;
		},
		setErrorFunc : function (err)
		{
			errorFunc = err;

			return this;
		},
		setData : function (dat)
		{
			data = dat;

			return this;
		},
		getData : function ()
		{
			return data;
		},
		setDataType : function (dt)
		{
			dataType = dt;

			return this;
		},
		getDataType : function ()
		{
			return dataType;
		},
		getErrorFunc : function ()
		{
			return error;
		},
		getUrl : function ()
		{
			return url;
		},
		getType : function ()
		{
			return type;
		},
		getSuccessFunc : function ()
		{
			return success;
		}
	};
};