var Response = function Response()
{
	function onlyErrorOutput(response)
	{
		if(response.error != null)
		{
			var errorMessages = response.error.messages;
			var output = '';
			for(var i = 0; i < errorMessages.length; ++i)
				output += errorMessages[i] + "\n";
			alert(output);
		}
	}

	function fullOutput(response)
	{
		if(response.error == null)
			alert(response.message);
		else
		{
			var errorMessages = response.error.messages;
			var output = '';
			for(var i = 0; i < errorMessages.length; ++i)
				output += errorMessages[i] + "\n";
			alert(output);

		}
	}

	function updateGridAndShowResponse(response) 
	{ 
		if(response.error == null)
		{
			$.fn.yiiGridView.update(gridId); 
			alert(response.message);
		}
		else
		{
			var errorMessages = response.error.messages;
			var output = '';
			for(var i = 0; i < errorMessages.length; ++i)
				output += errorMessages[i] + "\n";
			alert(output);

		}
	}

	return {

		getOnlyErrorOutputFunc : function(response)
		{
			return onlyErrorOutput(response);
		},
		getFullOutputFunc : function(response)
		{
			return fullOutput(response);
		}
	};
}