var newRowsCount = 1;
var newRows = {};
var NAME = "newRow_";
var selectedRows = {};

$(document).ready(function() {

	
});

function initGridRowSelection(gridID)
{
	$('#' + gridID + ' table tbody tr').live('click',function(e)
	{
		var keys = $('#' + gridID + ' > div.keys > span');
		var pk=keys.eq(this.rowIndex-2).text();
		
		if(typeof selectedRows[pk] != "undefined")
		{
			delete selectedRows[pk];
		}
		else
		{
		   	selectedRows[pk] = pk;
		   	
		   	var keys = $(gridID + ' > div.keys > span');
	   	}
	});
}

function initReportsFormSubmitForPage(form)
{
	$('#' + form).submit(function() {

		var data_field = $('#data_field');
		data_field.val(JSON.stringify(selectedRows));
	});
}

function initFormSubmitForPage(form,url,gridID)
{
	$('#' + form).submit(function() {



	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $(this).serialize(), // serializes the form's elements.
	           dataType: 'json',
	           success: function(response)
	           {
					if(response.error == null)
					{
						if(gridID == "member-grid")
						{
							var id = response.data.member_id;
							var files = $('#Member_w_9_id')[0].files;

							if(files.length > 0)
								uploadW9('member-grid',files,id);
							else
							{
								$.fn.yiiGridView.update(gridID); 
								alert(response.message);
							}
						}
						else
						{
							$.fn.yiiGridView.update(gridID); 
							alert(response.message);
						}
					}
					else
					{
						var errorMessages = response.error.messages;
						var output = '';
						for(var i = 0; i < errorMessages.length; ++i)
							output += errorMessages[i] + "\n";
						alert(output);
					}
	           }
	    });

	    return false; // avoid to execute the actual submit of the form.
	});
}

function uploadW9(gridID,files,id)
{
		var data = new FormData();
		
		$.each(files, function(key, value)
		{
			data.append(key, value);
		});

		$.ajax({
	        url: 'uploadW9/id/'+id,
	        type: 'POST',
	        data: data,
	        cache: false,
	        dataType: 'json',
	        processData: false, // Don't process the files
	        contentType: false, // Set content type to false as jQuery will tell the server its a query string request
	        success: function(response, textStatus, jqXHR)
	        {
	        	if(response.error == null)
				{
					$.fn.yiiGridView.update(gridID); 
					alert(response.message);
				}
				else
				{
					var errorMessages = response.error.messages;
					var output = '';
					for(var i = 0; i < errorMessages.length; ++i)
						output += errorMessages[i] + "\n";
					alert(output);

				}
	        },
	        error: function(jqXHR, textStatus, errorThrown)
	        {
	        	// Handle errors here
	        	console.log('ERRORS: ' + textStatus);
	        	// STOP LOADING SPINNER
	        }
	    });
}

function manageRow(gridId,button,url)
{
	var html = "";

	var name = NAME + newRowsCount;
	var table = $('#'+gridId+' table tbody');

	//If it is odd then the next one should be even.. 
	var number = ((table.find('tr').size())%2 === 0)?'odd':'even';


	switch(button)
	{
		case 'Add Row':
			//get certain variables
			switch(gridId)
			{
				case 'membership-grid':
					html = '<tr name="'+name+'" class="'+number+'"> <td></td><td></td><td></td><td><input name="tnba_number" type="number" placeholder="TNBA #" /></td><td></td><td><select name="status"> '+statusesDropDownOptions+' </select></td><td></td><td></td><td><select name="type"> '+typesDropDownOptions+' </select></td> </tr>';
					addNewRow(gridId,html);
					break;
				case 'member-grid':
					//html = '<tr name="'+name+'" class="'+number+'"> <td><input name="first_name" type="text" value="Member" placeholder="First Name" /></td><td><input name="middle_name" type="text" value="Middle Name" placeholder="Middle Name" /></td><td><input name="last_name" type="text" value="Member" placeholder="Last Name" /></td><td><input name="email" type="text" placeholder="Email" /></td><td><select name="category_type"> '+categoriesDropDownOptions+' </select></td><td><input name="usbc_number" type="text" placeholder="USBC #" /></td><td></td><td><input id="birthday_'+name+'" name="birthday" type="date" /></td><td><select name="sex"> '+sexesDropDownOptions+' </select></td><td><input name="street_address" type="text" value="580 S Pear Orchard Rd" /><td><input name="zipcode" type="number" value="39157" /><td><input name="phone_number" type="number" value="6018801788" /></td><td><select name="senate"> '+senatesDropDownOptions+' </select></td><td><select name="senate_region"> '+regionsDropDownOptions+' </select></td> </tr>';
					html = '<tr name="'+name+'" class="'+number+'"> <td><input name="first_name" type="text" placeholder="First Name" /></td><td><input name="middle_name" type="text" placeholder="Middle Name" /></td><td><input name="last_name" type="text" placeholder="Last Name" /></td><td><input name="email" type="text" placeholder="Email" /></td><td><select name="category_type"> '+categoriesDropDownOptions+' </select></td><td><input name="usbc_number" type="text" placeholder="USBC #" /></td><td></td><td><input id="birthday_'+name+'" name="birthday" type="date" /></td><td><select name="sex"> '+sexesDropDownOptions+' </select></td><td><input name="street_address" type="text" /><td><input name="zipcode" type="number" /><td><input name="phone_number" type="number" /></td><td><select name="senate"> '+senatesDropDownOptions+' </select></td><td><select name="senate_region"> '+regionsDropDownOptions+' </select></td> </tr>';
					addNewRow(gridId,html);
					break;
				case 'awards-grid':

					var defaultZipcode = 39157;
					var dropdowns = fetchBowlingAlleysAndLeaguesForZipcode(url,defaultZipcode);

					var bowlingAlleysDropdownOptions = dropdowns.bowlingAlleysDropdown;
					var leaguesDropdownOptions = dropdowns.leaguesDropdown;

					html = '<tr name="'+name+'" class="'+number+'"> <td></td><td></td><td></td><td></td><td></td><td></td><td><input name="tnba_number" type="number" placeholder="TNBA #" /></td><td><select id="league_name_dropdown_'+newRowsCount+'" name="league_cert_number"> '+leaguesDropdownOptions+' </select></td><td><select id="bowling_alley_dropdown_'+newRowsCount+'"" name="bowling_alley_name" data-rowNum="'+newRowsCount+'"> '+bowlingAlleysDropdownOptions+' </select></td><td><input id="zipcode_field_'+newRowsCount+'" type="number" placeholder="Zipcode" value="'+defaultZipcode+'" /></td><td><select name="status"> '+awardStatusesDropDownOptions+' </select></td><td></td><td><input id="date_of_performance_'+name+'" name="date_of_performance" type="date" placeholder="Date of Performance" /></td><td><input name="current_average" type=number placeholder="Current Average" /></td><td><select name="name_of_senate">'+senatesDropdownOptions+'</select></td><td><select name="honor_score"> '+honorScoresDropDownOptions+' </select></td> </tr>';
					addNewRow(gridId,html);
					attachEventHandlers(url,newRowsCount);

					break;
				case 'statsHistory-grid':

					var defaultZipcode = 39157;
					var dropdowns = fetchBowlingAlleysAndLeaguesForZipcode(url,defaultZipcode);

					var bowlingAlleysDropdownOptions = dropdowns.bowlingAlleysDropdown;
					var leaguesDropdownOptions = dropdowns.leaguesDropdown;

					html = '<tr name="'+name+'" class="'+number+'"> <td></td><td></td><td></td><td><input name="tnba_number" type="number" placeholder="TNBA #" /></td><td><input name="usbc_number" type="text" placeholder="USBC #" /></td><td><select id="league_name_dropdown_'+newRowsCount+'" name="league_cert_number"> '+leaguesDropdownOptions+' </select></td><td><select id="bowling_alley_dropdown_'+newRowsCount+'"" name="bowling_alley_name" data-rowNum="'+newRowsCount+'"> '+bowlingAlleysDropdownOptions+' </select></td><td><input id="zipcode_field_'+newRowsCount+'" type="number" placeholder="Zipcode" value="'+defaultZipcode+'" /></td><td><input name="average" type="number" placeholder="Average" /></td><td><select name="season"> '+seasonsDropDownOptions+' </select></td> </tr>';
					addNewRow(gridId,html);
					attachEventHandlers(url,newRowsCount);
					break;
				case 'league-grid':
					
					break;
			}
			
			++newRowsCount;

			break;
		case 'Save':
			saveNewRow(gridId,url);
			break;
		case 'Delete':
			deleteRow(gridId,url);
			break;

	}
}

function addNewRow(gridId,rowHtml) 
{
	var name = NAME + newRowsCount;

	var table = $('#'+gridId+' table tbody');
	//If it is odd then the next one should be even.. 
	var number = ((table.find('tr').size())%2 === 0)?'odd':'even';
	
	table.append(rowHtml);

	newRows[name] = null;

}

function saveNewRow(gridId,url)
{
	for (key in newRows)
	{
		//attach focusOut event to newRow child elements
		$('tr[name='+key+']').each(function() {
			var i = 0;
			var row = {};

			//get tds
			$(this).children().each(function() {
				var i = 0;
				//get values
				$(this).children().each(function() {
					var elementVal = $(this).val();
					var elementName = $(this).attr('name');
					if(typeof elementName != "undefined")
					{
						if(elementName == "birthday" && elementVal != "")
							elementVal = formatBirthday(elementVal);
						row[elementName] = elementVal;
					}
				});
			});

			newRows[key] = row;

		});
	}

	if(!checkForDuplicateRows(gridId))
		return false;

	//create new rows in DB
	$.ajax({
		'url': url,
		'type': 'post',
		'data': {
			'data': JSON.stringify(newRows)
		},
		'success': function(response) { 

			if(response.error == null)
			{
				$.fn.yiiGridView.update(gridId); 
				alert(response.message);
			}
			else
			{
				var errorMessages = response.error.messages;
				var output = '';
				for(var i = 0; i < errorMessages.length; ++i)
					output += errorMessages[i] + "\n";
				alert(output);

			}
		}
	});
}

function deleteRow(gridId,url)
{
	var selectedRows = $('#'+gridId).yiiGridView('getSelection');

	//if any rows are selected....delete
	if(selectedRows.length > 0)
	{
		$.ajax({
		'url': url,
		'type': 'post',
		'data': {
			'data': JSON.stringify(selectedRows)
		},
		'success': function(response) { 

				if(response.error == null)
				{
					$.fn.yiiGridView.update(gridId); 
					alert(response.message);
					resetVars();
				}
				else
				{
					var errorMessages = response.error.messages;
					var output = '';
					for(var i = 0; i < errorMessages.length; ++i)
						output += errorMessages[i] + "\n";
					alert(output);

				}
			}
		});
	}
	else
		alert('No row was selected for deletion');
}

function fetchBowlingAlleysAndLeaguesForZipcode(url,zipcode)
{
	var response = $.ajax({
		'url': url + '/retrieveBowlingAlleysAndLeaguesForZipcode',
		'type': 'get',
		'data': {
			'zipcode' : zipcode
		},
		'async' : false
	}).responseText;

	var response = $.parseJSON(response);

	if(response.error == null)
	{
		return response.data;
		//$.fn.yiiGridView.update(gridId); 
		//alert(response.message);

	}
	else
	{
		var errorMessages = response.error.messages;
		var output = '';
		for(var i = 0; i < errorMessages.length; ++i)
			output += errorMessages[i] + "\n";
		alert(output);
	}

	return null;
}

function fetchLeagueForBowlingAlley(url,rowNum)
{
	var bowling_alley_id = $('#bowling_alley_dropdown_'+rowNum).val();
	var response = $.ajax({
		'url': url + '/retrieveLeagueForBowlingAlley',
		'type': 'get',
		'data': {
			'bowling_alley_id' : bowling_alley_id
		},
		'success': function(response) { 

			if(response.error == null)
			{
				var html = response.data;
				$('#league_name_dropdown_'+rowNum).html(html);
			}
			else
			{
				var errorMessages = response.error.messages;
				var output = '';
				for(var i = 0; i < errorMessages.length; ++i)
					output += errorMessages[i] + "\n";
				alert(output);

			}
		}
	});
}

function attachEventHandlers(url,rowNum)
{
	//attach onchange for bowling_alley_dropdown
	$('#bowling_alley_dropdown_'+rowNum).change(function() {

		fetchLeagueForBowlingAlley(url,rowNum);
		
	});

	$('#zipcode_field_'+rowNum).blur(function(event) {

		var zipcode = $(this).val();
		var dropdowns = fetchBowlingAlleysAndLeaguesForZipcode(url,zipcode);

		//no bowling alleys for zipcode
		if(dropdowns != null)
		{
			var bowlingAlleysDropdownOptions = dropdowns.bowlingAlleysDropdown;
			var leaguesDropdownOptions = dropdowns.leaguesDropdown;

			$('#bowling_alley_dropdown_'+rowNum).html(bowlingAlleysDropdownOptions);
			$('#league_name_dropdown_'+rowNum).html(leaguesDropdownOptions);
		}
		else
		{
			$('#bowling_alley_dropdown_'+rowNum).html('');
			$('#league_name_dropdown_'+rowNum).html('');
		}

	});

/*
	$('#zipcode_field_'+rowNum).editable({
	    // url: '/Update/Client',
	    name: 'zipcode',
	    typeahead: {
	        name: 'zipcode',
	         remote: {
	             url: url + '/autoComplete'
	         }

	    }
	});
*/
}

function triggerReportTypeForm(form,func)
{
	$('#report_type_field').val(func);

	//force form reports submit
	$(form).trigger("submit");
}

function resetVars()
{
	newRowsCount = 1;
	newRows = {};
	selectedRows = {};	
}

function checkForDuplicateRows(gridId)
{
	switch(gridId)
	{
		case 'statsHistory-grid':
			
			var count = 1;
			var start = 1;
			
			for(var currentRow in newRows)
			{
				var obj1 = newRows[currentRow];
				var i = 1;

				for(var nextRow in newRows)
				{	
					if(i != start)
					{
						var obj2 = newRows['newRow_'+i];

						if(obj1.tnba_number == obj2.tnba_number 
							&& obj1.season == obj2.season 
							&& obj1.league_cert_number == obj2.league_cert_number)
						{
							alert("Cannot add rows with the same TNBA #, Season, and League");
							return false;
						}
					}
					++i;
				}
				++start;
				++count;
				
				if(newRowsCount - 1 == count)
					break;
			}
			break;
	}

	return true;
}
