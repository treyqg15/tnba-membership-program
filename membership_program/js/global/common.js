function readResponse(response)
{
	if(response.error != null)
	{
		var errorMessages = response.error.messages;
		var output = '';
		for(var i = 0; i < errorMessages.length; ++i)
			output += errorMessages[i] + "\n";
		alert(output);
	}
}

function formatBirthday(birthday)
{
	var indexes = birthday.split("-");

	return indexes[0] + '-' + indexes[1] + '-' + indexes[2];
}