$(document).ready(function() {

	var form = "create_member_form";
	var url = "addMemberToLeague";
	var createLeagueUrl = "createLeague";
	var gridID = "view-member-grid";
	var leagueForm = "create_league_form";
	var leagueGridID = "league-grid";

	initGridRowSelection(leagueGridID);

	initFormSubmitForPage(form,url,gridID);
	initFormSubmitForPage(leagueForm,createLeagueUrl,leagueGridID);

	form = "league_reports_form";
	initReportsFormSubmitForPage(form);

	/*
	$("#create_league_form").submit(function() {

		var url = "createLeague"; // the script where you handle the form input.

	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $(this).serialize(), // serializes the form's elements.
	           dataType: 'json',
	           success: function(response)
	           {
					if(response.error == null)
					{
						$.fn.yiiGridView.update('league-grid'); 
						alert(response.message);
					}
					else
					{
						var errorMessages = response.error.messages;
						var output = '';
						for(var i = 0; i < errorMessages.length; ++i)
							output += errorMessages[i] + "\n";
						alert(output);
					}
	           }
	    });

	    return false; // avoid to execute the actual submit of the form.

	});
	*/

	$("#BowlingAlley_zipcode").change(function() {

		var zipcode = $(this).val();
		var url = 'fetchBowlingAlleysForZipcode/zipcode/'+zipcode;
		$.ajax({
			'url': url,
			'type': 'get',
			'success': function(response) { 

				var arr = $.parseJSON(response);
				var html = '<select>';

				for (var i = 0; i < arr.length; ++i)
				{
					var bowlingAlley = arr[i];
					html += '<option value="'+bowlingAlley.value+'">'+bowlingAlley.text+'</option>';
				}

				html += '</select>';

				$('#BowlingAlley_id').html(html);
			}
		});
	});

	$("#Add_Member_Bowling_Alley_Dropdown").change(function() {

		var id = $(this).val();
		fetchLeaguesForBowlingAlley(id);
	});
});

function fetchBowlingAlleysForZipcode(zipcode) {

	var url = 'fetchBowlingAlleysForZipcode';
	var response = $.ajax({
		'url': url,
		'type': 'GET',
		'data': {
			'zipcode' : zipcode
		},
		'async' : false
	}).responseText;

	var response = $.parseJSON(response);

	if(response.error == null)
	{
		return $.parseJSON(response.data);
		//$.fn.yiiGridView.update(gridId); 
		//alert(response.message);

	}
	else
	{
		var errorMessages = response.error.messages;
		var output = '';
		for(var i = 0; i < errorMessages.length; ++i)
			output += errorMessages[i] + "\n";
		alert(output);
	}

	return null;
}

function fetchLeaguesForBowlingAlley(id)
{
	var url = 'fetchLeagueForBowlingAlley/id/'+id;

	$.ajax({
		'url': url,
		'type': 'get',
		'data': {
			'bowling_alley_id' : id
		},
		'success': function(response) { 

			var arr = $.parseJSON(response);
			var html = '<select>';

			for (var i = 0; i < arr.length; ++i)
			{
				var bowlingAlley = arr[i];
				html += '<option value="'+bowlingAlley.value+'">'+bowlingAlley.text+'</option>';
			}

			html += '</select>';

			$('#Add_Member_League_Dropdown').html(html);
		}
	});
}

function setSelectedLeague(gridID,cert_number,leagueName)
{
	var url = 'storeSelectedLeague';
	//set Title for Modal
	$('.modal_league_name').html(leagueName);

	$.ajax({
           type: "POST",
           url: url,
           data: {
           	'id' : cert_number
           }, 
           dataType: 'json',
           async : true,
           success: function(response)
           {
				if(response.error == null)
				{
	           		$.fn.yiiGridView.update(gridID); 					
				}
				else
				{
					var errorMessages = response.error.messages;
					var output = '';
					for(var i = 0; i < errorMessages.length; ++i)
						output += errorMessages[i] + "\n";
					alert(output);
				}
           }
    });
}