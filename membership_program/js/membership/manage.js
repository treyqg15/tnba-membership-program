var newRowsCount = 1;
var newRows = {};
var NAME = "newRow_";

$(document).ready(function() {

	var form = "create_membership_form";
	var url = "createMembership";
	var gridID = "membership-grid";

	initGridRowSelection(gridID);

	initFormSubmitForPage(form,url,gridID);

	form = "memberships_reports_form";
	initReportsFormSubmitForPage(form);	
});