$(document).ready(function() {
	
	$('#tnba_number_div').hide();

	$('#by_name').change(function(){
    	
    	$('#tnba_number_div').hide();
    	$("#tabs_div").html('');
    	$('#tabs_div').hide();
    	$('#by_name_div').show();
    	$('#member_results_div').show();
	});

	$('#by_tnba_number').change(function(){
    	
    	$('#by_name_div').hide();
    	$('#member_results_div').hide();
    	$('#tnba_number_div').show();
    	$("#tabs_div").html('');
    	$('#tabs_div').show();
	});

	$("#find_member_form").submit(function() {

		$("#tabs_div").html('');
		$('#tabs_div').hide();

		var url = "findMemberByName"; // the script where you handle the form input.

	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $(this).serialize(), // serializes the form's elements.
	           success: function(response)
	           {
	               if(response.error == null)
					{
						//var members = response.data;
						//alert(response.message);
						loadMemberResultsPage();
					}
					else
					{
						var errorMessages = response.error.messages;
						var output = '';
						for(var i = 0; i < errorMessages.length; ++i)
							output += errorMessages[i] + "\n";
						alert(output);

					}
	           }
	         });

	    return false; // avoid to execute the actual submit of the form.

	});
});

function storeTNBANumber() {

	var tnbaNumber = $(this).parent().parent().children(":nth-child(5)").text();

	var url = 'findMemberByTNBANumber';
	
	$.ajax({
    	type: "POST",
       	url: url,
       	data: {
       		'tnba_number' : tnbaNumber
       	},
       	success: function(response)
       	{
       		$('#tabs_div').show();
       		loadTabs();
       	}
	});
}

function loadMemberResultsPage() {

	$("#member_results_div").load('memberResults');
}

function loadTabs() {

	$("#tabs_div").unload();
	$("#tabs_div").load('tabs');
}