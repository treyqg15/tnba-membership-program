$(document).ready(function() {

	$("#member_w9_form").submit(function() {

		var url = "uploadW9"; // the script where you handle the form input.

	    $.ajax({
	           type: "POST",
	           url: url,
	           data: $(this).serialize(), // serializes the form's elements.
	           success: function(data)
	           {
	               alert(data); // show response from the php script.
	           }
	         });

	    return false; // avoid to execute the actual submit of the form.

	});

	var form = "create_member_form";
	var url = "createMember";
	var gridID = "member-grid";

	initGridRowSelection(gridID);

	initFormSubmitForPage(form,url,gridID);

	form = "member_reports_form";
	initReportsFormSubmitForPage(form);	

	attachEvents();
	
});

function attachEvents()
{
	$("#member-grid input[type=file]").change(function () {
	   	//var fileName = $(this).val();
	   	//$(".filename").html(fileName);
	   	//$("#member_w9_form").trigger("submit");
	   	var files = event.target.files;

	   	var id = $(this).attr('id').split("_")[3];

	   	uploadW9('member-grid',files,id);
	});

	$(".up_down_select").change(function () {
	   	//var fileName = $(this).val();
	   	//$(".filename").html(fileName);
	   	//$("#member_w9_form").trigger("submit");

	   	var id = $(this).attr('id').split("_")[3];
	   	var val = $(this).val();

	   	var up_down_div = $('#up_down_div_' + id);

	   	if(val == "Upload W9")
	   	{
	   		up_down_div.html('<input id="up_down_select_'+id+'" type="file" />');
	   	}
	   	else
	   	{
	   		up_down_div.html('<a href="'+memberControllerUrl+'/crud/member/downloadW9/id/'+id+'" > Download W9 </a>');
	   	}

	   	attachEvents();
	   	//alert("here");
	});
}
 
function roleParams(params)
{
	var i = 0;
	params.value.splice(0,1)
	params.name = 'role_id';
	return params;
}