var selectedRows = {};
var columns = [
	'First Name',
	'Middle Name',
	'Last Name',
	'Street Address',
	'Zipcode',
	'TNBA Number',
	'USBC Number',
	'Status',
	'Date Of Performance',
	'Honor Score'
];

var PACKING_LIST = 'PACKING LIST';
var PACKING_LETTER = 'PACKING LETTER';

$(document).ready(function() {

	$('#awards-grid table tbody tr').live('click',function(e)
	{
		var keys = $('#awards-grid > div.keys > span');
		var pk=keys.eq(this.rowIndex-2).text();
		
		if(typeof selectedRows[pk] != "undefined")
		{
			delete selectedRows[pk];
		}
		else
		{
			var rows = {};

		    var length = $(this).children().length;
		    for(var i = 1; i < length; ++i)
		    {
		    	var value=$(this).children(':nth-child('+i+')').text();
		    	rows[columns[i - 1]] = value;
			}

		   	selectedRows[pk] = pk;
		   	
		   	var keys = $('#awards-grid > div.keys > span');
	   	}
	    //var pk=keys.eq(this.rowIndex-2).text();
	   	//currentPK = pk;
	});

	//set hidden data field with json representation of rows
	$("#awards_reports_form").submit(function() {

		var data_field = $('#data_field');
		data_field.val(JSON.stringify(selectedRows));

	});

	
});

function setPackingFunction(func)
{
	$('#report_type_field').val(func);

	//force form reports submit
	$("#awards_reports_form").trigger("submit");

}

function storeSelectedRows(id)
{
	var grid = $('#awards_reports_grid');
	//alert("storing row: " + id);
}

function generatePackingList()
{
	var grid = $('#awards_reports_grid');
	
	$.ajax({
      type: "POST",
      url: generatePackingListUrl,
      //data:  {val1:1,val2:2},
      success: function(response){
           alert("Success: " + response);
          },
      error: function(xhr){
      alert("failure"+xhr.readyState+this.url)

      }
    });
}