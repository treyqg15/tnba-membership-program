$(document).ready(function () {

	var form = "create_award_form";
	var url = "createAward";
	var gridID = "awards-grid";

	initGridRowSelection(gridID);

	initFormSubmitForPage(form,url,gridID);

	form = "awards_reports_form";
	initReportsFormSubmitForPage(form);

	$("#BowlingAlley_zipcode").change(function() {

		var zipcode = $(this).val();
		var url = 'fetchBowlingAlleysForZipcode/zipcode/'+zipcode;
		$.ajax({
			'url': url,
			'type': 'get',
			'success': function(response) { 

				var arr = $.parseJSON(response);
				var html = '<select>';

				for (var i = 0; i < arr.length; ++i)
				{
					var bowlingAlley = arr[i];
					html += '<option value="'+bowlingAlley.value+'">'+bowlingAlley.text+'</option>';
				}

				html += '</select>';

				$('#AwardsHighAverageBowler_bowling_alley_id').html(html);

				//Get first returned bowling alley
				fetchLeaguesForBowlingAlley(arr[0].value);
			}
		});
	});

	$("#AwardsHighAverageBowler_bowling_alley_id").change(function() {

		var id = $(this).val();
		fetchLeaguesForBowlingAlley(id);
	});

	$('.search-button').click(function(){
		$('.search-form').toggle();
		return false;
	});
	
	$('.search-form form').submit(function(){
		$('#awards-grid').yiiGridView('update', {
			data: $(this).serialize()
		});
		return false;
	});

	//Edit SL: more universal
	$(document).on('hidden.bs.modal', function (e) {
	    $(e.target).removeData('bs.modal');
	});
});

function fetchLeaguesForBowlingAlley(id)
{
	var url = 'fetchLeagueForBowlingAlley/id/'+id;

	$.ajax({
		'url': url,
		'type': 'get',
		'data': {
			'bowling_alley_id' : id
		},
		'success': function(response) { 

			var arr = $.parseJSON(response);
			var html = '<select>';

			for (var i = 0; i < arr.length; ++i)
			{
				var bowlingAlley = arr[i];
				html += '<option value="'+bowlingAlley.value+'">'+bowlingAlley.text+'</option>';
			}

			html += '</select>';

			$('#AwardsHighAverageBowler_league_cert_number').html(html);
		}
	});
}

function createZipcodeUrl(url)
{
	//var pk = $(this).parent().parent().children(":nth-child(1)").text();
	return url;
}

function populateBowlingAlleysDropdown()
{
	var response = fetchBowlingAlleysAndLeaguesForZipcode;

	var data = response.data;
}

function addAwardsGamesClickEvent(buttonID)
{
	$('#'+buttonID).click(function(event) {

		event.stopPropagation();

		var id = $(this).attr('id');
		var arr = id.split("_");

		//get id that I tacked on at the end
		id = arr[arr.length - 1];

		//get honor score saved in attribute
		var honorScore = $(this).attr('data-honorscore');

		var isSingleGame = $(this).attr('data-isSingleGame');

		//make sure it is a num and not a string
		isSingleGame = parseInt(isSingleGame);

		var url = "storeGamesSessionInfo";
		var type = "POST";
		var success = null;
		var data = {
			'awardID' : id,
			'honorScore' : honorScore
		};

		var response = new Response();

		var builder = new AjaxBuilder(url,type,response.getOnlyErrorOutputFunc);
		builder.setData(data).setAsync(false);

		var myAjax = new MyAjax(builder);

		myAjax.makeCall();

		$("#manage_awards_games .modal-body").load('manageAwards', function() { 
			
			form = "awards_games_form";
			url = "saveGames";
			initFormSubmitForPage(form,url,null);

			$('#awards_id_field').val(id);
			$('#honor_score_id_field').val($(this).attr('data-honorscore'));

			if(isSingleGame)
			{
				$('#multi_games_div').hide();
				$('#single_game_div').show();
			}
			else
			{
				$('#multi_games_div').show();
				$('#single_game_div').hide();	
			}
			
	        $("#manage_awards_games").modal("show");
    	});
		
	});
}